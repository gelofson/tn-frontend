if(!TN) var TN = {};
if(!TN.allNewspapers) TN.allNewspapers = {};

(function ($allNewspapers){
	var imageContainer = null;
	var currAjaxCallId = null;
	var totalPages = 0;
	var numItems = 25;
	var pageNum = 1;
	
	function getNewspaperHtml(currNewspaper){
		var npHtml;
		npHtml = '<div class="myPageNewspaperBox item">\
         	<div class="mainimageContainer">\
             </div><!--/mainimageContainer-->\
             <ul class="myPageNewspaperBox_List">\
             </ul>\
             <h2 class="npHeadline">' + currNewspaper.npi.headline + '</h2>\
         </div>';
		return npHtml;
	}
	
	function loadNewspaper(currNewspaper, targetHtmlElem){
		
		var firstImageCont = targetHtmlElem.find('.mainimageContainer');
		var npHeadlineElem = targetHtmlElem.find('.npHeadline');

		function getImageHtml(url, width, height){
			var imageHtml = '<img src="' + url + '"' +
				' onload="TN.utils.normalizeImage(this, ' + width + ',' + height + ');"/>';
			
			return imageHtml;
		}
		
		if( !!currNewspaper.stories ){
			var storiesArray = currNewspaper.stories;
			var numStories = storiesArray.length;
			
			firstImageCont.empty();
			if( numStories > 0 ){
				var firstStory = storiesArray[0];
				firstImageCont.append(getImageHtml(firstStory.imageOrig, 230, 230));
										
//					npHeadlineElem.css('color', newspaperJson.titleFontColor);
//					npHeadlineElem.css('font-family', newspaperJson.titleFontStyle);				
//					npHeadlineElem.css('font-size', newspaperJson.titleFontSize);
				
				if( numStories > 1 ){
					var remainingImagesCont = targetHtmlElem.find('.myPageNewspaperBox_List');
					for( var i = 1; i < 5; i++ ){
						if( i < numStories ){
							var currStory = storiesArray[i];
							remainingImagesCont.append('<li>' + getImageHtml(currStory.imageThumb, 54, 62) + '</li>');									
						}
						else {
							remainingImagesCont.append('<li></li>');									
						}
					}								
				}
				imageContainer.masonry();						
			}
			else {
				firstImageCont.append('This newspaper does not contain any images.');
			}
		}
		targetHtmlElem.click(function(){
			TN.newspaperViewLB.init(currNewspaper.npi.id);
		});			
		
	}
	
	function loadNewspapers(){
		currAjaxCallId = new Date().getTime();
		
		(function(){
			var ourAjaxCallId = currAjaxCallId;
			
			TN.services.getNewspapersImg(pageNum, numItems).done(function(json){
				if( ourAjaxCallId === currAjaxCallId ){
					if( !!json && !!json[0] ){
						totalPages = json[0].noOfPages;
						var npArray = json[0].np;
						
						var numNewspapers = npArray.length;
						
						for( var i=0; i < numNewspapers; i++ ){
							var currNewspaper = npArray[i];
							var currNewspaperHtmlElem = $(getNewspaperHtml(currNewspaper));
							
							imageContainer.append(currNewspaperHtmlElem);
							if(pageNum === 0 && i=== 0){
								imageContainer.masonry({
									itemSelector : '.item',
									isFitWidth: true,
									columnWidth : 0
								});
							}
							else{
								imageContainer.masonry('appended', currNewspaperHtmlElem);
							}
							loadNewspaper(currNewspaper, currNewspaperHtmlElem);
						}
					}
				}
			}).fail(function(jqXHR){
				if( ourAjaxCallId === currAjaxCallId ){
					if( jqXHR.status === 404 ){
						imageContainer.append('No newspapers available.');
					}
				}				
			});
			
		}());
	}
	
	$allNewspapers.initialize = function(){
		TN.services.keepThisSessionAlive();
		imageContainer= $('#container');
		imageContainer.empty();
		loadNewspapers();
		
        $('#paperSearch').keyup(function(event){
            if( event.which === 13 ){        
            	alert('Coming soon!');
            }
        });
        
        $('#paperSearchIcon').click(function(){
        	alert('Coming soon!');
        });
        
		$(window).scroll( function() {
			if (( $(window).scrollTop() >= $(document).height() - $(window).height() - 1000  ) && imageContainer.is(':visible') ) {
				if( pageNum < totalPages ){
					pageNum++;
					loadNewspapers();
				}
			}
		});
		
	};
})(TN.allNewspapers);