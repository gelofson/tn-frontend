if (!TN) var TN= {};
if (!TN.header) TN.header= {};

(function($header){
    
    custId = TN.utils.getCookie('TNUser');
    TN.header.custIdNum = TN.utils.getCookie('TNUserNum');
    if (TN.utils.getQueryStringParam("view")) viewId = TN.utils.getQueryStringParam("view");
    else viewId = TN.utils.getCookie('TNUser');
    var totalNPages = 0;
    var currNPage = 0;
    var notificationsNum = 0;
    var notificationsLoaded = false;
    var uncheckedNotifs = 0;
    if (typeof TN.homepage === "undefined") TN.homepage = false;
    var attempt = 0;
    var maxAttempts = 3;
    TN.guestUserName = "abc123987zyx@test.com";
    TN.guestPassword = "abc123987zyx";
    var emailRegistrationInProgress = false;

    function loadNotifications(pageNum, loadAll, prependOnly, prependNum) {

        function populateNotificationsHtml( response ){

            function getNotificationItemHtml(notificationItem){
                
                function contentTypeTemplate (messageType) {
                    if ((messageType == "FollowingRequest") || (messageType == "AcceptOrReject") || (messageType == "AcceptedFriendRequest")) {
                        return 'mypage.html?view=' + notificationItem.custId;
                    }
                    else {
                        if (notificationItem.messageId == "") return 'javascript:alert(\'Missing message ID (a notification for a duplicate like?)\')';
                        else return 'javascript:TN.lightbox.show(\'' + notificationItem.custId + '\', ' + notificationItem.messageId + ')';
                    }
                }

                function friendshipControls (requestOriginator) {
                    TN.services.loadUserInfo(requestOriginator, custId).done(function(msg){
                        if (msg[0].isFriend == "Y")
                            $(".requestOriginator[value='"+requestOriginator+"']").parent().text('Accepted');
                        else if (msg[0].friendRequestType == "") {
                            // Commented out because currently both friend request withdrawn and friend request rejected
                            // states fall into this same case:
                            // $(".requestOriginator[value='"+requestOriginator+"']").parent().text('Rejected');
                        }
                        else
                            $(".requestOriginator[value='"+requestOriginator+"']").parent().html('\
                            <input class="requestOriginator" type="hidden" value="' + notificationItem.custId + '" />\
                            <a href="javascript:void(0);" onclick="TN.header.answerFriendshipNotifs(\'Y\', this);" class="notifsReqAccept">accept</a>\
                            <a href="javascript:void(0);" onclick="TN.header.answerFriendshipNotifs(\'N\', this);" class="notifsReqReject">reject</a>\
                            ');
                    });
                    return "";
                }
                
                if( loadAll ){
                    var dataHtml = '\
                        <li">\
                            <a onclick="if( TN.header.isGuestUser() ) return false;" href="mypage.html?view=' + notificationItem.custId + '"><figure class="notifications-profile-img"><img width="36" height="36" src="' + notificationItem.thumbImageUrl + '" onload="TN.utils.normalizeImage(this, 36, 36);"/></figure></a>\
                            <a onclick="if( TN.header.isGuestUser() ) return false;" class="author left" href="'+ contentTypeTemplate(notificationItem.messageType) +'">'+ notificationItem.notificationMessage + '</a><br/><span class="post-time" style="font-size: 10px">'+ notificationItem.elapsedTime +'</span>\
                        </li>\
                        <li class="divider"></li>';
                    return dataHtml;                    
                }
                else {
                    var dataHtml = '\
                        <li class="bottom-border">\
                            <a class="author" style="padding-right:0px;padding-left:5px;" onclick="if( TN.header.isGuestUser() ) return false;" href="mypage.html?view=' + notificationItem.custId + '"><figure class="notifications-profile-img"><img width="36" height="36" src="' + notificationItem.thumbImageUrl + '" onload="TN.utils.normalizeImage(this, 36, 36);"/></figure></a>\
                            <a style="padding-left:0px; padding-right:0px; float:left; width:158px; white-space:normal;font-size:14px; color:#006aa8; font-family:\'optima\', \'Candara\', Helvetica Light, sans-serif; font-weight:bold;" href="'+ contentTypeTemplate(notificationItem.messageType) +'">'+ notificationItem.notificationMessage + '</a><br/><span style="display:block; float:right; padding-right: 18px; font-size:10px; color:#808080; font-family:\'optima\', \'Candara\', Helvetica Light, sans-serif; font-weight:bold;">' + notificationItem.elapsedTime + '</span>\
                        </li>';
                    return dataHtml;
                }               
            }
            
            if( !!response && !!response[0] ){
                if( loadAll ){
                    currNPage = pageNum;
                    totalNPages = parseFloat(response[0].noOfPages);
                    notificationsNum = parseFloat(response[0].totalRecords); 
                    if( !!response[0].notificationTrayList && response[0].notificationTrayList.length > 0 ){
                        var notifsReadCount = response[0].notificationTrayList.length;
                        var notificationsHtml = "";
                        for( var i = 0; i < notifsReadCount; i++ ) {
                            notificationsHtml += getNotificationItemHtml(response[0].notificationTrayList[i]);
                        }
                        $('#allNotificationsList').append(notificationsHtml);
                        if( currNPage < totalNPages ){
                            loadNotifications(currNPage+1, true );
                        }
                    }                   
                }
                else {
                    if( !prependOnly ){
                        currNPage = pageNum;
                        totalNPages = parseFloat(response[0].noOfPages);
                        notificationsNum = parseFloat(response[0].totalRecords); 
                        if( !!response[0].notificationTrayList && response[0].notificationTrayList.length > 0 ){
                            var notifsReadCount = response[0].notificationTrayList.length;
                            var notificationsHtml = "";
                            // $('#notifsShowAll').remove();
                            for( var i = 0; i < notifsReadCount; i++ ) {
                                notificationsHtml += getNotificationItemHtml(response[0].notificationTrayList[i]);
                            }
                            $('#notificationsList').append(notificationsHtml);
                        }
                    }
                    
                    if (prependOnly) {
                        totalNPages = parseFloat(response[0].noOfPages);
                        notificationsNum = parseFloat(response[0].totalRecords); 
                        if( !!response[0].notificationTrayList && response[0].notificationTrayList.length > 0 ){
                            var notifsReadCount = response[0].notificationTrayList.length;
                            var notificationsHtml = "";
                            for( var i = 0; i < notifsReadCount; i++ ) {
                                notificationsHtml += getNotificationItemHtml(response[0].notificationTrayList[i]);
                            }
                            $('#notificationsList').prepend(notificationsHtml);
                        }
                    }
                }
            }
        }
        
        TN.services.getNotificationTrayDetails(custId, 5, pageNum).done(function(json){
            populateNotificationsHtml(json);
            if ( !prependOnly && !loadAll ){
                if (currNPage != totalNPages) {
                    $('#notificationsList').append('<li id="notifsShowAll"><a href="#" class="notifications full-width" onclick="TN.header.loadAllNotifications();"><span class="text-center">See All</span></a></li>');
                }
            }
            notificationsLoaded = true;
        }).fail(function(){
            $('#noNotifications').show();
        });
    }
    
    $header.answerFriendshipNotifs = function(answer, sourceElem) {
        var originator = $(sourceElem).parent().find('.requestOriginator').val();
        $(".requestOriginator[value='"+originator+"']").parent().find('.notifsReqAccept, .notifsReqReject').unbind();
        TN.services.answerFriendRequest(originator, answer).done(function(){
            $(".requestOriginator[value='"+originator+"']").parent().find('.notifsReqAccept, .notifsReqReject').hide();
            if (answer == "Y") {
                $(".requestOriginator[value='"+originator+"']").parent().append("Accepted");
            }
            else {
                $(".requestOriginator[value='"+originator+"']").parent().append("Rejected");
            }
        }).fail(function(){
            alert("Error occurred");
        });
    };
    
    $header.loadAllNotifications = function(){
        $('#allNotificationsList').empty();
        $('#NotificationsModal').reveal();
        loadNotifications(1, true);
    };
    
    $header.trackNavigation = function(linkElem){
        _gaq.push(['_trackEvent', 'Page Nagivation', $(linkElem).text(), 'User: ' + custId]);
    };
    
    $header.trackNavigationToMyPage = function(){
        _gaq.push(['_trackEvent', 'Page Nagivation', 'My Profile', 'User: ' + custId]);
    };
    
    $header.loadHeader = function(){
        var headerHtml = '<nav class="top-bar contain-to-grid">\
            <ul>\
                <li class="name"><a href="index.html" class="logo">Tiny News - beta</a></li>\
                <li class="toggle-topbar"><a href="#"></a></li>\
            </ul>\
            <section>\
                <ul class="left left-interactive-header-ul">\
                    <li class="has-dropdown" style="display:' + ( ( (!!custId && ( custId !== TN.guestUserName )) || ( (custId === TN.guestUserName) && (!TN.homepage) ) ) ? '' : 'none') + '">\
                        <a class="noArrow" href="javascript:void(0);"><img class="header-img-icons" src="images/mypage-icon.png" onclick="#"/></a>\
                        <ul class="dropdown" id="myPageHeaderDropdown" style="display:block">\
                            <li><a href="mypage.html">My Profile</a></li>\
                            <li><a onclick="if( TN.header.isGuestUser() ) return false;" href="settings.html">My Settings</a></li>\
                            <li><a href="javascript:TN.header.findPeople();">Find People</a></li>\
                            <li><a href="leaderboard-all.html">Leaderboard</a></li>\
                            <li><a href="javascript:void(0);" id="signOut">Sign out</a></li>\
                        </ul>\
                    </li>\
                    <li class="has-dropdown" style="display:' + ( ((!!custId && ( custId !== TN.guestUserName )) || ( (custId === TN.guestUserName) && (!TN.homepage) ) ) ? '' : 'none') + '">\
                        <a class="noArrow" href="javascript:void(0);"><img class="header-img-icons" src="images/notifications-icon.png"/></a>\
                        <ul class="dropdown" id="notificationsList" style="width:230px;display:block;">\
                            <li id="noNotifications" style="display: none"><a href="#" class="notifications full-width"><span class="text-center">You have currently no notifications.</span></a></li>\
                        </ul>\
                    </li>\
                </ul>\
                <ul class="right">\
                    <li style="display:' + ( ((!!custId && ( custId !== TN.guestUserName )) || ( (custId === TN.guestUserName) && (!TN.homepage) ) ) ? '' : 'none') + '"><a href="javascript:void(0);" onclick="if (!!custId) { TN.header.trackNavigation(this); location.href=\'map.html\'; } else $(\'#signInRegisterModal\').reveal();">See it on the street</a></li>\
                    <li class="has-dropdown" style="display:' + ( ((!!custId && ( custId !== TN.guestUserName )) || ( (custId === TN.guestUserName) && (!TN.homepage) ) ) ? '' : 'none') + '">\
                        <a href="javascript:void(0);" onclick="(!!custId) ? (TN.header.isGuestUser() ? false : TN.createStory.show() ) : $(\'#signInRegisterModal\').reveal();">Tell your story </a>\
                        <ul class="dropdown" id="postStoryHeaderDropdown" style="display:none">\
                            <li><a href="javascript:void(0);" id="addStory">From my computer</a></li>\
                            <li><a id="addClipThis" href="javascript:void(0);">From a website</a></li>\
                        </ul>\
                    </li>\
                    <li style="display:' + ( ((!!custId && ( custId !== TN.guestUserName )) || ( (custId === TN.guestUserName) && (!TN.homepage) ) ) ? '' : 'none') + '"><a href="javascript:void(0);" onclick="if (!!custId) { TN.header.trackNavigation(this); location.href=\'stories.html\'; } else $(\'#signInRegisterModal\').reveal();">Include others stories</a></li>\
                    <li style="display:' + ( ((!!custId && ( custId !== TN.guestUserName )) || ( (custId === TN.guestUserName) && (!TN.homepage) ) ) ? '' : 'none') + '"><a href="javascript:void(0);" onclick="if (!!custId) { TN.header.trackNavigation(this); location.href=\'newspaperBegin.html\'; } else $(\'#signInRegisterModal\').reveal();">Publish newspaper</a></li>\
                    <li style="display:' + ( ((!!custId && ( custId !== TN.guestUserName )) || ( (custId === TN.guestUserName) && (!TN.homepage) ) ) ? 'none' : '') + '" class="signInOutParentLi"><a href="javascript:void(0);" id="signInOutLink">Sign in</a></li>\
                </ul>\
            </section>\
            </nav>';
        
        $('header').append(headerHtml);
    };
    
    TN.header.loadHeader();

    function loadFooter(){
        var footerHtml = '<div class="twelve columns">\
            <div class="row">\
                <div class="twelve columns">\
                    <ul class="link-list left">\
                        <!--li><a href="contact.html" ><img src="images/email.png"  alt="mail" title="Contact Tiny News" class="has-tip tip-top" data-width="80"></a></li-->\
                        <li><a href="http://www.facebook.com/pages/Tiny-News/326376470776048"><img src="images/facebook.png"  alt="facebook" title="Friend Us On Facebook" class="has-tip tip-top" data-width="90"></a></li>\
                        <li><a href="javascript:alert(\'We are getting there!\');"><img src="images/google.png"  alt="gmail" title="Join Us On Google Plus" class="has-tip tip-top" data-width="90"></a></li>\
                        <li ><a href="https://twitter.com/TinyNewser"><img src="images/twitter.png" alt="twitter" title="Follow Us on Twitter" class="has-tip tip-top" data-width="70"></a></li>\
                    </ul>\
                    <p>&copy; 2013 Tiny News.  All rights reserved.</p>\
                </div>\
            </div>\
        </div>';
        
        $('footer').empty().append(footerHtml);
    }
    
    function loadNotificationsModal(){
        var notifModalHtml = '<div id="NotificationsModal" class="reveal-modal notificationsSrollbar">\
                                <h2>Notifications</h2>\
                                <div class="row">\
                                    <div class="twelve columns">\
                                        <ul id="allNotificationsList" class="side-nav"></ul>\
                                    </div>\
                                </div>\
                                <a class="close-reveal-modal"></a>\
                            </div>';
        $('body').append(notifModalHtml);
    }

    function loadSignInReveal(){
        var revealHtml = '<div id="signInRegisterModal" class="reveal-modal large">\
            <h2 class="Signin">Sign in</h2>\
            <div class="row">\
                <div class="twelve columns">\
                    <div class="row">\
                        <div class="five columns">\
                            <div class="row">\
                                <div class="eight columns top-margin">\
                                    <figure class="facepile"><img src="images/facepile.png" /></figure>\
                                    <a id="fbLogin" href="#"><img src="images/signInRegister/facebook-signin.png"></a>\
                                </div>\
                            <div class="four columns"><span class="OR-text">OR</span></div>\
                            </div>\
                        </div>\
                        <div class="seven columns">\
                            <form>\
                                <div class="row">\
                                    <div class="ten columns centered">\
                                        <label><b>Username</b></label>\
                                        <input type="text" placeholder="i.e. - jane@wrightstuff.com" name="username" id="username_input" />\
                                        <small style="display: none;" class="error" id="usernameTicker"></small>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="ten columns centered">\
                                        <label><b>Password</b></label>\
                                        <input type="password" name="password" id="password_input" />\
                                        <small style="display: none;" class="error" id="passwordTicker"></small>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="five columns offset-by-seven">\
                                        <a class="radius button" href="javascript:void(0);" id="signInButton">Sign In</a>\
                                    </div>\
                                </div>\
                            </form>\
                        </div>\
                    </div>\
                </div>\
            </div>\
                <p class="text-center">Not registered, <a href="javascript:void(0);" onclick="$(\'#registerModalSocial\').reveal()">click here</a></p>\
                <a class="close-reveal-modal"></a>\
            </div>';
        $('body').append(revealHtml);
    }
    
    function loadRegistrationReveals(){
        var revealHtmlOld = '<div id="registerModal" class="reveal-modal expand">\
          <div class="row">\
            <div class="twelve columns">\
              <div class="row">\
                <div class="five columns">\
                  <div class="row">\
                    <div class="eight columns top-margin">\
                      <figure class="facepile"><img src="images/facepile.png" /></figure>\
                      <a href="#" id="fbRegister" class="primary button expand"><figure class="facebook-mark"></figure>Register via Facebook</a>\
                    </div>\
                    <div class="four columns"><span class="OR-text">OR</span></div>\
                  </div>\
                </div>\
                <div class="seven columns">\
                  <div class="row">\
                    <div class="eight columns">\
                      <div class="row">\
                        <div class="nine columns top-margin">\
                          <label><b>Name</b></label>\
                          <input type="text" placeholder="i.e. - Joey or JRock, you decide" name="username" id="username_input" />\
                          <label><b>Email</b></label>\
                          <input type="text" placeholder="i.e. - jane@wrightstuff.com" name="email" id="email_input" />\
                              <small style="display: none;" class="error" id="registerEmailTicker"></small>\
                          <label><b>Password</b></label>\
                          <input type="password" name="password" id="password_input"/>\
                              <small style="display: none;" class="error" id="registerTicker"></small>\
                          <div class="row">\
                            <div class="eight columns">\
                            </div>\
                            <div class="four columns">\
                              <input type="submit" name="Register" value="Submit" class="button right" id="submitRegisterFormButton" />\
                            </div>\
                          </div>\
                        </div>\
                        <div class="three columns">\
                          <span class="OR-text">&amp;</span>\
                        </div>\
                      </div>\
                    </div>\
                    <div class="four columns">\
                      <h4>Upload Photo</h4>\
                      <form action="/php/store_image.php" method="post" enctype="multipart/form-data" id="photoForm">\
                        <label for="file" id="uploadProfileImgArea">\
                          <figure class="add-photo"><img id="userPhoto" src="images/no-photo.png" /></figure>\
                          <a href="javascript:;" class="radius button">Add Profile Picture</a>\
                        </label>\
                        <input type="file" name="file" id="file" />\
                      </form>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <a class="close-reveal-modal"></a>\
        </div>';
        
        var registerModalSocial = '<div id="registerModalSocial" class="reveal-modal" style="width:462px; border-left: 0 none;">\
          <div class="row">\
            <div class="ten columns centered" style="padding-top: 30px">\
              <a href="#" id="fbRegister"><img src="images/signInRegister/fb.png" alt="fb"></a>\
              <a href="#" style="display:none"><img src="images/signInRegister/tw.png" alt="tw"></a>\
              <span id="socialRegisterTicker"></span>\
            </div>\
          </div>\
          <div class="row" style="padding-top: 15px">\
            <p style="text-align:center; font-size:15px">or register with your \
            <a href="javascript:void(0);" onclick="$(\'#registerModalEmail\').reveal({\'opened\':function(){\
                    $(\'#username_input_reg, #email_input_reg, #password_input_reg\').placeholder();\
                 }});">email address</a></p>\
          </div>\
          <div class="row">\
            <div class="centered" style="padding-left: 3px">\
              <img src="images/signInRegister/pic-3.png" alt="user-pic">\
              <img src="images/signInRegister/pic-5.png" alt="user-pic">\
              <img src="images/signInRegister/pic-6.png" alt="user-pic">\
              <img src="images/signInRegister/pic-7.png" alt="user-pic">\
              <img src="images/signInRegister/pic-8.png" alt="user-pic">\
              <img src="images/signInRegister/pic-1.png" alt="user-pic">\
              <img src="images/signInRegister/pic-2.png" alt="user-pic">\
              <img src="images/signInRegister/pic-4.png" alt="user-pic">\
              <img src="images/signInRegister/pic-9.png" alt="user-pic">\
              <img src="images/signInRegister/pic-15.png" alt="user-pic" style="padding-left: 2px; padding-right: 1px">\
              <img src="images/signInRegister/pic-16.png" alt="user-pic">\
              <img src="images/signInRegister/pic-10.png" alt="user-pic">\
              <img src="images/signInRegister/pic-11.png" alt="user-pic">\
              <img src="images/signInRegister/pic-12.png" alt="user-pic">\
              <img src="images/signInRegister/pic-13.png" alt="user-pic" style="padding-right:1px">\
              <img src="images/signInRegister/pic-14.png" alt="user-pic">\
            </div>\
          </div>\
          <a class="close-reveal-modal"></a>\
        </div>';

        var registerModalEmail = '<div id="registerModalEmail" class="reveal-modal" style="width:462px; border-left: 0 none; padding: 15px">\
          <div class="row">\
            <div class="signup centered">\
              <div class="row" style="margin-bottom: 0px">\
                <div class="signup-values">\
                  <input type="text" placeholder="Name" name="username" id="username_input_reg">\
                  <input type="text" placeholder="Email Address" name="email" id="email_input_reg">\
                    <small style="display: none;" class="error" id="registerEmailTicker"></small>\
                  <input type="password" placeholder="Password" name="password" id="password_input_reg">\
                    <small style="display: none;" class="error" id="registerTicker"></small>\
                </div>\
              </div>\
              <div class="row">\
                <form action="/php/store_image.php" method="post" enctype="multipart/form-data" id="photoForm">\
                  <label for="file" id="uploadProfileImgArea">\
                    <div class="upload-pic">\
                      <figure class="add-photo-new"><img src="images/signInRegister/upload.png" alt="upload-picture" id="userPhoto"></figure>\
                      <h1 id="registerModalUploadPhotoText">Upload Photo</h1>\
                    </div>\
                  </label>\
                  <input type="file" name="file" id="file" />\
                </form>\
              </div>\
              <div class="row" style="margin-bottom: 0px">\
                <div class="signup-btn twelve columns">\
                  <p>or <strong>add it later</strong></p>\
                  <div class="twelve columns primary button radius" id="submitRegisterFormButton">\
                    <a href="javascript:void(0);">Sign Up</a>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <a class="close-reveal-modal"></a>\
        </div>';

        $('body').append(registerModalSocial);
        $('body').append(registerModalEmail);

    };

    $header.findPeople = function() {

        function fillResultTemplate(obj, isFriend){

            return '<li>\
              <div class="row" style="padding-left: 30px; padding-right: 50px;">\
                <div class="five left">\
                    <div class="modulNews">\
                        <a href="mypage.html?view=' + obj.emailid + '">\
                        <div class="modulNewsImage left"> <img style="width:50px; height:50px" alt="user image" src="' + obj.profileThumbPicUrl + '"> </div>\
                        <h2 class="text-left">' + obj.firstName + ' ' + obj.lastName + '</h2></a>\
                        <p class="text-left">  </p>\
                    </div>\
                </div>\
                <div class="seven right">\
                    <div class="modulNews right">\
                        <input name="emailid" type="hidden" value="' + obj.emailid + '">\
                        ' + (isFriend ? '' : '<a href="javascript:void(0);" class="searchResult_AddBtn button rounded" \
                            style="position:relative; top: 8px">Add Friend</a>') +'\
                    </div>\
                </div>\
              </div>\
            </li>';
        }

        function injectResult(searchResult) {
            $('.searchResult_list').html('');
            jQuery.each(searchResult[0].nonFriends, function(index, val){
                $('.searchResult_list').append(fillResultTemplate(val, false));
            });
            jQuery.each(searchResult[0].friends, function(index, val){
                $('.searchResult_list').append(fillResultTemplate(val, true));
            });
            $('.searchResult_AddBtn').click(function(event){
                if( TN.header.isGuestUser() ) return;
                $(event.target).unbind();
                var emailid = $(event.target).parent().find('[name="emailid"]').val();
                TN.services.addShopBeeFriends(emailid).done(function(msg){
                    $(event.target).parent().text('Request Pending');
                });
            })
        }

        function initiateSearch() {
            var searchString = $('#findPeopleSearchInput').val();
            TN.services.shopbeeUsersSearch(searchString, 1, 5).done(function(json){
                injectResult(json);
            }).fail(function(){
                $('.searchResult_list').html("<li>No match found</li>");
            });
        }

        var findPeopleReveal = $('<div class="row reveal-modal" id="findPeopleReveal">\
            <div class="add-friend centered">\
                <h4 class="af-heading">Add Friends</h4>\
                <ul>\
                    <li><a href="#"><img src="images/addFriendNew/tinynews.png" alt="tinynews-button"></a></li>\
                    <!--li><a href="#"><img src="images/addFriendNew/facebook.png" alt="facebook-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/twitter.png" alt="twitter-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/googleplus.png" alt="googleplus-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/pinterest.png" alt="pintrest-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/instagram.png" alt="instagram-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/linkedln.png" alt="linkedln-button"></a></li-->\
                </ul>\
                <div class="row">\
                    <div class="twelve columns ff-with-text">\
                        <h4 class="af-subheading">Begin by searching Tiny News for your friends.</h4>\
                        <input type="text" class="af-input" id="findPeopleSearchInput" placeholder="Enter your friends name here">\
                    </div>\
                </div>\
                    <div class="primary button radius" id="findPeopleSearchButton">\
                        <a href="#">Find Friends</a>\
                    </div>\
                </div>\
                <div class="">\
                    <ul class="searchResult_list no-bullet ModelList">\
                    </ul>\
                </div>\
            <a class="close-reveal-modal"></a>\
        </div>');

        findPeopleReveal.find('#findPeopleSearchInput').keyup(function(event){
            if( event.which === 13 ) {
                initiateSearch();
            }
        });

        findPeopleReveal.find('#findPeopleSearchButton').click(function(){
            initiateSearch();
        });

        $('body').append(findPeopleReveal);
        findPeopleReveal.reveal({"closed":function(){
            findPeopleReveal.remove();
            }
        });

    };

    /*function isJQueryLoaded(){
        if (typeof $ === "undefined") return false;
        else return true;
    };*/

    function isFoundationLoaded(){
        if (!!$ && typeof $.foundation === "undefined") return false;
        else return true;
    };

    function loginAsGuest(){
    	
        TN.services.loginCustomer('', TN.guestUserName, TN.guestPassword).done(function(msg){
            //console.log(typeof msg[0]);
            if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
                TN.utils.setCookie("TNUser", TN.guestUserName, null, 345600);
                if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                // window.location.href='./' + location.search; //
                var gaResp = _gaq.push(['_trackEvent', 'Join', 'Guest: Sign In', 'User: ' + TN.guestUserName]);
                location.href="index.html";
            }
            else alert ("Error occured in website initialization.");
        }).fail(function(msg){
//            jQuery.each( msg, function(key, value){ if (key == "status") loginfail = value; });
//            if (loginfail == 401) $('#usernameTicker').text('Wrong credentials').show();
//            else if (loginfail == 500) $('#passwordTicker').text('Wrong credentials').show();
//            else if (loginfail == 404) {
//                $('#usernameTicker').text('').hide();
//                $('#passwordTicker').text('Wrong Username/ Password').show();
//            }
//            else alert ("Error occured");
        	alert ("Error occured");
        });
    }
    
    $header.isGuestUser = function(){
    	if( custId === TN.guestUserName ){
        	var message = "You are currently logged in as a Guest user.  In order to use this functionality please register/ login as a Tiny News user.\r\n\r\nClick Ok to proceed to login page.\r\nClick Cancel to continue browsing as a Guest.";
        	if( confirm(message) ){
        		location.href = "index.html?signIn=true";
        	}
       		return true;
    	}
    	else {
    		return false;
    	}
    };
        
    $header.initialize = function(){

        attempt++;
        //console.log(isFoundationLoaded(), isJQueryLoaded(), attempt);
        //alert('check1');

        if (TN.newspaperBegin || isFoundationLoaded()) {
            
            custId = TN.utils.getCookie('TNUser');
            
            //alert('check2');
            
            /* <header> is loaded before DOM ready, otherwise Foundation's "toggle-topbar" functionality breaks - Goran */
            
            loadSignInReveal();

            loadRegistrationReveals();

            loadFooter();
            
            var fbRootElem = $('<div id="fb-root"></div>');
            $('body').append(fbRootElem);
            
            loadNotificationsModal();
            
            var TNServerAddress = null;        
            
            $header.servAddressFetch = $.ajax({ type:'GET', dataType:'text', url:'/php/server_address.php' });
            $header.servAddressFetch.done(function( msg ) {
                TNServerAddress = msg;
            });

            var imageSrc = unescape(TN.utils.getQueryStringParam('imageSrc'));
            var storyUrl = unescape(TN.utils.getQueryStringParam('storyUrl'));
            var signIn = unescape(TN.utils.getQueryStringParam('signIn'));
            var lightboxId = unescape(TN.utils.getQueryStringParam('lightboxId'));
            var npId = unescape(TN.utils.getQueryStringParam('npId'));;
            
            var imageUploaded = false;

            $('#addStory').click(function(){ 
            	if( TN.header.isGuestUser() ) return;
            	
            	TN.createStory.show(); 
            });
            
            $('#signOut').click(function(){
                TN.services.logout().always(function(){
                    document.cookie = 'TNUser=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    document.cookie = 'TNUserNum=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    document.cookie = 'TNUserUrl=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    document.cookie = 'TNUserName=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    TN.userInfo = {};
                    _gaq.push(['_trackEvent', 'Join', 'Sign Out', 'User: ' + custId]);
                    custId = null;
                    window.location.href = 'index.html';
                });
            });

            // This handles both sign-in and sign-out actions:
            $('#signInOutLink').click(function(){
               if (!!custId && custId !== TN.guestUserName ) {
                    // $('#signInOutLink').text('Signing out..');
                    // If signed in, call signout WS and irrespective of result empty session related data, register event to GA and reload page
                    TN.services.logout().always(function(){
                        document.cookie = 'TNUser=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        document.cookie = 'TNUserNum=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        document.cookie = 'TNUserUrl=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        document.cookie = 'TNUserName=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        TN.userInfo = {};
                        custId = null;
                        _gaq.push(['_trackEvent', 'Join', 'Sign Out', 'User: ' + custId]);
                        location.href="index.html";
                    });
                }
                // Else, show Sign-in/registration Reveal
                else $("#signInRegisterModal").reveal();
            });

            $('#username_input, #password_input').keyup(function(event){
                if( event.which === 13 ) {
                    $('#signInButton').click();
                }
            });
            
            $.getScript("javascripts/fbFunc.js");

            $('#fbLogin').click(function(){
                TN.FB.registerOrLogin();
            });
            
            $('#fbRegister').click(function(){
                TN.FB.registerOrLogin();
            });
            
            $('#signInButton').click(function(){
                var $form = $(this).parent().parent().parent();
                var $email_input = $form.find('input[name="username"]');
                var $pass_input = $form.find('input[name="password"]');
                if (!$email_input.val()) {
                    $email_input.addClass('error');
                    $('#usernameTicker').text('All Fields Are Required').show();
                }
                else if (!$pass_input.val()) {
                    $pass_input.addClass('error');
                    $('#passwordTicker').text('All Fields Are Required').show();
                }
                else {
                    $email_input.removeClass('error'); 
                    $pass_input.removeClass('error');
                    $.ajaxSetup({ cache: false, dataType: 'json' });
                    TN.services.loginCustomer('', $email_input.val(), $pass_input.val()).done(function(msg){
                        //console.log(typeof msg[0]);
                        if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
                            TN.utils.setCookie("TNUser", $email_input.val(), null, 345600);
                            if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                            // window.location.href='./' + location.search; //
                            $('#signOut').show();
                            var gaResp = _gaq.push(['_trackEvent', 'Join', 'Sign In', 'User: ' + $email_input.val()]);
                            location.href="index.html";
                        }
                        else alert ("Error occured");
                    }).fail(function(msg){
                        jQuery.each( msg, function(key, value){ if (key == "status") loginfail = value; });
                        if (loginfail == 401) $('#usernameTicker').text('Wrong credentials').show();
                        else if (loginfail == 500) $('#passwordTicker').text('Wrong credentials').show();
                        else if (loginfail == 404) {
                            $('#usernameTicker').text('').hide();
                            $('#passwordTicker').text('Wrong Username/ Password').show();
                        }
                        else alert ("Error occured");
                    });
                }
            });

            if($.browser.mozilla) {
              $('#uploadProfileImgArea').click(function(){
                  $('#file').click();
              });
            };
            
            // callback for ajaxSubmit:
            function showImageUrlResponse(responseText, statusText, xhr, $form) {
                var uploadedPhoto = new Image();
                
                uploadedPhoto.onload = function(){
                    imageUploaded = true;
                    
                    $('#userPhoto').on('load', function(){
                        $('#userPhoto').css('padding-top', '0px');
                        $('#registerModalUploadPhotoText').css('visibility', 'hidden');
                        var resultingHeight = $('#userPhoto').height();
                        if ( resultingHeight < 462 ) {
                            $('.upload-pic').css('height', resultingHeight + 'px');
                        }
                        else $('.upload-pic').css('height', '462px');
                    });
                    
                    $('#userPhoto').attr('src', responseText);
                };

                uploadedPhoto.src = responseText;
            };
            
            // Store image and preview by copying given url to registration's image src attribute:
            var regPhotoSubmitOptions = {
                success: showImageUrlResponse,
                dataType:'text'
            };

            $("#file").change(function() {
                $('#photoForm').ajaxSubmit(regPhotoSubmitOptions);
                //$('#registerModalUploadPhotoText').text('Uploading..');
            });

            // File input must be hidden in this way not to break any browser's file upload functionality
            $("#file").css('position', 'absolute').css('left', '-9999em');
            
            $('#email_input_reg, #username_input_reg, #password_input_reg').keyup(function(event){
                if (event.which == 13) {
                    $("#submitRegisterFormButton").click();
                }
            });

            $("#submitRegisterFormButton").click(function() {
                if (emailRegistrationInProgress) return;
                emailRegistrationInProgress = true;
                //$("#submitRegisterFormButton").val("Registering..");
                var registerModal = $('#registerModalEmail'),
                    name_val = registerModal.find('input[name="username"]').val(),
                    email_val = registerModal.find('input[name="email"]').val(),
                    pass_val = registerModal.find('input[name="password"]').val(),
                    photo_url,
                    random_avatar = 'http://' + TNServerAddress + '/images/random_avatars/' + Math.floor((Math.random()*7)+1) + '.jpg',
                    regfail;
                $('#registerEmailTicker, #registerTicker').hide();
                
                if ((typeof imageUploaded) !== "undefined") {
                    if (imageUploaded === true) photo_url = $('#userPhoto').attr("src");
                    else photo_url = random_avatar;
                }
                else photo_url = random_avatar;
                if ((!name_val) || (!email_val) || (!pass_val)) {
                    $('#registerTicker').text('All Fields Are Required').show();
                    $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                    //$("#submitRegisterFormButton").val("Submit");
                    emailRegistrationInProgress = false;
                }
                else {
                    //$('#registerTicker').html('<br>');
                    TN.services.registerCustomer(email_val, pass_val, name_val, "", photo_url).done(function(msg){
                        if ((msg[0] == true) || (typeof msg[0] === "number")) {
                            TN.utils.setCookie("TNUser", email_val, null, 345600);
                            TN.services.loginCustomer('', email_val, pass_val).done(function(msg){
                                if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                                TN.services.sendBuzzRequest(email_val, "Message Board", "Q", photo_url).done(function(){
                                    // window.location.href='../' + location.search; // 
                                    _gaq.push(['_trackEvent', 'Join', 'Register', 'User: ' + email_val]);
                                    location.href="index.html";
                                });
                            });
                        }
                        else {
                            alert ("Error occured during the registration process.");
                            //$("#submitRegisterFormButton").val("Submit");
                            emailRegistrationInProgress = false;
                        }
                    }).fail(function(msg){
                        if ((msg == null) || (msg === null)) alert ("Error occured during the registration process.");
                        else {
                            jQuery.each( msg, function(key, value){ if (key == "status") regfail = value; });
                            // log(msg);
                            if (regfail == 409) {
                                $('#registerEmailTicker').text('Provided e-mail is already registered. Please try a different e-mail address.').show();
                                $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                            }
                            else if (regfail == 500) {
                                $('#registerTicker').text('All Fields Are Required').show();
                                $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                            }
                            else alert ("Error occured");
                        };
                        //$("#submitRegisterFormButton").val("Submit");
                        emailRegistrationInProgress = false;
                    });
                }
            });
                    
            function getclipThisInstHtml(){
                return ('<!--div id="clipThisLightbox-bg" ></div-->\
                        <!--div id="clipThisLightbox-panel"-->\
                        <div id="clipThisHowTo" class="reveal-modal">\
                        <div id="clipThisHeader">\
                            <div id="clipThisTitle">\
                                <h3>Clip This How To</h3>\
                            </div>\
                            <!--div id="clipThisClose">\
                                <a id="clipThisClose-panel" href="#"><img src="images/icons/close-button.png"/></a>\
                            </div-->\
                            <div><a class="close-reveal-modal"></a></div>\
                        </div>\
                        <div id="clipThisAction">\
                            <div id="clipThisButton"></div>\
                            <div id="clipThisCallToAction">\
                                <a id="clipThisAnchor" href="#" onclick="return false;" title="Clip this"><img src="images/clipthislink.png" alt="Clip This"></a>\
                                <p>Add this link to your bookmark bar</p>\
                            </div>\
                        </div>\
                        <div id="clipThisInstructions">\
                            <span>\
                            <span class="clipThisInstructionsBrowsers">Chrome, Firefox, Safari</span><br>\
                            1. Drag the "Clip This" button to your browser&apos;s toolbar<br>\
                            2. When you are browsing, click the "Clip This" button to clip a story</span>\
                            <br><br>\
                            <span class="clipThisInstructionsBrowsers">IE</span><br>\
                            <span>1. Display your Bookmarks Bar by clicking on <br>View &gt; Toolbars &gt; Bookmarks Toolbar<br>\
                            2. Add the "Clip This" button to your Bookmarks Toolbar<br>\
                            3. When you are browsing the web, click the "Clip This" button to clip a story</span>\
                        </div>\
                    <!--/div--> \
                    <!--/div-->'
                );
            }
            
            $('body').append(getclipThisInstHtml());

            $('#addClipThis').click(function(){
                if( TN.header.isGuestUser() ) return;

                $('#clipThisAnchor').attr('href', "javascript:void((function(){var s=document.createElement('script');s.type='text/javascript';s.src='http://"+TNServerAddress+"/php/clipthis.php?r='+Math.random()*99999999;document.body.appendChild(s)})())");
                $('#clipThisHowTo').css('padding', '0');
                $('#clipThisHowTo').reveal();
            });
            
            // usage: log('inside coolFunc',this,arguments);
            // http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
            window.log = function(){
              log.history = log.history || [];   // store logs to an array for reference
              log.history.push(arguments);
              if(this.console){
                console.log( Array.prototype.slice.call(arguments) );
              }
            };
            
            var pollNotifications = function() {
                if (notificationsLoaded) {
                    TN.services.getNotificationTrayDetails(custId, 1, 1).done(function(json){
                        if (json[0].totalRecords > notificationsNum) {
                            var incomingNotifsN = parseFloat(json[0].totalRecords) - parseFloat(notificationsNum);
                            loadNotifications(1, false, true, incomingNotifsN);
                            uncheckedNotifs += incomingNotifsN;
                            $('#id_newNotifiactionsNum').text('['+uncheckedNotifs+']');
                            // log("incomingNotifsN: "+incomingNotifsN+" uncheckedNotifs: "+uncheckedNotifs);
                        }
                    });
                }
                setTimeout(pollNotifications, 60000);
            };

            setTimeout(pollNotifications, 60000);
            
            //log('0');
            // Check to see if we are logged-in on server side
            TN.services.pingServer().done(function(){
                //log('1');
                if (!!custId ) {
                    //log('2');
                    // Everything ok, considered logged in at this point  
                    //log('calling loadUserInfo with custId: ' + custId);
                    //log(typeof jQuery);
                    $header.userInfoFetch = TN.services.loadUserInfo(custId, custId);
                    $header.userInfoFetch.done(function(msg){
                        TN.userInfo = msg[0];
                        // $('#myNamePage').find('a').text(TN.userInfo.firstName + "'s Page");
                        TN.utils.setCookie('TNUserUrl', msg[0].profileThumbPicUrl, null, 345600);
                        TN.utils.setCookie('TNUserName', msg[0].firstName, null, 345600);
                    });
                    $('#postStoryHeaderDropdown, #myPageHeaderDropdown, #notificationsList').show();
                    if( custId !== TN.guestUserName ){
                        // $('#signInOutLink').text('Sign out');
                        //$('.signInOutParentLi').css('width', 'auto');
                    }
                    else {
                        //$('#signInOutLink').text('Sign in');
                        $('#signOut').hide();                        
                    }
                    $.getScript("javascripts/socketClient.js");
                    $.getScript("javascripts/lightbox-tool.js");
                    TN.services.keepThisSessionAlive();
                    loadNotifications(1);
                }
                else {
                	if( !imageSrc ){
                    	loginAsGuest();                		
                	}
                	
//                    $('#postStoryHeaderDropdown, #myPageHeaderDropdown, #notificationsList').hide();
                    //log('3');
//                    if (!TN.homepage) parent.location.href = 'index.html';
                    //$('#signInOutLink').text('Sign in');
                    $('#signOut').hide();
//                    TN.userInfo = {};
                }
            }).fail(function(){
                //log('4');
                if (!TN.homepage) parent.location.href = 'index.html';
                document.cookie = 'TNUser=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                document.cookie = 'TNUserNum=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                document.cookie = 'TNUserUrl=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                document.cookie = 'TNUserName=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                $('#postStoryHeaderDropdown, #myPageHeaderDropdown, #notificationsList').hide();
                TN.userInfo = {};
                custId = null;
                //$('#signInOutLink').text('Sign in');
                $('#signOut').hide();
            	if( !imageSrc ){
                	loginAsGuest();                		
            	}
            });
            //log('5');
            //alert(5);
            //console.log(imageSrc);
            if( !!imageSrc ){
                //alert(6);
                TN.createStory.show(imageSrc, storyUrl, true);
            };

            //alert(7);
            if( !!signIn ){
            	$('#signInRegisterModal').reveal();
            }
            
            if( !!lightboxId ){
            	TN.lightbox.show(custId, lightboxId);            
            }
        
            if( !!npId ){
            	TN.newspaperViewLB.init(npId);
            }
        }
        else {
            //alert('8, attempt: ' + attempt);
            //console.log('reattempting '+attempt);
            if (attempt < maxAttempts) {
                setTimeout(function(){
                    TN.header.initialize();
                }, 2500);
            };
        };

    };
    
})(TN.header);