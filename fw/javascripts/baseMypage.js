if(!TN) var TN = {};
if (!TN.baseMypage) TN.baseMypage = {};

(function($baseMypage){

	// My page left column handling code:
    $baseMypage.leftColInit = function(){

    	TN.header.initialize();

    	//var $container = $('#container');
		var isFriend = false;
		var isFollowed = false;
		var pendingFriendship = false;
		var offeringFriendship = false;
		var previouslyPlacedMessagesCount = 0;
		var qTypeMessageId, numberOfComments;
		var populatingFriendsList, populatingFollowersList, populatingFollowingList;

		TN.baseMypage.leftColInit.unFollow = function(unfollow_id, deletion_elem){
		    $('#followUnfollowBtn').unbind();
		    TN.services.unFollow(unfollow_id).done(function(msg){
		        isFollowed = false;
		        // In case we are unFollowing from a list of people, we use this to 
		        // remove respective parent li; otherwise elem null is used:
		        $(deletion_elem).parent().parent().parent().parent().parent().fadeOut('fast', function(){
		        	$(deletion_elem).parent().parent().parent().parent().parent().remove();
		        });
		        $('#followUnfollowBtn').click(function(){ follow(viewId); });
		        $('#followUnfollowBtn').text("Follow");
		        if (viewId == custId) {
		            $('#followingCount').text( parseInt($('#followingCount').text())-1 );
		            $('#followingList li#connection_'+unfollow_id).remove();
		        }
		        else {
		            $('#followersCount').text( parseInt($('#followersCount').text())-1 );
		            $('#followersList li#connection_'+unfollow_id).remove();
		        }
		    });
		};

		TN.baseMypage.leftColInit.viewedUserInfo = TN.services.loadUserInfo(viewId, custId);
		TN.baseMypage.leftColInit.viewedUserInfo.done(function(msg){
			TN.viewedUserInfo = msg[0];
		});
		
		var setProfileImg = function(){
			var d = new Date();
			TN.baseMypage.leftColInit.viewedUserInfo.done(function(){
				var tempImg = new Image();
				tempImg.onload = function(){
					//$('.userPic').css('max-width', '150px');
					//$('.userPic').css('max-height', '150px');
					$('.userPic').attr('src', TN.viewedUserInfo.profileThumbPicUrl + '&' + d.getTime());
				};
				tempImg.src = TN.viewedUserInfo.profileThumbPicUrl;
			});
		};

		var setNewProfileImg = function(){
			var d = new Date();
			TN.services.loadUserInfo(custId, custId).done(function( msg ) {
				var tempImg = new Image();
				tempImg.onload = function(){
					//$('.userPic').css('max-width', '150px');
					//$('.userPic').css('max-height', '150px');
					$('.userPic').attr('src', msg[0].profileThumbPicUrl + '&' + d.getTime());
				};
				tempImg.src = msg[0].profileThumbPicUrl;
			});
		};

		// callback for ajaxSubmit:
		var showImageUrlResponse = function(responseText, statusText, xhr, $form) {
		    //$('.userPic').attr('src', responseText);
		    TN.services.updateUserInfo(responseText).done(function(msg){
		        //console.log(msg[0]);
		        if (msg[0] == true) {
		            //$('#fakeUploadButton').text("upload ok");
		            setNewProfileImg();
		        }
		    }).always(function(){
		        if($.browser.mozilla) {
			        $('.userPic').click(function(){
			            $('#user_image_file_input').click();
			        });
		        }
		        //setTimeout(function() { $('#fakeUploadButton').text("edit photo"); }, 3000);
		    });
		};

		var placeBubbles = function(jqXHR, s ){
			testXHR = jqXHR;
		    qTypeMessageId = jqXHR[0].fittingRoomSummaryList[0].messageId;
		    numberOfComments = jqXHR[0].fittingRoomSummaryList[0].comments;
		    //userId = jqXHR[0]['fittingRoomSummaryList'][0].wsBuzz.custid;
		    if (numberOfComments == 0) {
		        //$('.userMsg').hide();
		        return;
		    }
		    else return placeMessages(jqXHR[0]['fittingRoomSummaryList'][0]['messageResponseList'], "#mustacheMessageBubbles");
		};

		var refreshBubbles = function(){
			$.ajaxSetup({ cache: false, dataType: 'json' });
		    TN.services.getMyRequestSummaries(viewId, 1, 1, "Q").done(placeBubbles);
		    $('#messageTextarea').val("");
		    //$('#userMsg').show();
		};

		var placeMessages = function(d, listId){
		    var placedMessagesCount = 0;
		    var delta = 0;
		    $.each(d, function(i,listItem) {
		        if (listItem.responseMessageBody == "") return;        
		        placedMessagesCount++;
		        if (placedMessagesCount > previouslyPlacedMessagesCount) {
		            var template = $('#messageBubble').html();
		            
		            var viewParams = {
		                'bubble_msg_text' : listItem.responseMessageBody,
		                'bubble_msg_uname' : listItem.responseCustomerName,
		                'bubble_msg_time' : listItem.elapsedTime,
		                'display_state' : ((placedMessagesCount < (numberOfComments-1)) ? "none" : "")
		            };
		            $(listId).prepend(Mustache.to_html(template, viewParams));
		            delta++;
		        }
		    });
		    previouslyPlacedMessagesCount += delta;
		    //$('.userMsg').show();
		    $('#newMessagesLabel').show();
		    if ($('#mustacheMessageBubbles div.userMessage').length > 2) $('#showAllMessages').show();
		    else $('#showAllMessages').hide();
		    
//		    skimlinks();
		};

		var addComment = function(custId, msgId, commentText, successCallback){
		    $.ajax({
		        type:'POST',
		        cache:false,
		        data: {
		            action : "addFittingRoomResponse",
		            custid : custId,
		            msgresponse : JSON.stringify({
		                "type" : "Q", 
		                responseWords : commentText,
		                "messageId" : msgId,
		                isFavour : ""
		            })
		        },
		        url:'/salebynow/json.htm',
		        success:refreshBubbles,
		        error : function( jqXhr, textStatus, errorThrown ){
		            alert(errorThrown);
		        }
		    });
		};

		var placeFriends = function(jqXHR, s ) {
			return placeConnections(jqXHR, "#User_Friends ul.no-bullet", "#friendsCount");
		};

		var placeFollowers = function(jqXHR, s) {
			return placeConnections(jqXHR, "#User_Followers ul.no-bullet", "#followersCount");
		};

		var placeFollowing = function(jqXHR, s) {
			return placeConnections(jqXHR, "#User_Following ul.no-bullet", "#followingCount");
		};

		var placeConnections = function(d, listId, countId){
		    var count = 0;
			$.each(d, function(i,listItem) {
			    // filter some things out from third person view:
			    if (viewId != custId) {
		    		if (listId == "#User_Friends ul.no-bullet") {
		    		    if (listItem.requestType == "acceptORreject") return;
		                if (listItem.requestType == "pending") return;
		    		}
		        }
				if (listItem.imageUrl == "") return;
				if ( (listItem.requestType != "acceptORreject") && (listItem.requestType != "pending") ) count++;
				var template = $('#connectionItem').html();
				var viewParams = {
					'image_url' : listItem.thumbImageUrl,
					'user_id' : listItem.id
				};
				// Limitation for friends listings: 11; for other types of listings: 5
				if ( listId == "#User_Friends ul.no-bullet" ) {
					if( count < 12) {
						$(listId).append(Mustache.to_html(
							template,
							viewParams
						));	
					}
				}
				else {
					if( count < 6) {
						$(listId).append(Mustache.to_html(
							template,
							viewParams
						));	
					}
				}
					
			});
			if ((viewId != custId) && (listId == "#User_Friends ul.no-bullet")) {
			    if (count > 0) $(countId).text(count);
			    if (count == 0) $('#User_Friends').parent().parent().hide();
			}
		    else if (d.length > 0) $(countId).text(d.length);
		};

		var placeFriendsList = function(jqXHR, s ) {
		    $("#userFriendsListing ul.no-bullet").html("");
		    populatingFriendsList = true; populatingFollowersList = false; populatingFollowingList = false; 
		    return placeUserList(jqXHR, "#userFriendsListing ul.no-bullet");
		};

		var placeFollowersList = function(jqXHR, s ) {
		    $("#userFollowersListing ul.no-bullet").html("");
		    populatingFollowersList = true; populatingFriendsList = false; populatingFollowingList = false; 
		    return placeUserList(jqXHR, "#userFollowersListing ul.no-bullet");
		};

		var placeFollowingList = function(jqXHR, s ) {
		    $("#userFollowingListing ul.no-bullet").html("");
		    populatingFollowingList = true; populatingFriendsList = false; populatingFollowersList = false; 
		    return placeUserList(jqXHR, "#userFollowingListing ul.no-bullet");
		};

		var placeUserList = function(d, listId) {
		    $.each(d, function(i,listItem) {
		        // filter out 2 kinds of things from third person friends listing:
		        if ( (viewId != custId) && (listId == "#userFriendsListing ul.no-bullet") ){
		        	if ( (listItem.requestType == "acceptORreject") || (listItem.requestType == "pending") ) return;
		        }

		        var template = $('#userListing').html();
		        var viewParams = {
		            'user_id' : listItem.id,
		            'image_url' : (listItem.thumbImageUrl ? listItem.thumbImageUrl : "/images/user-no-photo.png"),
		            'firstname' : listItem.firstname,
		            'lastname' : listItem.lastname,
		            'user_city' : "",
		            'user_state' : "",
		            'user_country' : ""
		        };

		        if (populatingFollowersList) $.extend(viewParams, { 'display_btnFollow' : 'none' });
		        else if (populatingFriendsList) {
		            if (listItem.requestType == "pending") {
		                $.extend(viewParams, { 'listing_status' : 'pending' });
		                $.extend(viewParams, { 'listing_action' : '' });
		                $.extend(viewParams, { 'display_btnFollow' : 'inline; cursor:default' });
		            }
		            else if (listItem.requestType == "acceptORreject") {
		                $.extend(viewParams, { 'listing_status' : 'accept/reject' });
		                $.extend(viewParams, { 'listing_action' : 'location.href="mypage.html?view=' +listItem.id+ '"' });
		                $.extend(viewParams, { 'display_btnFollow' : 'inline' });
		            }
		            else $.extend(viewParams, { 'display_btnFollow' : 'none' });
		        }
		        else if ((populatingFollowingList) && (viewId == custId) && (custId !== TN.guestUserName)) {
		            $.extend(viewParams, { 'listing_status' : 'unfollow' });
		            $.extend(viewParams, { 'listing_action' : 'return TN.baseMypage.leftColInit.unFollow('+listItem.id+', this)' });
		        }

		        if (viewId != custId) $.extend(viewParams, { 'display_btnFollow' : 'none' });
		        
		        $(listId).append(Mustache.to_html(
		            template,
		            viewParams
		        ));
		    });
		};

		var follow = function(viewId) {
		    $('#followUnfollowBtn').unbind();
		    TN.services.addFollowing(viewId).done(function(msg){
		        isFollowed = true;
		        $('#followUnfollowBtn').click(function(){ TN.baseMypage.leftColInit.unFollow(viewId); });
		        $('#followUnfollowBtn').text("Following");
		        $('#followersCount').text( parseInt($('#followersCount').text())+1 );
		    });
		};

		var friendRequest = function() {
		    $('#addUnFriendBtn').unbind();
		    TN.services.addShopBeeFriends(viewId).done(function(msg){
		        $('#addUnFriendBtn').text("Friendship pending").click(function(){
		            undoFriendRequest();
		        });
		    });
		};

		var undoFriendRequest = function() {
		    if (confirm("Withdraw friend request?")) {
		        $('#addUnFriendBtn').unbind();
		        TN.services.undoFriendRequest(viewId).done(function(){
		            pendingFriendship = false;
		            $('#addUnFriendBtn').click(function(){ friendRequest(); });
		            $('#addUnFriendBtn').text("Add Friend");
		        }).fail(function(){
		            alert("Error occurred");
		        });
		    }
		};

		var unFriend = function(unfriend_id) {
		    $('#addUnFriendBtn').unbind();
		    TN.services.unFriend(unfriend_id).done(function(msg){
		        isFriend = false;
		        $('#addUnFriendBtn').click(function(){ friendRequest(); });
		        $('#addUnFriendBtn').text("Add Friend");
		        // $(elem).parent().remove();
		        $('#friendsList li#connection_'+unfriend_id).remove();
		        $('#friendsCount').text( parseInt($('#friendsCount').text())-1 );
		    });
		};

		var answerFriendship = function(answer) {
		    $('#addUnFriendBtn').unbind();
		    $('#followUnfollowBtn').unbind();
		    TN.services.answerFriendRequest(viewId, answer).done(function(){
		        if (answer == "Y") {
		            isFriend = true;
		            $('#addUnFriendBtn').click(function(){ unFriend(viewId); });
		            $('#addUnFriendBtn').text("Unfriend");
		            $('#friendsCount').text( parseInt($('#friendsCount').text())+1 );
		        }
		        else {
		            $('#addUnFriendBtn').click(function(){ friendRequest(); });
		            $('#addUnFriendBtn').text("Add Friend");
		        }
		        // restore followUnfollowBtn:
		        if (isFollowed) {
		            $('#followUnfollowBtn').click(function(){ TN.baseMypage.leftColInit.unFollow(viewId); });
		            $('#followUnfollowBtn').text("Unfollow");
		        }    
		        else {
		            $('#followUnfollowBtn').click(function(){ follow(viewId); });
		            $('#followUnfollowBtn').text("Follow");
		        }
		    }).fail(function(){
		        // alert("Error occurred");
		    });
		};

	    TN.baseMypage.leftColInit.viewedUserInfo.done(function(){
	        $(".my-page-name h3").html(TN.viewedUserInfo.firstName + ( ( TN.viewedUserInfo.lastName == "Lastname" ) ? "" : "<br>" + TN.viewedUserInfo.lastName ) );
	        //$(".userPic").attr("src", TN.viewedUserInfo.profileThumbPicUrl);
	        setProfileImg();
	        $('#User_Friends h4, #userFriendsListing h2').text(TN.viewedUserInfo.firstName + '\'s Friends');
	        $('#User_Followers h4, #userFollowersListing h2').text(TN.viewedUserInfo.firstName + '\'s Followers');
	        $('#User_Following h4, #userFollowingListing h2').text(TN.viewedUserInfo.firstName + '\'s Following');
	        // If loading others' my page:
	        if (viewId != custId) {
	            if (TN.viewedUserInfo.isFollowing == "Y") isFollowed = true;
	            if (TN.viewedUserInfo.isFriend == "Y") isFriend = true;
	            if (TN.viewedUserInfo.friendRequestType == "pending") pendingFriendship = true;
	            if (TN.viewedUserInfo.friendRequestType == "acceptOReject") offeringFriendship = true;
	            $('.userPic, .my-page-name').css('cursor', 'default');

	            // Initial buttons setup:
	            if (isFriend) {
	                $('#addUnFriendBtn').click(function(){
	                	if( TN.header.isGuestUser() ) return;
	                	unFriend(viewId); 
	                });
	                $('#addUnFriendBtn').text("Unfriend");
	            }
	            else {
	                if (offeringFriendship) {
	                    $('#addUnFriendBtn').text("Accept Friendship").click(function(){
	                    	if( TN.header.isGuestUser() ) return;
	                        answerFriendship("Y");
	                    });
	                    $('#followUnfollowBtn').text("Reject Friendship").click(function(){
	                    	if( TN.header.isGuestUser() ) return;
	                        answerFriendship("N");
	                    });
	                }
	                else if (pendingFriendship) {
	                    $('#addUnFriendBtn').text("Friendship pending").click(function(){
	                    	if( TN.header.isGuestUser() ) return;
	                        undoFriendRequest();
	                    });
	                }
	                else {
	                    $('#addUnFriendBtn').click(function(){ 
	                    	if( TN.header.isGuestUser() ) return;
	                    	friendRequest(); 
	                    });
	                    $('#addUnFriendBtn').text("Friend " + TN.viewedUserInfo.firstName);
	                }
	            }
	            if (!offeringFriendship) {
	                if (isFollowed) {
	                    $('#followUnfollowBtn').click(function(){
	                    	if( TN.header.isGuestUser() ) return;
	                    	TN.baseMypage.leftColInit.unFollow(viewId); 
	                    });
	                    $('#followUnfollowBtn').text("Following");
	                }    
	                else {
	                    $('#followUnfollowBtn').click(function(){
	                    	if( TN.header.isGuestUser() ) return; 
	                    	follow(viewId); 
	                    });
	                    $('#followUnfollowBtn').text("Follow " + TN.viewedUserInfo.firstName);
	                }
	            }

	            if ( TN.viewedUserInfo.friendsStatus != "Locked" ) TN.services.getFriendsList(viewId, custId).done(placeFriends).fail(function() { $('#User_Friends').hide(); });
	            else $('#User_Friends').hide();
	            TN.services.getFollowersList(viewId).done(placeFollowers).fail(function() { $('#User_Followers').hide(); });
	            if ( TN.viewedUserInfo.followingStatus != "Locked" ) TN.services.getFollowingList(viewId, custId).done(placeFollowing).fail(function() { $('#User_Following').hide(); });            
	            else $('#User_Following').hide();
	            //if ( msg[0].photosStatus != "Locked" ) TN.myPage.populateTopStoriesBar();
	            //else $('#top-stories ul').append("<li>Stories locked</li>");
	            $('#leaderboard_link').attr('href', 'leaderboard-user.html?view=' + viewId);
	        }
	        // If loading my own page:
	        else {
	            $('#findFriendsButton').show();
	            if($.browser.mozilla) {
		            $('.userPicLabel').click(function(){
		            	if( TN.header.isGuestUser() ) return;
				        $('#user_image_file_input').click();
				    });
			    }
			    else {
			    	$('.userPicLabel').click(function(event){
			    		event.preventDefault();
				        if( TN.header.isGuestUser() ) return;
				    });
			    }

			    $('.my-page-name').css('cursor', 'default');

	            TN.services.getFriendsList(viewId, custId).done(placeFriends).fail(function() { $('#User_Friends').hide(); });
	            TN.services.getFollowersList(viewId).done(placeFollowers).fail(function() { $('#User_Followers').hide(); });
	            TN.services.getFollowingList(viewId, custId).done(placeFollowing).fail(function() { $('#User_Following').hide(); });
	            //TN.myPage.populateTopStoriesBar();
	        }

	        // Initialize message board:
	        TN.services.getMyRequestSummaries(viewId, 1, 1, "Q").done(placeBubbles).fail(function() {
	            // Try to create message board:
	            $.when(servAddressFetch).then(function() {
	                var messageBoardImage = (!!msg[0].profileThumbPicUrl) ? msg[0].profileThumbPicUrl : '/images/LoginAndRegister/uploadPhoto-img.jpg';
	                TN.services.sendBuzzRequest(viewId, 'Message Board', 'Q', 'http://' + TNServerAddress + messageBoardImage)
	                    .done(function(){
	                        TN.services.getMyRequestSummaries(viewId, 1, 1, "Q").done(placeBubbles);
	                    }).fail(function(){
	                        log("Failed to create message board (Q type story)");
	                        $('.userMsg').hide();
	                    });
	            });
	        });

	    });

		// (viewId value gets assigned custId value if viewing one's own profile)
	    if (viewId != custId) $('#thirdPersonButtons').show();

    	$('#sendMessageButton').click(function(event){
	        event.preventDefault();
	        if( TN.header.isGuestUser() ) return;
	        var message = $('#messageTextarea').val();
	        if (TN.utils.isBlank(message)) alert("Please enter a message");
	        else if (typeof qTypeMessageId === "undefined") alert("This is an old test account without messaging capabilities.");
	        else if ((viewId != custId) && !isFriend) alert("You can only leave message to friends.");
	        else addComment(custId, qTypeMessageId, message);
	    });
	    
	    $('#showAllMessages').click(function(event){
	        event.preventDefault();
	        $('#mustacheMessageBubbles').children().show();
	        $('#showAllMessages').hide();
//	        skimlinks();
	    });

		$("#User_Friends.UserAccountOpt div.row div.twelve a.right").click(function(){
			$("#userFriendsListing").reveal();
			TN.services.getFriendsList(viewId, custId).done(placeFriendsList);
		});
		
		$("#User_Followers.UserAccountOpt div.row div.twelve a.right").click(function(){
			$("#userFollowersListing").reveal();
			TN.services.getFollowersList(viewId).done(placeFollowersList);
		});

		$("#User_Following.UserAccountOpt div.row div.twelve a.right").click(function(){
			$("#userFollowingListing").reveal();
			TN.services.getFollowingList(viewId, custId).done(placeFollowingList);
		});

		/*$container.imagesLoaded(function(){
			$container.masonry({
				itemSelector : '.item',
				columnWidth : 0
			});
		});*/
	    
	    var regPhotoSubmitOptions = {
	        success: showImageUrlResponse,
	        dataType:'text'
	    };
	    
	    $("#user_image_file_input").change(function() {
	        $('.userPicLabel').unbind();
	        //$('#fakeUploadButton').text("uploading..");
	        $('#photoForm').ajaxSubmit( regPhotoSubmitOptions );
	    });

	};


	// Mypage right column handling-code
	$baseMypage.rightColInit = function(){

		//var populatingUserLikersList, populatingUserReadersList;

		TN.baseMypage.leftColInit.viewedUserInfo.done(function(){
	        $('#User_Info h4.leaveMsg').text(TN.viewedUserInfo.firstName + '\'s Info');
	        $('#User_stories h4.leaveMsg').text(TN.viewedUserInfo.firstName + '\'s Most Liked stories');
	        $('#User_Readers.UserAccountOpt h4.leaveMsg').text('Who Read ' + TN.viewedUserInfo.firstName + '\'s Work');
	        $('#User_Likers.UserAccountOpt h4.leaveMsg').text('Who Liked ' + TN.viewedUserInfo.firstName + '\'s Work');
	        // If loading others' my page:
	        if (viewId != custId) {
	            TN.services.getUsersLikedStories(viewId).done(placeLikers).fail(function() { $('#User_Likers').hide(); });
	            TN.services.getAuthorsReadStories(viewId).done(placeReaders).fail(function() { $('#User_Readers').hide(); });
	        }
	        // If loading my own page:
	        else {
	            TN.services.getUsersLikedStories(viewId).done(placeLikers).fail(function() { $('#User_Likers').hide(); });
	            TN.services.getAuthorsReadStories(viewId).done(placeReaders).fail(function() { $('#User_Readers').hide(); });
	        }
	    });
		
		var placeLikers = function(jqXHR, s) {
			if (jqXHR[0].stories.length == 0) {
				$("#User_Likers ul.no-bullet").text('No likers yet');
			}
			else {
				$('#showLike_List').show();
				return placeLikeConnections(jqXHR, "#User_Likers ul.no-bullet");
			}
		};

		var placeReaders = function(jqXHR, s) {
			if ($.isEmptyObject(jqXHR[0])) {
				$("#User_Readers ul.no-bullet").text('No readers yet');
			}
			else {
				$('#showRead_List').show();
				return placeReadConnections(jqXHR, "#User_Readers ul.no-bullet");
			}
		};

		var placeReadConnections = function(d, listId){
			var count = 0;
		    var storyReaders = [];
		    // Temp array to serve as container in which we will sort items by time
		    d[0].storiesSort = [];

		    // 10 thumbnails as preview of readers to show on Mypage
			$.each(d[0].stories, function(i,listItem) {
			   
			   	if (listItem.headline == 'Message Board') return;
				var currStoryItem = listItem;

				$.each(currStoryItem.users, function(i,listItem) {

					if (listItem.email == custId) return;

					listItem.story_id = currStoryItem.storyId;
		        	listItem.story_image_url = currStoryItem.image;
		        	listItem.headline_bottom = currStoryItem.headline;
		        	d[0].storiesSort.push(listItem);

					if ( $.inArray(listItem.uniqueId, storyReaders) == -1 ) {
						storyReaders.push(listItem.uniqueId);
						var template = $('#connectionItem').html();
						var viewParams = {
							'image_url' : listItem.thumbImageUrl, 
							'user_id' : listItem.uniqueId
						};
						if( count < 10) {
							$(listId).append(Mustache.to_html(
								template,
								viewParams
							));	
						}
						count++;
					}

				});
			});

		    d[0].storiesSort.sort(function(a,b){return b.actionDate-a.actionDate});

			// Fill readers list "Reveal holder" div.
			$.each(d[0].storiesSort, function(i,listItem) {

				if (listItem.headline == 'Message Board') return;
			        var template = $('#userReadLikeListing').html();
			        var viewParams = {
			            'user_id' : listItem.uniqueId,
			            'user_image_url' : listItem.thumbImageUrl,
			            'firstname' : listItem.firstname,
			            'lastname' : listItem.lastname,
			            'user_city' : "",
			            'user_state' : "",
			            'user_country' : "",
			            'story_id' : listItem.story_id,
			            'story_image_url' : listItem.story_image_url,
			            'headline_bottom' : listItem.headline_bottom
			        };
			        $('#userReadersListing ul.no-bullet').append(Mustache.to_html(
			            template,
			            viewParams
			        ));
		    });

		};

		var placeLikeConnections = function(d, listId){
		    var count = 0;
		    var storyLikers = [];

			$.each(d[0].stories, function(i,listItem) {

				var currStoryItem = listItem;
				$.each(currStoryItem.users, function(i,listItem) {

					if ( $.inArray(listItem.uniqueId, storyLikers) == -1 ) {

						if (listItem.email == custId) return;

						storyLikers.push(listItem.uniqueId);
						var template = $('#connectionItem').html();
						var viewParams = {
							'image_url' : listItem.thumbImageUrl, 
							'user_id' : listItem.uniqueId
						};
						if( count < 18) {
							$(listId).append(Mustache.to_html(
								template,
								viewParams
							));	
						}
						count++;
					}

				});
			});
		};

		var fillLikersReveal = function(d) {

			// Temp array to serve as container in which we will sort items by time
			d[0].storiesSort = [];

		    $.each(d[0].stories, function(i,listItem) {
		    	var currStoryItem = d[0].stories[i];
		        $.each(currStoryItem.users, function(i,listItem) {
		        	listItem.story_id = currStoryItem.storyId;
		        	listItem.story_image_url = currStoryItem.image;
		        	listItem.headline_bottom = currStoryItem.headline;
		        	if (listItem.email != custId) d[0].storiesSort.push(listItem);
		        });
		    });
			
			d[0].storiesSort.sort(function(a,b){return b.actionDate-a.actionDate});
		    
		    $.each(d[0].storiesSort, function(i,listItem){

		    	var template = $('#userReadLikeListing').html();
		        var viewParams = {
		            'user_id' : listItem.uniqueId,
		            'user_image_url' : listItem.thumbImageUrl,
		            'firstname' : listItem.firstname,
		            'lastname' : listItem.lastname,
		            'user_city' : "",
		            'user_state' : "",
		            'user_country' : "",
		            'story_id' : listItem.story_id,
		            'story_image_url' : listItem.story_image_url,
		            'headline_bottom' : listItem.headline_bottom
		        };
		        $('#userLikersListing ul.no-bullet').append(Mustache.to_html(
		            template,
		            viewParams
		        ));
		    });
		};

		/*var fillReadersReveal = function(d) {		    
		    $.each(d[0], function(index,listItem0) {
		   		// Extract story descriptor strings
				var regexp = /storyId=(.*?)\, headline/;
				var storyId = index.match(regexp)[1];
				regexp = /headline=(.*?)\, imageThumb/;
				var headline =  index.match(regexp)[1];
				regexp = /imageThumb=(.*?)\, author/;
				var storyImageThumb =  index.match(regexp)[1];

				// Iterate through particular likes for a given story..
				$.each(listItem0, function(i,listItem) {
					if (TN.utils.isBlank(listItem.thumbImageUrl)) return;
					else {
				        var template = $('#userReadLikeListing').html();
				        var viewParams = {
				            'user_id' : listItem.uniqueId,
				            'user_image_url' : listItem.thumbImageUrl,
				            'firstname' : listItem.firstname,
				            'lastname' : listItem.lastname,
				            'user_city' : "",
				            'user_state' : "",
				            'user_country' : "",
				            'story_id' : storyId,
				            'story_image_url' : storyImageThumb,
				            'headline_bottom' : headline
				        };
				        $('#userReadersListing ul.no-bullet').append(Mustache.to_html(
				            template,
				            viewParams
				        ));
				    }

			    });
		   });
		};*/

		$("#showLike_List").click(function(){
			// Not using preloaded Reveal
			$("#userLikersListing ul.no-bullet").html("");
			$("#userLikersListing").reveal(
				{"closed":function(){
//					skimlinks();
					}, 
				 "opened":function(){
//					skimlinks();
				 	}
				}
			);
			TN.services.getUsersLikedStories(viewId).done(fillLikersReveal);
		});

		$("#showRead_List").click(function(){
			// Reveal preloaded
			$("#userReadersListing").reveal(
				{"closed":function(){
//					skimlinks();
					}, 
				 "opened":function(){
//					skimlinks();
				 	}
				}
			);
			//TN.services.getAuthorsReadStories(viewId).done(fillReadersReveal);
		});

		// Display user's most liked stories:
		TN.services.getMostLikedStory(viewId).done(function(msg){
			var mostLikedStoriestemplate = $('#mostLikedStories').html();
			var numMostLikedDisplayed = 0;
			var mostLikedMaxNum = (msg.length > 5) ? 5 : msg.length;

		    msg.sort(function(a,b){return b.like_counter-a.like_counter});

		    for(i=0; i<mostLikedMaxNum; i++) {
		    	if ( (msg[i].like_counter == 0 ) || (msg[i].headline == 'Message Board') ) continue;
				else {
					var viewParams = {
						'story_headline' : !!msg[i].headline ? msg[i].headline : 'Test Headline',
						'likes_string' : (msg[i].like_counter > 1) ? msg[i].like_counter + ' Likes' : msg[i].like_counter + ' Like',
						'story_id' : msg[i].id
					};
					$('#User_stories ul.no-bullet').append(Mustache.to_html(mostLikedStoriestemplate, viewParams));
					numMostLikedDisplayed++;
				}
		    };
		    
//		    skimlinks();

		    if (numMostLikedDisplayed == 0) $('#User_stories ul.no-bullet').append('No liked stories yet');

		});

	};

}(TN.baseMypage));