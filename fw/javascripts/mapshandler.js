
(function($TN){
	
	var catType = 'Breaking News';
	var mapDissolved = false;
	var mapsHandler = {};
	(function($mapsHandler){
		var apiKey = 'AIzaSyC3JpD-ZB_NgciJjjPDaz5u9G11vs37Czc';
		var mapCanvas = $('#map_canvas');
//		var mapInset = $('#mapInset');
//		var mapCanvasLeftOffset = mapCanvas.offset().left;
//		var mapCanvasTopOffset = mapCanvas.offset().top;
		var mapCanvasObject = null;
		var trafficLayer = null;
		var canvasZoomLevel = 2;
//		var insetZoomLevel = 12;
		var mapInsetObject = null;
		var latLongArray = {};
        var prevwindow = null;
        var showHeadlines = false;
        var all=document.getElementById("all");
//        var fr=document.getElementById("friends");
//        var foll=document.getElementById("following");
//        var me=document.getElementById("me");
//        var pop=document.getElementById("popular");
//        var vid=document.getElementById("videos");
                     
        var currentfilter=all;
        $(currentfilter).css('background','#0b68a8').css('color','white');     
//		var custId = 388;
		var custId=TN.utils.getCookie('TNUser');
//		var currentOpenDetailBox = null;
		
//		var loadingInProgress = false;
//		var interruptSignal = false;
//		var interruptCompleteCallback;
			
//		var insetBorderLeftWidth = parseFloat(mapInset.css('border-left-width'));
//		var insetBorderTopWidth = parseFloat(mapInset.css('border-top-width'));
						
		var latLng = null;
		var canvasOptions = null;
//		var insetOptions = null;
                
		
		var markersArray = [];
		var markerCluster = null;
//		var localCats = [];
//		var globalCats = [];
//		var catBarElem = $('#categoriesBar');
		var catDropDown = $('#Categories');
		
		var mapFilter = 'all';
		var boundchange = 0;
        var state=mapFilter;
        var prevId=-1;
        var count=0;
        var refreshslide = 0;
        var countcurr=-1;
        var current_slider=$("#slider");
        var sliderarray=[];
        
		var currAjaxCallId = null;
		var mapEnlarged = false;
                
		function getInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content){
			
			var detailedBox = document.createElement('div');
			detailedBox.innerHTML = '<div class="inlineBox block" onclick="TN.lightbox.show(\'' + custId + '\', ' + messageId + ');">\
				<a href="#" class="inlineBox-btnClose"></a>\
				<div class="inlineBoxContainer block">\
					<div class="inlineBox-imageBlock">\
						<div class="block" >\
							<div class="inlineBox-imageWrapper">\
								<img src="' + thumbImageUrl + '" title="Story Image" >\
							</div>\
						</div>\
						<div class="inlineBox-User block">\
							<img src="' + content.originatorImageUrl + '" title="' + content.originatorName + '">\
							<h2>by <a href="#">' + content.originatorName + '</a></h2>\
							<p>' + content.elapsedTime + '</p>\
						</div>\
					</div>\
					<div class="inlineBox-textBlock block">\
						<h1>' + markerTitle + '</h1>' +
						(!!content.who ? '<p>Who: ' + content.who + '</p>' : '') +
						(!!content.what ? '<p>What: ' + content.what + '</p>' : '') +
						(!!content.where ? '<p>Where: ' + content.where + '</p>' : '') +
						(!!content.when ? '<p>When: ' + content.when + '</p>' : '') +
						(!!content.how ? '<p>How: ' + content.how + '</p>' : '') +
						(!!content.why ? '<p>Why: ' + content.why + '</p>' : '') +
						(!!content.oped ? '<p>' + content.oped + '</p>' : '') +
					'</div>\
				</div>\
			</div>';
			detailedBox.style.cssText = 'width:506px; height:280px; cursor:pointer;';
			return detailedBox;
		}
		
        function addMarker( lat, long, messageId, markerTitle, thumbImageUrl, content,update){
			// Ref code for InfoBox: http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/docs/examples.html
			
			function getInfoBoxHtml(){
				return( '<div class="infoBoxContent">\
                                                        <img class="thumbImage" src="' + thumbImageUrl + '"></img> \
							<div class="headline">' + markerTitle + '</div>\
						</div>' );
			}			
			
			var latLong = new google.maps.LatLng(lat, long);
                        
			var markersObject = createMarker(latLong, markerTitle);
			
//			var infoBoxOptions = {
//				maxWidth:0,
//				alignBottom:false,
//				pixelOffset:new google.maps.Size(5, -50),
//				boxStyle:{ background: "url('./images/icons/infobox_arrow.png') no-repeat left bottom"},
//				content: getInfoBoxHtml(),
//				disableAutoPan:true,
//				closeBoxURL:"images/icons/close-button.png",
//				closeBoxMargin:"2px 10px 0px 2px",
//				position:latLong,
//				enableEventPropagation:true
//			};
			
			var infoWindowOptions = {
				boxClass:"infoWindowDetailed",
				maxWidth:0,
				alignBottom:false,
				pixelOffset:new google.maps.Size(5, -110),
				boxStyle:{ background: "url(images/body-bg.png) no-repeat"},
				content: getInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content),
				closeBoxURL:"images/icons/close-button.png",
				closeBoxMargin:"15px 15px 2px 2px",
				disableAutoPan:true,
				position:latLong,
				enableEventPropagation:false
			};
			
//			var infoWindow = new InfoBox(infoBoxOptions);
			var infoWindowDetailed = new InfoBox(infoWindowOptions);
			
                        
            google.maps.event.addListener(mapCanvasObject,"bounds_changed",function() {
                if (countcurr===-1){
                	boundchange=1;
                	mapsHandler.interruptOrReload();
                }
                else{
                	refreshslide = 1;           
                }
                
            });
            
            google.maps.event.addListener(mapCanvasObject,'mouseup',function (){
            	if(refreshslide===1){
            		refreshslide=-1;
            		SliderReload();
            	}
            });
           
            google.maps.event.addListener(mapCanvasObject,'click',function (){
                if (prevwindow !==null){
                	prevwindow.close();
                    prevwindow=null;
                }
            });
            
            google.maps.event.addListener(mapCanvasObject,'zoom_changed',function (){
               
            	if(refreshslide===1||refreshslide===-1){
            		refreshslide=0;
            		SliderReload();
            	}
            });
               
			google.maps.event.addListener(infoWindowDetailed, 'closeclick', function(){
                prevwindow=null;
			});
			
                      
			google.maps.event.addListener(markersObject.globalMarker, 'mouseover', function(){
//				if( !showHeadlines && currentOpenDetailBox === null){
				
                    if (prevwindow !==null){
                    	prevwindow.close();
                        prevwindow=null;
                    }
					infoWindowDetailed.open(mapCanvasObject, markersObject.globalMarker);
//					skimlinks();
                    prevwindow = infoWindowDetailed;
//				}
			});
			
//			google.maps.event.addListener(markersObject.globalMarker, 'mouseout', function(){
//				if(!showHeadlines ){
//					infoWindowDetailed.close();
//				}
//				markersObject.globalMarker.setAnimation(null);
//			});
			
			
			google.maps.event.addListener(markersObject.globalMarker, 'click', function(){
				TN.lightbox.show(custId, messageId);
            });		
			
			latLongArray[lat] = {};
			latLongArray[lat][long] = {};
			latLongArray[lat][long].globalMarker = markersObject.globalMarker;
			if( !!markersObject.insetMarker ){
				latLongArray[lat][long].insetMarker = markersObject.insetMarker;
            }
			
		    latLongArray[lat][long].headline = infoWindowDetailed;
          
            if (prevwindow !==null){
              prevwindow.close();
            }
           
            var currwindow = latLongArray[lat][long];
//            currwindow.headline.open(mapCanvasObject, currwindow.globalMarker);
            prevwindow=currwindow.headline;    
        }
		
                
        function TooltipOpen(lat,long, messageId, markerTitle, thumbImageUrl, content){
            		
			function getInfoBoxHtml(){
				return( '<div class="infoBoxContent">\
                                                         <img class="thumbImage" src="' + thumbImageUrl + '"></img> \
							<div class="headline">' + markerTitle + '</div>\
						</div>' );
			}
			
			var latLong = new google.maps.LatLng(lat, long);
                        
//			function getInfoBoxDetailed(){
//				
//				var detailedBox = document.createElement('div');
//				detailedBox.innerHTML = '<div class="inlineBox block">\
//					<a href="#" class="inlineBox-btnClose"></a>\
//					<div class="inlineBoxContainer block">\
//						<div class="inlineBox-imageBlock">\
//							<div class="block" >\
//								<div class="inlineBox-imageWrapper">\
//									<img src="' + thumbImageUrl + '" title="Story Image" >\
//								</div>\
//							</div>\
//							<div class="inlineBox-User block">\
//								<img src="' + content.originatorImageUrl + '" title="' + content.originatorName + '">\
//								<h2>by <a href="#">' + content.originatorName + '</a></h2>\
//								<p>' + content.elapsedTime + '</p>\
//							</div>\
//						</div>\
//						<div class="inlineBox-textBlock block">\
//							<h1>' + markerTitle + '</h1>\
//							<p>Who: ' + content.who + '</p>\
//							<p>When: ' + content.when + '</p>\
//							<p>What: ' + content.what + '</p>\
//						</div>\
//					</div>\
//				</div>';
//				detailedBox.style.cssText = 'width:506px; height:280px;';
//				return detailedBox;
//			}
				
//			var infoBoxOptions = {
//				maxWidth:0,
//				alignBottom:false,
//				pixelOffset:new google.maps.Size(5, -50),
//				boxStyle:{ background: "url('./images/icons/infobox_arrow.png') no-repeat left bottom"},
//				content: getInfoBoxHtml(),
//				disableAutoPan:true,
//                closeBoxURL:"none",
//                closeBoxMargin:"2px 10px 0px 2px",
//			    position:latLong,
//				enableEventPropagation:true
//			};
			
			var infoWindowOptions = {
					boxClass:"infoWindowDetailed",
					maxWidth:0,
					alignBottom:false,
					pixelOffset:new google.maps.Size(5, -110),
					boxStyle:{ background: "url(images/body-bg.png) no-repeat"},
					content: getInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content),
					closeBoxURL:"images/icons/close-button.png",
					closeBoxMargin:"15px 15px 2px 2px",
					disableAutoPan:true,
					position:latLong,
					enableEventPropagation:false
				};
				
//			var infoWindow = new InfoBox(infoBoxOptions);
			var infoWindow = new InfoBox(infoWindowOptions);
	             
			return infoWindow;
                   
        }       
        
        function sliderReset(){
        	current_slider.remove();
        	$('#slideme').append('\<div id="slider" class="slider-horizontal"></div>');
        	current_slider=$('#slider');
        	prevId=-1;
        	current_slider.FlowSlider();
        	
/*			if( !!window.chrome ){
	        	current_slider.unbind('flowSliderMoveStop').bind('flowSliderMoveStop', function(slider){
	        		$('body').click();
	        	});				
			}
*/        }
        
        function SliderReload(){
                      
        	sliderReset();
	        for(var i=0;i<countcurr;i++){
	        	var MapItem=sliderarray[i];
	        	var latLong = new google.maps.LatLng(MapItem.latitude,MapItem.longitude);			
	        	if( mapCanvasObject.getBounds().contains(latLong)){
				                        
	    			var storyImageUrl = (!!MapItem.senderThumbImageUrl ?  MapItem.senderThumbImageUrl : MapItem.thumbImageUrl );
                    var SliderItem= '\<div class="item"><div class="itemcontainer" id='+i+'><img src="'+storyImageUrl+'" />\
                                      <div class="text">'+(!!MapItem.headline ? MapItem.headline : 'Test Headline') +'</div>\
                                      </div></div>';
                                      current_slider.FlowSlider().content().append(SliderItem);
                                      current_slider.FlowSlider().setupDOM();
                  
                    $('#'+i+'').click( function(){
                                      var ID = $(this).attr("id");
                                      var selecteditem=sliderarray[ID];
                                      TN.lightbox.show(custId, selecteditem.messageId);
                                                                       
                     }); 
                    
                    $('#'+i+'').hover( function() {
	                      var ID = $(this).attr("id");
//	                      if (prevId!==ID){
	                         var selecteditem=sliderarray[ID];
	                         if (prevwindow !==null){
	                        	 prevwindow.close();
	                         }
	                                								
							var markerContent = {};
							if ( !!selecteditem.wsBuzz ){
								markerContent = {										
									what: selecteditem.wsBuzz.what,
									when: selecteditem.wsBuzz.when,
									where: selecteditem.wsBuzz.where,
									who: selecteditem.wsBuzz.who,
									why: selecteditem.wsBuzz.why,
									how: selecteditem.wsBuzz.how,
									oped: selecteditem.wsBuzz.oped,
									originatorImageUrl: selecteditem.originatorImageUrl,
									originatorName: selecteditem.originatorName,
									elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 
								};
							}
							else {
		                         markerContent = {
									what: selecteditem.what,
									when: selecteditem.when,
									where: selecteditem.where,
									who: selecteditem.who,
									why: selecteditem.why,
									how: selecteditem.how,
									oped: selecteditem.oped,
									originatorImageUrl: selecteditem.customerThumbImageUrl,
									originatorName: selecteditem.firstName,
									elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 											
	 	    					};
							}
	                                              
	                         var selectedImageUrl = (!!selecteditem.senderThumbImageUrl ?  selecteditem.senderThumbImageUrl : selecteditem.thumbImageUrl );
	
	                         var currtip=TooltipOpen(selecteditem.latitude,selecteditem.longitude, selecteditem.messageId, (!!selecteditem.headline ? selecteditem.headline : 'Test Headline'),
	                        		selectedImageUrl, markerContent);
	                          //alert(latLongArray[selecteditem.latitude][selecteditem.longitude].toSource);
	                          // if(latLongArray[selecteditem.latitude][selecteditem.longitude]!=={}){
	                        var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
	                        currtip.open(mapCanvasObject, currwindow.globalMarker);
//	                        skimlinks();
	                        prevwindow=currtip; 
	                                                                                              
//	                      }
	                      prevId=ID;
                    });  
                    
                  }
	        }
        }
                
		function createMarker( latLng, markerTitle ){
			var markersObject = {};
  			var markerImage = new google.maps.MarkerImage('images/mappin.png',
                                          new google.maps.Size(29,29),
                                          new google.maps.Point(0,0),
                                          new google.maps.Point(15,29));
                                        
            var shadow = new google.maps.MarkerImage('images/mappinshadow.png',
                         new google.maps.Size(47,29),
                         new google.maps.Point(0,0),
                         new google.maps.Point(15,29));   
            var shape = {
                    coord: [28,0,28,1,28,2,28,3,28,4,28,5,28,6,28,7,28,8,28,9,28,10,28,11,28,12,28,13,28,14,28,15,28,16,28,17,28,18,28,19,28,20,28,21,28,22,28,23,28,24,28,25,28,26,28,27,28,28,0,28,0,27,0,26,0,25,0,24,0,23,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,0,5,0,4,0,3,0,2,0,1,0,0,28,0],
                    type: 'poly'
                    };             
			// Create inset map marker if inset map exists
			if( !!mapInsetObject ){
				markersObject.insetMarker = new google.maps.Marker({     
					  position: latLng,   
					  title: markerTitle,
					//  animation: google.maps.Animation.DROP,
					  icon: markerImage,
                                          shadow: shadow,
                                          shape: shape,
					  map: mapInsetObject  });
		                 //markerCluster.addMarkers(markersObject.insetMarker);		
				 //markersArray.push(markersObject.insetMarker);
			}
			
			markersObject.globalMarker = new google.maps.Marker({     
				position: latLng,   
				//animation: google.maps.Animation.DROP,
				icon: markerImage,
                                shadow: shadow,
                                shape: shape,
				map: mapCanvasObject  });
			
			markersArray.push(markersObject.globalMarker);

                             
			  // create and return global map marker so additional processing can be done for it.
			return( markersObject );
		}
		
		function getRequestSummaries(custId, numItems, pageNum, type){
			if(state!==mapFilter||boundchange === 1){
				current_slider.remove();
				$('#slideme').append('\<div id="slider" class="slider-horizontal"></div>');
				current_slider=$('#slider');
				boundchange=0;
			}
			countcurr=-1;
			state=mapFilter;
			current_slider.FlowSlider();
			
/*			if( !!window.chrome ){
	        	current_slider.unbind('flowSliderMoveStop').bind('flowSliderMoveStop', function(slider){
	        		$('body').click();
	        	});				
			}
*/                   
			if( mapFilter === 'all' ){
				return(TN.services.getAllRequestSummaries(custId, numItems, pageNum, type));
			}
			
			if( mapFilter ==='me' ){
				return(TN.services.getMyRequestSummaries(custId, numItems, pageNum, type));
			}
			
			if( mapFilter ==='friends' ){
				return(TN.services.getFriendsRequestSummaries(custId, numItems, pageNum, type));
			}
						
			if( mapFilter ==='following' ){
				return(TN.services.getFollowingRequestSummaries(custId, numItems, pageNum, type));
			}      
		}
		
		function loadCategories() {
			return(TN.services.getAllMessageTypeActiveCategories().done(function(json){
				if( !!json ){
					var maxItems = json.length;
					catDropDown.attr('disabled', 'disabled');
					for( var i = 0; i < maxItems; i++ ){
						var catLabel = TN.categories.getLabel(json[i].id);
						
						//if label is valid
						if( !!catLabel ){
							if( json[i].global === 1 ){
								catDropDown.append('<option value="' + json[i].id + '">' + catLabel + '</option>').trigger('change');
							} else {
								if( json[i].global === 0 ){
									catDropDown.append('<option value="' + json[i].id + '">' + catLabel + '</option>').trigger('change');
								}
							}
						}
					}
					
					catDropDown.removeAttr('disabled');
					catDropDown.val('Breaking News').trigger('change');
					catDropDown.change(function(){
						var selectedCat = catDropDown.val();
						if( !!selectedCat ){
							$('#newsmap-search-input').val('');
							var currCat = selectedCat;
							TN.loadCategoryOnMap(currCat);
						}
					});
					
				}
				
			}));
		}
		
//		function moveLocalToMain(){
//			mapCanvasObject.setZoom(insetZoomLevel);
//			mapCanvasObject.setCenter(latLng);
//			mapInsetObject.setZoom(canvasZoomLevel);
//                        boundchange=1;
//                        mapsHandler.interruptOrReload();	
//                	google.maps.event.addListenerOnce(mapInsetObject, 'click', moveGlobalToMain);
//			
//			populateCatBar(localCats);
//		}
		
//		function moveGlobalToMain(){
//			mapCanvasObject.setZoom(canvasZoomLevel);
//			mapCanvasObject.setCenter(latLng);
//			
//			mapInsetObject.setZoom(insetZoomLevel);
//			boundchange=1;
//			mapsHandler.interruptOrReload();			
//			
//			google.maps.event.addListenerOnce(mapInsetObject, 'click', moveLocalToMain);
//			
//			populateCatBar(globalCats);
//		}
		
//		function populateCatBar(catArray){
//			var numCats = catArray.length;
//			
//			if( numCats >  0){
//				catBarElem.empty();
//				for (var i = 0; i < numCats; i++){
//					catBarElem.append('<li><a href="#catOnBar" onclick="TN.loadCategoryOnMap(\'' + catArray[i] + '\')" >' + catArray[i] + '</a></li>');
//				}
//			}
//		}
		
		function resetAndReload(searchOptions){
//			interruptSignal = false;			
			if (markersArray) {
				for (i in markersArray) {
					markersArray[i].setMap(null);    
				}    
				markersArray.length = 0;  
			}
			latLongArray = {};
			markerCluster.clearMarkers();
            if (prevwindow !==null){
                prevwindow.close();
                prevwindow = null;
              }
			sliderReset();
        	
			setMarkers(searchOptions);
		}
            
		function crop(crop_id, x, y, width, height) {
                       
            var scale_x = (crop_id).attr('width') / width;
            var scale_y = (crop_id).attr('height') / height;

            (crop_id).css({
            	position: 'relative',
                overflow: 'hidden' 
            });

            (crop_id).css({
                 position: 'absolute',
                 display: 'block',
                 left: (-x * scale_x) + 'px',
                 top: (-y * scale_y) + 'px',
                 width: (crop_id.attr('width') * scale_x) + 'px',
                 height: (crop_id.attr('height') * scale_y) + 'px'
            });
		}
		
		function setMarkers(options){
			var pageNum = 1;
			var maxPages = 1;
			
//			loadingInProgress = true;
			function loadMarkers( json, ourAjaxCallId  ){
//				if( !interruptSignal ){
				if( json && json[0] && currAjaxCallId === ourAjaxCallId ){
					maxPages = parseFloat( json[0].noOfPages );
					if( json[0].fittingRoomSummaryList ){
						var numItems = json[0].fittingRoomSummaryList.length;
						var currItem=null;
						
						for( var i = 0; i<numItems; i++ ){
							currItem = json[0].fittingRoomSummaryList[i];
							if( !!currItem.latitude && !!currItem.longitude && !!currItem.messageId){
								if( !latLongArray ){
									latLongArray = {};
								}
								latLongArray[currItem.latitude] = {};
								latLongArray[currItem.latitude][currItem.longitude] = {};
								latLongArray[currItem.latitude][currItem.longitude].messageId = currItem.messageId;
								
								var markerContent = {
									what: currItem.wsBuzz.what,
									when: currItem.wsBuzz.when,
									where: currItem.wsBuzz.where,
									who: currItem.wsBuzz.who,
									why: currItem.wsBuzz.why,
									how: currItem.wsBuzz.how,
									oped: currItem.wsBuzz.oped,
									originatorImageUrl: currItem.originatorImageUrl,
									originatorName: currItem.originatorName,
									elapsedTime: !!currItem.wsMessage ? currItem.wsMessage.elapsedTime : '' 
								};
								
								var latLong = new google.maps.LatLng(currItem.latitude,currItem.longitude);			
//								if( mapCanvasObject.getBounds().contains(latLong)){
									addMarker(currItem.latitude,currItem.longitude, currItem.messageId, 
										(!!currItem.headline ? currItem.headline : 'Test Headline'),
										currItem.senderThumbImageUrl, markerContent,0);								
				                              //add here
									var SliderItem= '\<div class="item"><div class="itemcontainer" id='+count+'><img src="'+currItem.senderThumbImageUrl+'" />\
										<div class="text">'+(!!currItem.headline ? currItem.headline : 'Test Headline') +'</div>\
										</div></div>';
									current_slider.FlowSlider().content().append(SliderItem);
									current_slider.FlowSlider().setupDOM();
									sliderarray[count]=currItem;      
                                                                  
									$('#'+count+'').click( function(){
                                       var ID = $(this).attr("id");
                                       var selecteditem=sliderarray[ID];
                                       TN.lightbox.show(custId, selecteditem.messageId);                                                                       
									}); 
                                                                  
									$('#'+count+'').hover( function() {
										var ID = $(this).attr("id");
//										if (prevId!==ID){
										
										var selecteditem=sliderarray[ID];
										markerContent = {
											what: selecteditem.wsBuzz.what,
											when: selecteditem.wsBuzz.when,
											where: selecteditem.wsBuzz.where,
											who: selecteditem.wsBuzz.who,
											why: selecteditem.wsBuzz.why,
											how: selecteditem.wsBuzz.how,
											oped: selecteditem.wsBuzz.oped,
											originatorImageUrl: selecteditem.originatorImageUrl,
											originatorName: selecteditem.originatorName,
											elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 
										};
											
										if (prevwindow !==null){
											prevwindow.close();
										}
										var currtip=TooltipOpen(selecteditem.latitude,selecteditem.longitude,selecteditem.messageId, (!!selecteditem.headline ? selecteditem.headline : 'Test Headline' ),
											selecteditem.senderThumbImageUrl, markerContent);
										//alert(latLongArray[selecteditem.latitude][selecteditem.longitude].toSource);
										// if(latLongArray[selecteditem.latitude][selecteditem.longitude]!=={}){
										var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
										currtip.open(mapCanvasObject, currwindow.globalMarker);
//											skimlinks();
										prevwindow=currtip;                                           
											//}                                 
//										}
										prevId=ID;
									});
                                   
                                   //crop($('#'+count+''), 0, 0, 223, 150);
                                      
                                   /*   $('#'+count+'').hover( function() {
                                          Todo   
                                         });
                                     */
									count=count+1;
                                                    
//								}
							}
						}
                                                             
                                    
						if( pageNum < maxPages ){
							pageNum++;
							getRequestSummaries(custId, 25, pageNum, catType).
								done( function( data ){
									loadMarkers(data, ourAjaxCallId);
								});
//								.fail(function(){
//									loadingInProgress = false;
//									if( interruptSignal ){
//										resetAndReload();
//									}
//								});
							//  markerCluster.addMarkers(markersArray);
							//    alert('mugga');
						}
						else {  
							countcurr=count;
							count=0;
							markerCluster.addMarkers(markersArray);
							mapCanvasObject.setZoom(canvasZoomLevel);
							if( !!prevwindow ){
								prevwindow.open(mapCanvasObject, prevwindow.globalMarker);
//								skimlinks();
							}
							// markerCluster = new MarkerClusterer(mapCanvasObject,[], markersArray);				       
//							loadingInProgress = false;
						}
					}
				}				
//				}
//				else {  
//					count=0;
//					loadingInProgress = false;
//					resetAndReload();
//				}
            }

            function loadSearchMarkers( json, ourAjaxCallId ){
//				if( !interruptSignal ){
				if( json && json[0] && currAjaxCallId === ourAjaxCallId ){
					var numItems = json.length;
					var currItem = null;
					
                    for( var i = 0; i<numItems; i++ ){
						currItem = json[i];
						if( !!currItem.latitude && !!currItem.longitude && !!currItem.messageId){
							if( !latLongArray ){
								latLongArray = {};
							}
							latLongArray[currItem.latitude] = {};
							latLongArray[currItem.latitude][currItem.longitude] = {};
							latLongArray[currItem.latitude][currItem.longitude].messageId = currItem.messageId;
							
							var markerContent = {
								what: currItem.what,
								when: currItem.when,
								where: currItem.where,
								who: currItem.who,
								why: currItem.why,
								how: currItem.how,
								oped: currItem.oped,
								originatorImageUrl: currItem.customerThumbImageUrl,
								originatorName: currItem.firstName,
								elapsedTime: !!currItem.wsMessage ? currItem.wsMessage.elapsedTime : '' 
							};
				            var latLong = new google.maps.LatLng(currItem.latitude,currItem.longitude);			
					        
//					        if( mapCanvasObject.getBounds().contains(latLong)){
		                                      
								addMarker(currItem.latitude,currItem.longitude, currItem.messageId, 
								(!!currItem.headline ? currItem.headline : 'Test Headline'),
								currItem.thumbImageUrl, markerContent,0);								
								//add here
								var SliderItem= '\<div class="item"><div class="itemcontainer" id='+count+'><img src="'+currItem.thumbImageUrl+'" />\
									<div class="text">'+(!!currItem.headline ? currItem.headline : 'Test Headline') +'</div>\
									</div></div>';
								current_slider.FlowSlider().content().append(SliderItem);
								current_slider.FlowSlider().setupDOM();
								sliderarray[count]=currItem;      

								$('#'+count+'').click( function(){
									var ID = $(this).attr("id");
									var selecteditem=sliderarray[ID];
									TN.lightbox.show(custId, selecteditem.messageId);
								}); 

								$('#'+count+'').hover( function() {
									var ID = $(this).attr("id");
//									if (prevId!==ID){
									
									var selecteditem=sliderarray[ID];
									markerContent = {
										what: selecteditem.what,
										when: selecteditem.when,
										where: selecteditem.where,
										who: selecteditem.who,
										why: selecteditem.why,
										how: selecteditem.how,
										oped: selecteditem.oped,
										originatorImageUrl: selecteditem.customerThumbImageUrl,
										originatorName: selecteditem.firstName,
										elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 											
									};
									
									if (prevwindow !==null){
										prevwindow.close();
									}
									var currtip=TooltipOpen(selecteditem.latitude,selecteditem.longitude, selecteditem.messageId, (!!selecteditem.headline ? selecteditem.headline : 'Test Headline'),
									selecteditem.thumbImageUrl, markerContent);

									var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
									currtip.open(mapCanvasObject, currwindow.globalMarker);
//										skimlinks();
									prevwindow=currtip; 
//									}
									prevId=ID;
								});

								count=count+1;
//                        	}
                        }
            		}
                                                         
                    /*if( pageNum < maxPages ){
                    	pageNum++;
						getRequestSummaries(custId, 25, pageNum, catType).
						done(loadMarkers).
						error(function(){
							loadingInProgress = false;
							if( interruptSignal ){
								resetAndReload();
							}
						});
					}
					else {  
						countcurr=count;
						count=0;
                        markerCluster.addMarkers(markersArray);
						loadingInProgress = false;
					}*/
					countcurr=count;
					count=0;
                    markerCluster.addMarkers(markersArray);			
                    mapCanvasObject.setZoom(canvasZoomLevel);
					if( !!prevwindow ){
	                    prevwindow.open(mapCanvasObject, prevwindow.globalMarker);
//	                    skimlinks();
					}

//                    searchInProgress = false;
//					loadingInProgress = false;
				}
//				}
//				else {  
//					count=0;
//					searchInProgress = false;
//					loadingInProgress = false;
//					resetAndReload();
//				}
        	}
        	//console.log(options, options.searchStories, (options && options.searchStories));
        	
			currAjaxCallId = new Date().getTime();
			
			if (options && options.searchStories) {
				count=0;
				(function(){
					var ourAjaxCallId = currAjaxCallId;
					TN.services.searchStories(options.searchTxt, 1, 50).
					done( function(data){
						loadSearchMarkers(data, ourAjaxCallId);
					});
				})();
//					.fail(function(){
//						searchInProgress = false;
//						loadingInProgress = false;
//						if( interruptSignal ){
//							resetAndReload();
//						}
//				});
			}

			else {
				count=0;
				(function(){
					var ourAjaxCallId = currAjaxCallId;
					getRequestSummaries(custId, 25, pageNum, catType).
					done( function(data){
						loadMarkers(data, ourAjaxCallId);
					});
				})();
//				.fail(function(){
//					loadingInProgress = false;
//					if( interruptSignal ){
//						resetAndReload();
//					}
//				});	
			}

		}

//		var searchInProgress = false;
		var currentSearchString = '';

		function searchDispatch(){

            var nextSearchTxt = $('#newsmap-search-input').val();

            if ( !TN.utils.isBlank(nextSearchTxt) ) {
                
                /*$('#allCatsOption').show();
                TN.utils.setDropdownValue(catListElem, 'All Categories');*/
				
                //getSearchStories(nextSearchTxt);
                currentSearchString = nextSearchTxt;

	            // getSearchStories
//	        	searchInProgress = true;
//	        	loadingInProgress = true;

//	            TN.services.searchStories( currentSearchString, 1, 20 ).
//	                done(function(json){
	                	
	                	// To do: interrupt callbacks:
	        			/*if( loadingInProgress ){
	            			if( loadInterrupted ){
	            				loadInterrupted = false;
	            				loadingInProgress = false;
	            				if( !!interruptCompleteCallback ){
	            					interruptCompleteCallback();
	            				}
	            				return;
	            			}        				            			
	        			}
	        			else {
	        				loadingInProgress = true;
	        			}*/
	        	
	        	catDropDown.val('').trigger('change');
	        	resetAndReload({searchStories:true, searchTxt: currentSearchString});

//	                	setMarkers({searchStories:true, searchTxt: currentSearchString});
	                	
//	                }).
//	                always(function(){
	        			//loadingInProgress = false;
//                        searchInProgress = false; //('#progressIndicator').remove();
//	                }).fail(function(){
	                    //("No stories available.");
//	                    searchInProgress = false; //('#progressIndicator').remove();
//	                });
	        }
        };
		
		$mapsHandler.getApiKey = function(){
			return apiKey;
		};
				
		$mapsHandler.hide = function(){
			mapCanvas.hide();
			delete mapCanvasObject;
		};
		
		$mapsHandler.interruptOrReload = function(){
//			if( loadingInProgress ){
//				interruptSignal = true;
//			}
//			else {
				resetAndReload();
//			}
		};
		
		$mapsHandler.show = function(){
			function loadScript() {  
				var script = document.createElement('script');  
				script.type = 'text/javascript';  
				script.src = 'http://maps.googleapis.com/maps/api/js?key=' + apiKey + '&sensor=true&callback=TN.showMap';  
				document.body.appendChild(script);
			}
			
			function noGeoLocationSupport(){
				alert("Unable to determine your current location.  Your browser does not support geolocation.");
			}
			
			function noPositionError(){
				alert("Your browser is unable to determine your current location.");
			}
			
			function showGlobalMap(){
				latLng =  new google.maps.LatLng(37.441, 0);
				canvasOptions = {    
					zoom: canvasZoomLevel,    
					center: latLng,    
					mapTypeId: google.maps.MapTypeId.HYBRID};

				mapCanvas.empty();
				mapCanvasObject = new google.maps.Map(document.getElementById("map_canvas"), canvasOptions);
     /*                            var styles=[{
      url: 'images/mappin.png',
        height: 35,
        width: 35,
        opt_anchor: [16, 0],
        opt_textColor: '#FF00FF'
      },
      {
        url: 'images/mappin.png',
        height: 45,
        width: 45,
        opt_anchor: [24, 0],
        opt_textColor: '#FF0000'
      },
      {
        url: 'images/mappin.png',
        height: 55,
        width: 55,
        opt_anchor: [32, 0]
      }];*/
                                         
				var mcOptions = {gridSize: 5, maxZoom: 60};
				markerCluster = new MarkerClusterer(mapCanvasObject,mcOptions);				       
				$("#map_canvas").show();
				// Add a listener to close an open current detail box if we click outside it on the map
				// Currently disabled because it's not playing well with our drag map through infoWindowDetailed functionality
				/*google.maps.event.addListener(mapCanvasObject, 'click', function(){
					if(currentOpenDetailBox !== null){
						currentOpenDetailBox.close();
						currentOpenDetailBox = null;
					}
				});*/
				
				setMarkers();
			}
			
			function loadWeatherGadget(position){
				var openWeatherAPIKey = '1976e9a941e48ec94d00e79c050a4617';
								
				var sliceType = [];
				
				sliceType['09'] = 'Morning';
				sliceType['12'] = 'Morning';
				sliceType['15'] = 'Day';
				sliceType['18'] = 'Day';
				sliceType['21'] = 'Evening';
				sliceType['00'] = 'Evening';
				sliceType['03'] = 'Night';
				sliceType['06'] = 'Night';
				
				function getWindHtml(wind, prefix){
					
					var icon = '';
					var direction = '';
					var degree = wind.deg;

					if( degree === 0 ){
						direction = 'N;'
					}
					
					if( degree > 0 && degree < 90 ){
						direction = 'NE';
					}
					
					if( degree === 90 ){
						direction = 'E';
					}
					
					if( degree > 90 && degree < 180 ){
						direction = 'SE';
					}
					
					if( degree === 180 ){
						direction = 'S';
					}
					
					if( degree > 180 && degree < 270 ){
						direction = 'SW';
					}
					
					if( degree === 270 ){
						direction = 'W';
					}
					
					if( degree > 270 ){
						direction = 'NW';
					}
					
					if (!!prefix ){
						icon =  prefix + '-' + direction + '-' + 'directional.png';
					}
					else {
						icon =  direction + '-' + 'directional.png';
					}
					
					var windHtml = '';
					
					if( prefix === 'lrg' ){
						windHtml = '<div class="wind">\
							<p>Wind</p>\
							<div class="left">\
								<img src="images/weather/wind/' + icon + '" alt="' + direction + '">\
								<p><strong>' + direction + '</strong></p>\
							</div>\
							<div class="right">\
								<p><strong>' + Math.round(wind.speed) + 'mph</strong></p>\
							</div>\
						</div>';
					} else {
						windHtml = '<div class="wind">\
							<p><img src="images/weather/wind/' + icon + '" alt="' + direction + '">&nbsp' + Math.round(wind.speed) + '</p>\
							<p class="sw">' + direction + '</p>\
						</div>';
					}
					
					return windHtml;
					
				}
				
				function getRestOfDayHtml(daySlice, headerClassAdd){
					var forecastHtml = '';
					
					forecastHtml = '<div class="header ' + headerClassAdd + '"><p>' + sliceType[daySlice.dt_txt.split(' ')[1].split(':')[0]] + '</p></div>\
						<div class="temperature">\
							<img src="http://openweathermap.org/img/w/' + daySlice.weather[0].icon + '.png" alt="' + daySlice.weather[0].main + '">\
							<p><span class="tempNum">' + Math.round(daySlice.main.temp) + '</span> &deg;<span class="tempType">F</span></p>\
						</div>' + 
						getWindHtml(daySlice.wind, '') +
						'<div class="humidity">\
							<p>' + daySlice.main.humidity + '%</p>\
						</div>';
					
					return forecastHtml;
				}
				
				function getWeatherGadgetHtml(data){
					var gadgetHtml = '';
										
					var rightNow = data.list[0];
					var daySlice1, daySlice2, daySlice3;
					
					//determine the index for the next day slice
					var currentForecastTime = rightNow.dt_txt.split(' ')[1].split(':')[0];
					
					if( currentForecastTime === '09' || currentForecastTime === '15' || currentForecastTime === '21' || currentForecastTime === '03' ){
						daySlice1 = 2;
					}
					else {
						daySlice1 = 1;
					}
					
					daySlice2 = daySlice1 + 2;
					daySlice3 = daySlice2 + 2;
					
//					gadgetHtml = '<iframe id="weather_gadget_iframe" src="weather-gadget.html?lat=' + position.coords.latitude + '&long=' + position.coords.longitude + '"></iframe>';
					gadgetHtml = '<div class="weather-gadget twelve columns">\
						<div id="weather-app-header">\
							<p class="left curr-city">' + data.city.name + '</p>\
							<dl class="tabs pill">\
								<dd id="celcius"><a href="#pillTab1">C</a></dd>\
								<dd id="farenheit" class="active"><a href="#pillTab2">F</a></dd>\
							</dl>\
                        </div>\
						<div class="days">\
							<div class="four columns">\
								<div class="right-now">\
									<div class="sun">\
										<p>Right now</p>\
										<div class="left">\
											<img src="http://openweathermap.org/img/w/' + rightNow.weather[0].icon + '.png" alt="' + rightNow.weather[0].main + '">\
										</div>\
										<div class="right">\
											<p><strong><span class="tempNum">' + Math.round(rightNow.main.temp) + '</span> &deg;<span class="tempType">F</span></strong></p>\
										</div>\
									</div>' +
									getWindHtml(rightNow.wind, 'lrg') +
									'<div class="humidity">\
										<div class="left">\
											<p>Humidity</p>\
										</div>\
										<div class="right">\
											<p><strong>' + rightNow.main.humidity + '%</strong></p>\
										</div>\
									</div>\
								</div>\
							</div>\
							<div class="two columns">' + 
								getRestOfDayHtml(rightNow) + 
							'</div>' + 
							'<div class="two columns lb">' +
								getRestOfDayHtml(data.list[daySlice1], 'dark') + 
							'</div>\
							<div class="two columns">' + 
								getRestOfDayHtml(data.list[daySlice2]) + 
							'</div>\
							<div class="two columns lb">' + 
								getRestOfDayHtml(data.list[daySlice3], 'dark') + 
							'</div>\
						</div>\
						<div class="right settings">\
						</div>\
						<div class="right row">\
							<p>Source: <a href="http://openweathermap.org">OpenWeatherMap</a></p>\
						</div>\
					</div>';
					
					return gadgetHtml;
				}
				
				$.ajax({
					type: 'GET',
					data : {
						lat: position.coords.latitude,
						lon: position.coords.longitude,
						units:'imperial',
						APPID : openWeatherAPIKey
					},
					url : 'http://api.openweathermap.org/data/2.5/forecast',
					dataType: 'jsonp'
				}).done(function(json){
					// Insert weather display gadget for detected position
					$('#weather-app').html(getWeatherGadgetHtml(json));
					$('#farenheit').click(function(){
						$('.tempNum').each(function(){
							var jqElem = $(this);
							jqElem.text(TN.utils.toFarenheit(jqElem.text()));
						});
						$('.tempType').each(function(){
							var jqElem = $(this);
							jqElem.text('F');
						});
					});
					$('#celcius').click(function(){
						$('.tempNum').each(function(){
							var jqElem = $(this);
							jqElem.text(TN.utils.toCelcius(jqElem.text()));
						});
						$('.tempType').each(function(){
							var jqElem = $(this);
							jqElem.text('C');
						});
					});
				}).fail(function(jqXHR, textStatus, errorThrown){
					alert('There was an error in retrieving weather information at this time for your location.');
				});
				
			}
			
			function centerToUserLocation(position){
				
				if( !!position && !!position.coords && !!position.coords.latitude && !!position.coords.longitude ){
					delete latLng;

					loadWeatherGadget(position);
					
					latLng =  new google.maps.LatLng(position.coords.latitude, 
							position.coords.longitude);
					
					mapCanvasObject.setCenter(latLng);
				}
			}
			
//			function showInsetMap(position) {  
//				if( !!position && !!position.coords && !!position.coords.latitude && !!position.coords.longitude ){
//					delete latLng;
//					
//					latLng =  new google.maps.LatLng(position.coords.latitude, 
//							position.coords.longitude);
//					
//					insetOptions = {
//						zoom:insetZoomLevel,
//						center:latLng,
//						mapTypeControl:false,
//						panControl:false,
//						rotateControl:false,
//						scrollwheel:false,
//						streetViewControl:false,
//						zoomControl:false,
//						draggable:false,
//						tilt:45,
//						mapTypeId:google.maps.MapTypeId.ROADMAP
//					};
//
//					var insetLeftOffset = mapCanvas.offset().left + mapCanvas.width()-mapInset.width()-insetBorderLeftWidth;
//					//var insetTopOffset = mapCanvas.offset().top + mapCanvas.height()-mapInset.height()-insetBorderTopWidth;
//					var insetTopOffset = -mapInset.height()-insetBorderTopWidth;
//					
//					$('#mapFilterBar').css('position', 'relative').css('top',-183).css('float','right').show();
//                                        
//					mapInset.css('top', insetTopOffset-25).css('position', 'relative').css('float','right').show();
//					$('#slideme').css('position','relative').css('top',-200).show();
//					$('#foot').css('position','relative').css('top',-60).css('padding',0).show();
//  
//				       // $('#slideme').css('position', 'relative').css('top', -171).show();
//                                //	$('#features').css('position', 'relative').css('top', insetTopOffset).css('margin-bottom', insetTopOffset).show();
//					
//					mapInset.empty();
//					mapInsetObject = new google.maps.Map(document.getElementById("mapInset"), insetOptions);
//								
////					google.maps.event.addListenerOnce(mapInsetObject, 'click', moveLocalToMain);
//					
//					// reset center of global map the new latlang
//					mapCanvasObject.setCenter(latLng);
//					
//					moveLocalToMain();
//					
//					// update inset map with markers added so far in the global map
//					for( var curLat in latLongArray ){
//						for( var curLong in latLongArray[curLat] ){
//							latLongArray[curLat][curLong].insetMarker = new google.maps.Marker({     
//								  position: latLongArray[curLat][curLong].marker.getPosition(),   
//								  title: latLongArray[curLat][curLong].headline.getContent(),
//								//  animation: google.maps.Animation.DROP,
//								  map: mapInsetObject  });
//							
//							markersArray.push(latLongArray[curLat][curLong].insetMarker);
//						}
//					}
//				}
//			}
			
			$TN.showMap = function(){
				$.getScript('javascripts/libs/infobox_packed.js').done(function(){
					showGlobalMap();
					trafficLayer = new google.maps.TrafficLayer();
					
//					populateCatBar(globalCats);
										
					if( !!navigator.geolocation ){
//						navigator.geolocation.getCurrentPosition(showInsetMap, noPositionError);
						navigator.geolocation.getCurrentPosition(centerToUserLocation, noPositionError);
					}
					else {
						noGeoLocationSupport();
					}
//					var insetTopOffset = -mapInset.height()-insetBorderTopWidth;
					$('#mapFilterBar').css('position', 'relative').css('top',-20).show();
                                        //$('#slideme').css('position','relative').css('top',-60).show();
  
					$('#mapShowHeadlines').click(function(){
						showHeadlines = $(this).is(':checked');
						
						var curLatLong=null;
						for( var curLat in latLongArray ){
							for( var curLong in latLongArray[curLat] ){
								curLatLong = latLongArray[curLat][curLong];
								if( showHeadlines ){
									curLatLong.headline.open(mapCanvasObject, curLatLong.globalMarker);
								}
								else {
									curLatLong.headline.close();
								}
							}
						}
					});
				
					$('#traffic').click(function(){
						var jqElem = $(this);
						if( !!trafficLayer ){
							if( !!trafficLayer.getMap() ){
								trafficLayer.setMap(null);
								jqElem.removeClass('selected');
							}
							else {
								trafficLayer.setMap(mapCanvasObject);
								jqElem.addClass('selected');
							}
						}
					});
					
					$('#enlargeShrink').click(function(){
						var jqElem = $(this);
						if( mapEnlarged ){
							mapEnlarged = false;
							$('#mapContainer').removeClass('twelve').addClass('nine');
							$('#friendsStatus').show();
							google.maps.event.trigger(mapCanvasObject, 'resize');
							jqElem.css('background-image', 'url(\'images/expand-map-icon.png\')');
						}
						else {
							mapEnlarged = true;
							$('#mapContainer').removeClass('nine').addClass('twelve');
							$('#friendsStatus').hide();
							google.maps.event.trigger(mapCanvasObject, 'resize');
							jqElem.css('background-image', 'url(\'images/collapse-map-icon.png\')');
						}
					});
					
					$('body').click(function(){
		                if (prevwindow !==null){
		                	prevwindow.close();
		                    prevwindow=null;
		                }						
					});
                                        
//					$(all).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=all;
//						$(currentfilter).css('background','#0b68a8').css('color','white');
//						mapFilter = 'all';
//						mapsHandler.interruptOrReload();
//					});
//                                    
//					$(fr).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=fr;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						mapFilter = 'friends';
//						mapsHandler.interruptOrReload();
//					});
//				
//					$(foll).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=foll;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						mapFilter = 'following';
//						mapsHandler.interruptOrReload();
//					});
//					$(me).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//                                                        
//						$(currentfilter).css('background','transparent').css('color','black');                                                                                       
//						currentfilter=me;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						mapFilter = 'me';
//						mapsHandler.interruptOrReload();
//					});
//				
//					$(pop).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=pop;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						//    mapFilter = 'popular';
//						//	mapsHandler.interruptOrReload();
//					});                               
//
//					$(vid).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=vid;
//						$(currentfilter).css('background','#0b68a8').css('color','white');
//						//      mapFilter = 'video';
//						//	mapsHandler.interruptOrReload();
//					});                                
                           
					$('#newsmap-search-input').keyup(function(event){

//						event.preventDefault();
						if( event.which === 13 ){
//			            	if( loadingInProgress ){
//			            		interruptSignal = true;
//			            		interruptCompleteCallback = searchDispatch;
//			            	}
//			            	else {
			                    searchDispatch();
//			            	}
			            }
					});
					
//					$('.TNsearchBtn').click(function(){
//	                    searchDispatch();						
//					});

					mapDissolved = true;
				});
			};	
			
			loadCategories().done(loadScript);
		};
				
	}(mapsHandler));

	$TN.dissolveArtMap = function(){
		mapsHandler.show();
	};
	
	$TN.loadCategoryOnMap = function(categoryType){
		catType = escape(categoryType);
		if( !mapDissolved ){
			$TN.dissolveArtMap();
		}
		else {
			mapsHandler.interruptOrReload();
		}
	};
	
}(TN));
