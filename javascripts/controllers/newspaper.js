require('templates');

TNE = Ember.Application.create({});

TNE.NewspaperController = Ember.Controller.create({
  currentCatSelected:null,
  allMessageTypeActiveCategories:[],
  allOrMineStories:[],
  allRequestSummaries:[],
  allNewspapers:[],
  selectedNewspaperId:null,
  openPromptObject:null,
  currNewspaperId:0,
  isPreviewStory:false,
  isAllStoriesMode:true,
  currentTheme:"",
  defaultHeadline:"Your Brilliant Newspaper Title",
  defaultLocation:"Philadalphia PA",
  defaultEdition:"VOL 1 ISSUE-1",
  openPrompt:{headline:"Open"},
  themesObject:{

    "Helvetica":{

      "npHeadline":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"bold",
        "fontSize":"21px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"normal",
        "fontSize":"16px"
      },
      "storyByline":{
        "color":"black",
        "fontFamily":"Helvetica",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    },

    "Hipster":{

      "npHeadline":{
        "color":"#E34022",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"#244373",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"#E34022",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"24px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Museo Sans",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyByline":{
        "color":"#E34022",
        "fontFamily":"Museo",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    },

    "Hard News":{

      "npHeadline":{
        "color":"#244373",
        "fontFamily":"Bebas Neue",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"black",
        "fontFamily":"Open Sans Condensed",
        "fontWeight":"bold",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"#244373",
        "fontFamily":"Bebas Neue",
        "fontWeight":"bold",
        "fontSize":"24px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Open Sans Condensed",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyByline":{
        "color":"#244373",
        "fontFamily":"Open Sans Condensed",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    },

    "Sporty":{

      "npHeadline":{
        "color":"#A50F0F",
        "fontFamily":"Anton",
        "fontWeight":"bold",
        "fontSize":"50px"
      },
      "npLocAndEdition":{
        "color":"black",
        "fontFamily":"Scada",
        "fontWeight":"bold",
        "fontSize":"18px"
      },
      "storyHeading":{
        "color":"#A50F0F",
        "fontFamily":"Anton",
        "fontWeight":"bold",
        "fontSize":"24px"
      },
      "storyBody":{
        "color":"black",
        "fontFamily":"Scada",
        "fontWeight":"normal",
        "fontSize":"18px"
      },
      "storyByline":{
        "color":"#A50F0F",
        "fontFamily":"Scada",
        "fontWeight":"bold",
        "fontSize":"14px"
      }
    }
  },

  newNewspaperSetup:function(){
    var newspaperId = this.get("selectedNewspaperId"),
        self = this;

    this.get("currNewspaperId",0);
    $("#newspaper").find(".story").remove();
    $('#npHeadline').html(this.get("defaultHeadline"));
    $('#npLocation').html(this.get("defaultLocation"));
    $('#npEdition').html(this.get("defaultEdition"));

    $('#npHeadline').css("color", "#222");
    $('#npHeadline').css("font-family", "'didot','Palatino Linotype','Bodoni MT',serif cursive;");
    $('#npHeadline').css("font-size", "37px");
    $('#npHeadline').css("font-weight", "normal");

    $('#npLocation').css("color", "rgb(0, 0, 0)");
    $('#npLocation').css("font-family", "'optima', 'Candara', Helvetica Light, sans-serif");
    $('#npLocation').css("font-size", "14px");
    $('#npLocation').css("font-weight", "normal");

    $('#npEdition').css("color", "rgb(0, 0, 0)");
    $('#npEdition').css("font-family", "'optima', 'Candara', Helvetica Light, sans-serif");
    $('#npEdition').css("font-size", "14px");
    $('#npEdition').css("font-weight", "normal");
    $('li.RoundedLeft select.allNewspapers option[value="new"]').attr('selected', '');
    $('li.RoundedLeft select.allNewspapers option[value="preselectHack"]').attr('selected', 'selected');

    // Commented out for indefinite time to speed things up
    /*self.get("allMessageTypeActiveCategories").forEach(function(obj){
      if(obj.id === "Breaking News"){
        self.set("currentCatSelected", obj);
      }
    });
    self.allStories();*/
    TN.utils.passiveNotification('Create new newspaper', 'You can drag and drop story items now.');
  },

  newspaperIdObserver:function(){
    var newspaperId = this.get("selectedNewspaperId"),
        self = this;
    self.set("openPrompt",this.get("openPromptObject"));

    //Handling the new newspaper case
    if(newspaperId === "new"){
      self.newNewspaperSetup();
    }

    // Handling the newspaperId case
    if( !Ember.empty(newspaperId) && newspaperId > 0 ){
      TN.services.getNewspaper(newspaperId).done(function(data){
          self.set("currNewspaperId",newspaperId);
          self.populateNewspaper(data[0]);
      }).fail(function(jqXHR, textStatus, errorThrown){
          TN.utils.passiveNotification('Error!', 'There was an error in retrieving the newspaper: (' +  jqXHR.status + ') ' + errorThrown);        
        });
    }

  }.observes("selectedNewspaperId"),

  populateNewspaper:function(newspaper){
    if(!Ember.empty(newspaper)){
      var self = this;
      $('#npHeadline').html(newspaper.headline);
      $('#npLocation').html(newspaper.location);
      $('#npEdition').html(newspaper.edition);
      $('#npHeadline').css("color", newspaper.titleFontColor);
      $('#npHeadline').css("font-family", newspaper.titleFontStyle);
      $('#npHeadline').css("font-size", newspaper.titleFontSize);

      $('#npLocation').css("color", newspaper.locEdFontColor);
      $('#npLocation').css("font-family", newspaper.locEdFontStyle);
      $('#npLocation').css("font-size", newspaper.locEdFontSize);

      $('#npEdition').css("color", newspaper.locEdFontColor);
      $('#npEdition').css("font-family", newspaper.locEdFontStyle);
      $('#npEdition').css("font-size", newspaper.locEdFontSize);

      //First clear newspaper

      $("#newspaper").find(".story").remove();

      // And then Populate Stories!
      newspaper.stories.forEach(function(story){
        //For each story fetch all the attributes and make story template i.e. html
        var who = "",
            what = "",
            when = "",
            where = "",
            how = "",
            why = "",
            authorName = "";

        if(!Ember.empty(story.who)){
         who = '<p><b>Who</b>:&nbsp' + story.who + '</p>';
        }

        if(!Ember.empty(story.what)){
         what = '<p><b>What</b>:&nbsp' + story.what + '</p>';
        }

        if(!Ember.empty(story.when)){
         when = '<p><b>When</b>:&nbsp'+ story.when +'</p>';
        }

        if(!Ember.empty(story.where)){
         where = '<p><b>Where</b>:&nbsp' + story.where + '</p>';
        }

        if(!Ember.empty(story.how)){
         how = '<p><b>How</b>:&nbsp'+ story.how +'</p>';
        }

        if(!Ember.empty(story.why)){
         why = '<p><b>Why</b>:&nbsp'+ story.why +'</p>';
        }

        if(!Ember.empty(story.authorName)){
         authorName = '<div id="storyByline" class="storyByline itemForPaper">by '+ story.authorName +'</div>';
        }

        var storyTemplate = '<li class="story grid_3">'+
        '<img class="storyIcon" src="'+ story.buzzThumbImageUrl+'" alt="' + story.headline + '">'+

        '<div class="storyIconBigContainer"><img class="storyIconBig" src="'+ story.buzzImageUrl+'" alt="' + story.headline + '"></div>' + 

        '<h3 id="storyHeading" class="storyHeading">'+story.headline+'</h3>' +
        '<div id="storyBody" class="storyBody itemForPaper"> '
          + who + what + where + when + how + why +
          '<p>'+ story.oped +'</p>' + ' </div>' +
          authorName +
        '<a href="#" class="effect-on-hover"></a>'+
        '<input class="imageId" type="hidden" value="'+ 0 +'"/>'+
        '<input class="storyOriginalImage" type="hidden" value="'+ story.buzzImageUrl +'"/>'+
        '<input class="storyThumbImage" type="hidden" value="'+ story.buzzThumbImageUrl +'"/>'+
        '<input class="buzzId" type="hidden" value="'+ story.customerBuzzId +'"/>' + '</li>';

        // Fetch coordinates of story and then fetch corresponding container and insert story in its container
        var hashForTiles = [[0,0],[0,1,4,0,6,9,11],[0,2,5,0,7],[0,3,0,0,8,10]],
            tileId = hashForTiles[story.jrow][story.jcolumn],
            tile = $("#" + tileId),
            tileObject = {target:tile},
            storyObject = $(storyTemplate);

        // Make storyObject draggable
        storyObject.draggable({
          addClasses: false,
          appendTo: "body",
          helper: "clone",
          revert: "invalid", // when not dropped, the item will revert back to its initial position
          containment: "document",
          cursor: "move"
        });

        // Put story on tile now
        TNE.NewspaperView.putStoryOnTile(tileObject,storyObject, false);
        // apply styling and all to stories
        
        tile.find('.story .storyHeading').css("color", story.fontcolor);
        tile.find('.story .storyHeading').css("font-family", story.fontstyle);
        tile.find('.story .storyHeading').css("font-size", story.fontsize);

        tile.find('.story .storyBody p').css("color", story.bodyfontcolor);
        tile.find('.story .storyBody p').css("font-family", story.bodyfontstyle);
        tile.find('.story .storyBody p').css("font-size", story.bodyfontsize);
      });
    }
  },

  applyTheme:function(event, optionalTheme){
    var idAsIndex;
    if(Ember.empty(event)){
      idAsIndex = optionalTheme;
    }
    else{
      idAsIndex = $(event.target).attr("id");
    }

    this.set("currentTheme", idAsIndex);

    var npHead = this.get("themesObject")[idAsIndex].npHeadline;
    var npLocAndEdition = this.get("themesObject")[idAsIndex].npLocAndEdition;
    var storyHeading = this.get("themesObject")[idAsIndex].storyHeading;
    var storyBody = this.get("themesObject")[idAsIndex].storyBody;
    var storyByline = this.get("themesObject")[idAsIndex].storyByline;

    $('#npHeadline').css("color", npHead.color);
    $('#npHeadline').css("font-family", npHead.fontFamily);
    $('#npHeadline').css("font-weight", npHead.fontWeight);
    $('#npHeadline').css("font-size", npHead.fontSize);

    $('#npLocation').css("color", npLocAndEdition.color);
    $('#npLocation').css("font-family", npLocAndEdition.fontFamily);
    $('#npLocation').css("font-weight", npLocAndEdition.fontWeight);
    $('#npLocation').css("font-size", npLocAndEdition.fontSize);

    $('#npEdition').css("color", npLocAndEdition.color);
    $('#npEdition').css("font-family", npLocAndEdition.fontFamily);
    $('#npEdition').css("font-weight", npLocAndEdition.fontWeight);
    $('#npEdition').css("font-size", npLocAndEdition.fontSize);

    $('.storyTile .storyHeading').css("color", storyHeading.color);
    $('.storyTile .storyHeading').css("font-family", storyHeading.fontFamily);
    $('.storyTile .storyHeading').css("font-weight", storyHeading.fontWeight);
    $('.storyTile .storyHeading').css("font-size", storyHeading.fontSize);

    $('.storyTile .storyBody p').css("color", storyBody.color);
    $('.storyTile .storyBody p').css("font-family", storyBody.fontFamily);
    $('.storyTile .storyBody p').css("font-weight", storyBody.fontWeight);
    $('.storyTile .storyBody p').css("font-size", storyBody.fontSize);

    $('.storyTile .storyByline').css("color", storyByline.color);
    $('.storyTile .storyByline').css("font-family", storyByline.fontFamily);
    $('.storyTile .storyByline').css("font-weight", storyByline.fontWeight);
    $('.storyTile .storyByline').css("font-size", storyByline.fontSize);

  },

  applyTemplate:function(){
    //code for applying template
  },

  categoryObserver:function(){
    var category = this.get("currentCatSelected");

    if(!Ember.empty(category)){
      if(this.get("isAllStoriesMode") === true){
        this.allStories();
      }
      else{
        this.mineStories();
      }
    }

  }.observes("currentCatSelected"),

  initialise:function(){
    var self = this;
    var catDropDown = $('#Categories');
    var catListElem = $('#storyCatsDropDown');
    var usersNewspapersSelect = $('li.RoundedLeft select.allNewspapers');
    
    function loadCategories(){
    	
        var csCatDropdown = $('#storyCatsDropDown > ul');
        function addCategoryItem(itemText){
        	var categoryItemElem = null;
        	
        	if( itemText === 'All Categories' ){
            	categoryItemElem = $('<li id="allCatsOption">' + itemText + '</li>');           		
        	} else {
            	categoryItemElem = $('<li>' + itemText + '</li>');
        	}            	
        	
        	categoryItemElem.click(function(){
        		var currElem = $(this);
        		var currLabel = currElem.text();
        		var currId = TN.categories.getValue(currElem.text());
                TN.utils.setDropdownValue(catListElem, currLabel);            		
                
                self.set("currentCatSelected", {id:currId, label:currLabel});              
                
                if(this.get("isAllStoriesMode") === true){
                    this.allStories();
                  }
                  else{
                    this.mineStories();
                  }
                
        		$('body').click();
        		return false;        	
        	});
        	
        	csCatDropdown.append(categoryItemElem);
        }
                
        // addCategoryItem('All Categories');
        // TN.utils.setDropdownValue(catListElem, 'All Categories');
    	
        TN.services.getAllMessageTypeActiveCategories().done(function(json){
            if( !!json ){
                var maxItems = json.length;
                
                for( var i = 0; i < maxItems; i++ ){
                    if (json[i].id == "Q") continue;
                    if( json[i].global === 1 || json[i].global === 0 ){
						var currCat = json[i].id;
						var currCatLabel = TN.categories.getLabel(currCat);
						if( !!currCatLabel ){
	                    	addCategoryItem(currCatLabel);							
						}
                    }
                }                 
                
                TN.utils.setDropdownValue(catListElem, 'Trending News');            		
            }
        });
    }

    this.getNewspapers();
    $.ajax({
      type: 'GET',
      data: 'action=getAllMessageTypeActiveCategories',
      url : '/salebynow/json.htm',
      success:function(categories){
    	  var mappedCategories = [];
    	  var numCats = categories.length;
    	  
    	  for( var i = 0; i < numCats; i++ ){
    		  var currCatValue = categories[i].id;
    		  var currLabel = TN.categories.getLabel(currCatValue);
    		  if( !!currLabel ){
    			  catDropDown.append('<option value="' + currCatValue + '">' + currLabel + '</option>').trigger('change');
    			  mappedCategories.push({id:currCatValue,label:currLabel});
    		  }
    	  }
    	  
		  catDropDown.val('Breaking News').trigger('change');
    	  self.set("allMessageTypeActiveCategories", Ember.ArrayController.create({content:mappedCategories}));
    	  self.get("allMessageTypeActiveCategories").forEach(function(obj){
          if(obj.id === "Breaking News"){
            self.set("currentCatSelected", obj);
          }
        });
      }
    });
    
    loadCategories();
    
    $('#storiesSearch').keyup(function(event){
      if( event.which === 13 ){
        self.searchForStories();
      }
    });

    usersNewspapersSelect.click(function(event){
      if (event.currentTarget.value == "new") {
        self.newNewspaperSetup();
      }
    });

  },

  allStories:function(event){
    this.set("allOrMineStories",[]);
    this.set("isAllStoriesMode", true);
    var custId = TN.utils.getCookie('TNUser'),
    	totalPages = 0,
        pageNum = 1,
        category,
        numItems = 25,
        self = this,
        storiesArray = [];

    function getItems(){
        TN.services.getAllRequestSummaries(custId, numItems, pageNum, category).
      	done(function(data){
      		totalPages = data[0].noOfPages;
            $("#allStories").css("background", "rgb(236,96,63)");
            $("#mine").css("background", "");
            $('#storiesSearch').val("");
            storiesArray = storiesArray.concat(data[0].fittingRoomSummaryList);
            
            pageNum++;
            if( pageNum <= totalPages ){
            	getItems();
            }
            else{
                self.set("allOrMineStories", storiesArray);            	
            }
      	});    	
    }
    
    if(!Ember.empty(this.get("currentCatSelected"))){
      category = this.get("currentCatSelected").id;
      
      getItems();
    }
    else{
      TN.utils.passiveNotification('Category not selected yet!', "Please select a category first!");
    }
  },

  mineStories:function(){
      this.set("allOrMineStories",[]);
      this.set("isAllStoriesMode", false);
      var custId = TN.utils.getCookie('TNUser'),
      	  totalPages = 0,
          pageNum = 1,
          numItems = 25,
          category,
          self = this,
          storiesArray = [];
      
      function getItems(){
          TN.services.getStoriesAndWishListFiltered(custId, numItems, pageNum, 'mine', category).
          	done(function(data){
          	  totalPages = data[0].numPages;
          	  $("#mine").css("background", "rgb(236,96,63)");
              $("#allStories").css("background", "");
              $('#storiesSearch').val("");
              
              storiesArray = storiesArray.concat(data[0].stories);
              
              pageNum++;
              
              if( pageNum <= totalPages ){
            	  getItems();
              }
              else {
                  self.set("allOrMineStories",storiesArray);            	  
              }
            });    	  
      }
      
      if(!Ember.empty(this.get("currentCatSelected"))){
        category = this.get("currentCatSelected").id;
        
        getItems();
      }
      else{
        TN.utils.passiveNotification('Category not selected yet!', "Please select a category first!");
      }
  },
  
  searchForStories:function(event){
    this.set("allOrMineStories",[]);
    var searchText = $('#storiesSearch').val();
    var self = this;
    
    if( !!searchText ){
      TN.services.searchStories( searchText, 1, 20 ).
      done(function(data){
    	  self.set("isAllStoriesMode", true);
          $("#allStories").css("background", "");
          $("#mine").css("background", "");
          self.set("allOrMineStories", data);       
      });     
    }
  },
  
  deleteNewspaper:function(event){
  	if( TN.header.isGuestUser() ) return;
	  	
    var self = this;
    var currNewspaperId = self.get("currNewspaperId");
    
    if( currNewspaperId > 0 ){
      TN.services.deleteNewspaper(Number(currNewspaperId)).done(function(){
        self.getNewspapers();
          $("#newspaper").find(".story").remove();
        self.set('currNewspaperId',0);
        self.set('selectedNewspaperId',0);
        TN.utils.passiveNotification('Deleted!', 'Newspaper has been successfully deleted.');
      }).fail(function(jqXHR, textStatus, errorThrown){
        if( jqXHR.responseText === 'success'){
          self.getNewspapers();
          $("#newspaper").find(".story").remove();
          self.set('currNewspaperId',0);
          self.set('selectedNewspaperId',0);
          $('#npHeadline').html(self.get("defaultHeadline"));
          $('#npLocation').html(self.get("defaultLocation"));
          $('#npEdition').html(self.get("defaultEdition"));
          TN.utils.passiveNotification('Deleted!', 'Newspaper has been successfully deleted.');
        } else {
          TN.utils.passiveNotification('Error!', 'Error deleting newspaper:' + errorThrown );
        }
      });
    }
    else {
      TN.utils.passiveNotification('Please select a newspaper first!', 'No newspaper is currently loaded.');
    }
  },

  themesAction:function(event){
    $("#stories").hide();
    $("#themes").show();
    $("#templates").hide();
  },

  storiesAction:function(event){
    $("#stories").show();
    $("#themes").hide();
    $("#templates").hide();
  },

  templatesAction:function(event){
    $("#templates").show();
    $("#stories").hide();
    $("#themes").hide();
  },

  getNewspapers:function(){
    var self = this,
        custId = TN.utils.getCookie('TNUser'),
        newNewspaperObject = {id:"new", headline:"Create New"},
        newPreselectedObject = {id:"preselectHack"};

    self.set("allNewspapers", []);
    TN.services.getNewspapers(custId).done(function(data){
      self.set("allNewspapers", data);
      self.get("allNewspapers").reverse();
      self.get("allNewspapers").addObject(newNewspaperObject);
      self.get("allNewspapers").addObject(newPreselectedObject);
      self.get("allNewspapers").reverse();
    }).fail(function(){
      self.get("allNewspapers").addObject(newPreselectedObject);
      self.get("allNewspapers").addObject(newNewspaperObject);
    });
  },


  expandContainer:function(){
    if($(".storiesContainer").css("height") === "220px"){
      $(".storiesContainer").animate({"height": "500px"}, {duration: "slow" });
      $(".storiesContainer").addClass("storiesContainerExpanded");
    }
    else{
      $(".storiesContainer").animate({"height": "220px"}, {duration: "slow" });
      $(".storiesContainer").scrollTop(0);
      $(".storiesContainer").removeClass("storiesContainerExpanded");     
    }
  },

  getSelectedCat:function(){
    return "";
  },

  navBarAction:function(event){
    /*$(event.target).parent().find(".navBarActive").removeClass("navBarActive");
    $(event.target).addClass("navBarActive");*/
  },

  addMoreTiles:function(){

    //Take the id of the last tile
    var lastTileId = parseInt($(".tileImage").last().parent().attr("id"));

    var firstRow = '<div class="grid_16 firstRow">'+
                      '<li id="'+ (lastTileId+1) + '" class="storyTile" data-row="1" data-col="1" data-sizex="5" data-sizey="4"><div class="tileImage"><img src="images/story-1.png" alt="story"></div></li>'+
                      '<li id="'+ (lastTileId+2) + '" class="storyTile" data-row="2" data-col="1" data-sizex="3" data-sizey="2"><div class="tileImage"><img src="images/story-2.png" alt="story"></div></li>'+
                      '<li id="'+ (lastTileId+3) + '" class="storyTile" data-row="3" data-col="1" data-sizex="3" data-sizey="2"><div class="tileImage"><img src="images/story-3.png" alt="story"></div></li>'+
                    '</div>'

    var secondRow ='<div class="grid_16 secondRow">'+
                        '<li id="'+ (lastTileId+4) + '" class="storyTile" data-row="1" data-col="2" data-sizex="3" data-sizey="3"><div class="tileImage"><img src="images/story-4.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+5) + '" class="storyTile" data-row="2" data-col="2" data-sizex="5" data-sizey="3"><div class="tileImage"><img src="images/story-5.png" alt="story"></div></li>'+
                      '</div>'
                
    var thirdRow = '<div class="grid_16 thirdRow">'+
                        '<li id="'+ (lastTileId+6) + '" class="storyTile" data-row="1" data-col="4" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-6.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+7) + '" class="storyTile" data-row="2" data-col="4" data-sizex="6" data-sizey="3"><div class="tileImage"><img src="images/story-7.png" alt="story"></div></li>'+
                      '</div>'
                        
    var fourthRow = '<div class="grid_16 fourthRow">'+
                        '<li id="'+ (lastTileId+8) + '" class="storyTile" data-row="3" data-col="4" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-8.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+9) + '" class="storyTile" data-row="1" data-col="5" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-9.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+10) + '" class="storyTile" data-row="3" data-col="5" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-9.png" alt="story"></div></li>'+
                        '<li id="'+ (lastTileId+11) + '" class="storyTile" data-row="1" data-col="6" data-sizex="2" data-sizey="3"><div class="tileImage"><img src="images/story-9.png" alt="story"></div></li>'+
                      '</div>'

    $("#gridsterList").append(firstRow);
    $("#gridsterList").append(secondRow);
    $("#gridsterList").append(thirdRow);
    $("#gridsterList").append(fourthRow)
    
    var self = this,
        newspaperTile = $(".gridster");
    $( "li", newspaperTile ).droppable({
        accept: "li",
        activeClass: "",
        drop: function( event, ui ) {
          TNE.NewspaperView.resizeStory( event, ui.draggable );
        }
      });
  },

  previewHelper:function(){
    $("#previewStory").append($('#newspaper').clone());
    $("#previewStory").find(".preview").remove();
    $("#previewStory").find(".dustbin").remove();
    $("#previewStory .storyTile").remove();

    $("#previewStory ul .firstRow").remove();
    $("#previewStory ul .secondRow").remove();
    $("#previewStory ul .thirdRow").remove();
    $("#previewStory ul .fourthRow").remove();

    $("#previewStory ul").append($("#newspaper .storyTile .story").parent().clone());
    $("#previewStory .storyTile .tileImage").remove();
    //$("#previewStory")
    $("#previewStory .storyTile").find(".itemForPaper").show();
    $("#previewStory .storyTile .story").addClass("storyOnDrop2");
    $("#previewStory .storyTile").addClass("previewStoryTile");
  },


  buildStories:function(storiesArray){

    this.previewHelper();

    $('#previewStory #gridsterList').children().each(function(index){
      var currElem = $(this);
      var story = {
          'buzzImageId': currElem.find('.imageId').val(),
          'buzzImageUrl' : escape(currElem.find('.storyOriginalImage').val()),
          'buzzThumbImageUrl' : escape(currElem.find('.storyThumbImage').val()),
          'customerBuzzId' : currElem.find('.buzzId').val(),
          'elementId' : 0,
          'fontcolor' : currElem.find('.storyHeading').css('color'),
          'fontsize' : currElem.find('.storyHeading').css('font-size'),
          'fontstyle' : currElem.find('.storyHeading').css('font-family'),
          'bodyfontcolor' : currElem.find('.storyBody p').css('color'),
          'bodyfontsize' : currElem.find('.storyBody p').css('font-size'),
          'bodyfontstyle' : currElem.find('.storyBody p').css('font-family'),
          'bylinefontcolor' : currElem.find('.storyByline').css('color'),
          'bylinefontsize' : currElem.find('.storyByline').css('font-size'),
          'bylinefontstyle' : currElem.find('.storyByline').css('font-family'),
          'index' : index,
          'xpos' : parseFloat(currElem.position().left),
          'ypos' : parseFloat(currElem.position().top),
          'pixelWidth' : parseFloat(currElem.css('width')),
          'pixelHeight' : parseFloat(currElem.css('height')),
          'jcolumn' : parseFloat(currElem.attr('data-col')),
          'jheight' : parseFloat(currElem.attr('data-sizey')),
          'jrow' : parseFloat(currElem.attr('data-row')),
          'jwidth' : parseFloat(currElem.attr('data-sizex'))
      };        
      storiesArray.pushObject(story);
    });
    
    $("#previewStory #newspaper").remove();
  },

  previewStory:function(){
    if($(".storyTile").find(".story").length === 0){
      TN.utils.passiveNotification('No story added yet!', "Please add a story to newspaper!");
    }
    else{
      this.previewHelper();
      this.set("isPreviewStory",true);
      $(".previewStoryView").css("visibility", "visible");
      $("#ember-newspaper").addClass("fadeBackground");
    }
  },

  closePreview:function(){
    this.set("isPreviewStory",false);
    $(".previewStoryView").css("visibility", "hidden");
    $("#previewStory #newspaper").remove();
    $("#ember-newspaper").removeClass("fadeBackground");
  },

  editText:function(event){
    if(this.get("isPreviewStory") === false){
      $(event.target).attr('contentEditable','true');
    }
  },

  saveNewspaper:function(event){
  	if( TN.header.isGuestUser() ) return;
	  	
    if($(".storyTile").find(".story").length === 0){
      TN.utils.passiveNotification('No story added yet!', "Please add a story to newspaper!");
    }
    else{
      var today = new Date();
      var storiesArray = [];    
      this.buildStories(storiesArray);    
      
      var npHeadlineElem = $('#npHeadline');
      var npLocationElem = $('#npLocation');
      var npEditionElem = $('#npEdition');
      
      var newspaper = {
            'custId' : TN.utils.getCookie('TNUser'),
            'custName' : $('#firstname').text(),
            'date' : today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear(),
            'edition' : npEditionElem.text(),
            'headline' : npHeadlineElem.text(),
            'titleFontColor' : npHeadlineElem.css('color'),
            'titleFontStyle' : npHeadlineElem.css('font-family'),
            'titleFontSize' : npHeadlineElem.css('font-size'),
            'locEdFontColor' : npLocationElem.css('color'),
            'locEdFontStyle' : npLocationElem.css('font-family'),
            'locEdFontSize' : npLocationElem.css('font-size'),
            'id' : this.get("currNewspaperId"),
            'location' : npLocationElem.text(),
            'stories' : storiesArray
      };
      
      if( this.get("currNewspaperId") > 0 ){
        var self = this;
        TN.services.updateNewspaper(newspaper).done(function(response){
          if( response[0] === true ){
            /*this.loadNewspapersList();
            this.loadNewspaper(this.get("currNewspaperId"));*/
            TN.utils.passiveNotification('Updated!', 'Newspaper has been successfully updated.');
          }
          else {
            TN.utils.passiveNotification('Error!', 'There was an error in making the updates');
          }
        }).fail(function(jqXHR, textStatus, errorThrown){
          TN.utils.passiveNotification('Error!', 'There was an error in making the updates: (' +  jqXHR.status + ') ' + errorThrown);        
        });
      }
      else {
        var self = this;
        TN.services.saveNewspaper(newspaper).done(function(response){
          /*this.loadNewspapersList();
          this.loadNewspaper(parseFloat(response));*/
          self.set("currNewspaperId",response[0]);
          self.getNewspapers();
          TN.utils.passiveNotification('Saved!', 'Newspaper has been successfully saved.');
        }).fail(function(jqXHR, textStatus, errorThrown){
          TN.utils.passiveNotification('Error!', 'There was an error in saving the newspaper: (' +  jqXHR.status + ') ' + errorThrown);
        }); 
      }
    }
  },

  publishNewspaper:function(){
  	if( TN.header.isGuestUser() ) return;
	  	
    if (this.get("currNewspaperId") != 0) {
      var newspaperHeadline = $('#npHeadline').text();
      TN.sharingHandler.initNPShare(this.get("currNewspaperId"), newspaperHeadline);
    }
    else{
      if($(".storyTile").find(".story").length === 0){
        TN.utils.passiveNotification('No newspaper selected yet!', "Please load a newspaper to share first.");
      }
      else{
        TN.utils.passiveNotification('Newspaper not saved yet!', 'Please save your newspaper first!');
      }
    }
  }

});

TNE.NewspaperView = Ember.View.create({
    templateName:"templates/newspaper",
    controllerBinding:"TNE.NewspaperController",

    didInsertElement:function(){

      this.get("controller").initialise();

      var self = this,
          newspaperTile = $(".gridster");

      $( "li", newspaperTile ).droppable({
        accept: "li",
        activeClass: "",
        drop: function( event, ui ) {
          self.resizeStory( event, ui.draggable );
        }
      });

    },

    putStoryOnTile:function(event, draggedObj, shouldApplyTheme){
      // console.log( $(event.target).css("height"), $(event.target).css("width"), $(event.target).height(), $(event.target).width() );
      
      // Normalize image dimensions according to div "window" .storyIconBigContainer which is set to overflow:hidden
      // This is equivalent to TN.utils.normalizeImage function which is used to "normalize" image dimensions in simpler circumstances.
      // Commented console.log lines are deliberately left out for inspection needs.
      var storyIconBigImg = $(draggedObj).find('img.storyIconBig');
      // http://css-tricks.com/snippets/jquery/get-an-images-native-width/
      // Create new offscreen image to test
      var theImage = new Image();
      var targetWidth = $(event.target).css("width");
      var targetHeight = $(event.target).css("height");
      // 16 pixels total to decrement before calculating image container window (.storyIconBigContainer div's) "width factor"
      // comes from 2x7 pixels for left and rigth padding respectively and from additional 2x1 pixels for 1 pixel thick .story border
      var windowWidth = $(event.target).width() - 16;
      var windowHeight = ($(event.target).height() * 0.8) - 16;      
      var imgContainerWindowWidthFactor = windowWidth / windowHeight;

      theImage.onload = function() {
        // Get accurate measurements from that.
        var originalImgWidth = theImage.width;
        var originalImgHeight = theImage.height;
        var origImgWidthFactor = originalImgWidth/originalImgHeight;
        // console.log('targetWidth: '+targetWidth, 'targetHeight: '+targetHeight);
        // console.log('originalImgWidth: '+originalImgWidth, ' originalImgHeight: '+originalImgHeight, ' windowWidth: '+windowWidth, ' windowHeight: '+windowHeight, 'origImgWidthFactor: '+origImgWidthFactor, 'imgContainerWindowWidthFactor: '+imgContainerWindowWidthFactor);
        if(origImgWidthFactor > imgContainerWindowWidthFactor){
          storyIconBigImg.height(windowHeight);
          //console.log('setting new width: ' + (storyIconBigImg.height() * origImgWidthFactor));
          storyIconBigImg.width( storyIconBigImg.height() * origImgWidthFactor );
          // console.log('new width: ' + storyIconBigImg.width() );
          var trailingWidth = storyIconBigImg.height() * origImgWidthFactor - windowWidth;
          if (trailingWidth > 1) storyIconBigImg.css('position', 'relative').css('right', trailingWidth/2);
        }
        else if( origImgWidthFactor < imgContainerWindowWidthFactor ){
          storyIconBigImg.width(windowWidth);
          // console.log('setting new height: ' + (storyIconBigImg.width() / origImgWidthFactor));
          storyIconBigImg.height( storyIconBigImg.width() / origImgWidthFactor );
          storyIconBigImg.css('position', 'relative').css('right', 0);
        }
        else {
          storyIconBigImg.height(windowHeight);
          storyIconBigImg.width(windowWidth);
          storyIconBigImg.css('position', 'relative').css('right', 0);
        }
      };
      
      theImage.src = storyIconBigImg.attr("src");
      $(event.target).append(draggedObj);
      
      $(draggedObj).css("width", targetWidth);
      $(draggedObj).css("height", targetHeight);
      
      $(draggedObj).addClass("storyOnDrop");
      $(draggedObj).append('<div class="removeOverlay"><img src="images/close-icon-hover.png" class="removeStory"></div>');
      if($(draggedObj).find(".tileNumber").length === 0){
        $(draggedObj).append('<div class="tileNumber"></div>');
        $(draggedObj).find(".tileNumber").html($(event.target).attr("id"));
      }
      else{
        $(draggedObj).find(".tileNumber").html($(event.target).attr("id"));
      }
      $(event.target).find(".storyIcon").hide();
      $(event.target).find(".storyIconBigContainer").show();
      //$(event.target).find(".storyIconBig").css("height", "80%");
      $(event.target).find("h3").css("font-size", "20px");
      

      if(shouldApplyTheme){
        if(!Ember.empty(this.get("controller").get("currentTheme"))){
          this.get("controller").applyTheme(null, this.get("controller").get("currentTheme"));
        }
      }

      $(draggedObj).hover(function(){
        $(draggedObj).find(".removeOverlay").show();
      },
      function(){
        $(draggedObj).find(".removeOverlay").hide();
      });

      $(draggedObj).find(".removeStory").click(function(event){
        var obj = $(event.target).parent().parent();
        $("#stories").append(obj);

        obj.removeClass("storyOnDrop");
        obj.removeAttr("style");
        obj.find(".removeOverlay").remove();
        obj.find(".storyIcon").show();
        obj.find(".storyIconBigContainer").hide();
        obj.find("h3").css("color", "#333");
        obj.find("h3").css("font-size", "12px");
        obj.find("h3").css("font-weight", "normal");
        obj.find("h3").css("font-family", "'News Cycle', Helvetica Light, sans-serif");
        obj.draggable("enable");

      });
    },

    resizeStory:function(event, draggedObj){
      
      if($(event.target).find(".story").length === 0){
        this.putStoryOnTile(event, draggedObj,true);
      }
      else{
        if(!$(draggedObj).hasClass("storyOnDrop")){
          //move to the first empty tile!
          var firstEmptyTile = $('.storyTile').filter(function() { return !($(this).children().hasClass("storyOnDrop")); }).first();
          var targetObj = {target:firstEmptyTile};
          this.putStoryOnTile(targetObj,draggedObj,true);
        }
        else{
          //swap the stories!
          var tempStory = $(event.target).find(".story");
          var idOfInitialContainer = $(draggedObj).find(".tileNumber").html();
          this.putStoryOnTile(event, draggedObj,true);
          this.putStoryOnTile({target:$("#" + idOfInitialContainer)}, tempStory,true);
        }
      }
    }

});

TNE.StoryView = Ember.View.extend({
  
  templateName:"templates/story",
  
  didInsertElement : function(){

    var stories = $("#draggable");
    $( "li", stories ).draggable({
      addClasses: false,
      appendTo: "body",
          helper: "clone",
        revert: "invalid", // when not dropped, the item will revert back to its initial position
        containment: "document",
        cursor: "move"
    });

   /* $(".gridster ul").gridster({
        widget_margins: [5, 15],
        widget_base_dimensions: [100, 100]
      });*/
  }
});

TNE.PreviewStoryView = Ember.View.create({
  templateName:"templates/previewStory",
  controllerBinding:"TNE.NewspaperController",
  classNames:"previewStoryView",
  didInsertElement:function(){
    $(".previewStoryView").css("visibility", "hidden");
  }
});

TNE.NewspaperView.appendTo('#ember-newspaper');
TNE.PreviewStoryView.appendTo('#modalDiv');