if (!TN) var TN= {};
if (!TN.categories) TN.categories= {};

(function($categories){
	var catMap = [];
	
	catMap['Breaking News'] = 'Trending News';
	catMap['Local Politics'] = 'Politics';
	catMap['Funny'] = 'LOL';
	catMap['The Green Earth'] = 'The Green Earth';
	catMap['Sports'] = 'Sports';
	catMap['Politics and Insurrection'] = 'International affairs';
	catMap['Trembling Economies'] = 'Business';
	catMap['What To Do'] = 'Just Fun';
	catMap['Buzzworthy'] = 'Buzzworthy';
	catMap['Fashion'] = 'Fashion';
	catMap['Food n Drink'] = 'Best Food n Drinks';
	catMap['Theatre-Arts'] = 'Entertainment';
	catMap['Tech'] = 'Science/Tech';
	catMap['Events'] = 'Events';
	catMap['Movies'] = 'Nightlife';
	catMap['IBoughtIt'] = 'Weather';
	catMap['BuyItOrNot'] = 'Comics';
	catMap['FoundASale'] = 'The Turnip';
	catMap['How do I Look?'] = 'Travel';
	
	$categories.getLabel = function(id){
		return catMap[id];
	};
	
	$categories.getValue = function(label){
		for( var value in catMap ){
			if( catMap[value] === label ){
				return value;
			}
		}
		if (label == 'Choose A Category') return 'Choose A Category';
	};
	
})(TN.categories);