(function($TN){
	
	function dependenciesLoaded(){
		if( !!$TN && !!$TN.utils ){
			return $TN.utils.isScriptLoaded('javascripts/baseNewspaper.js') && $TN.utils.isScriptLoaded('javascripts/editable.js');
		}
	}
	
	function extend($np){
		var numBuckets = 12;
		var npContainerElem = $('#npContainer');
		
		function getNextAvailableBucket(){
			var availableBucket;
			for( var i = 1; i <= numBuckets; i++ ){
				var currBucket = npContainerElem.find('#bucket'+i);
				if (currBucket.find('.placeHolder').is(':visible')){
					availableBucket = currBucket;
					break;
				}
			}
			return availableBucket;
		}
		
		function putStoryInBucket(storyElem, bucketElem){
		      // Normalize image dimensions according to div "window" .storyIconBigContainer which is set to overflow:hidden
		      // This is equivalent to TN.utils.normalizeImage function which is used to "normalize" image dimensions in simpler circumstances.
		      // Commented console.log lines are deliberately left out for inspection needs.
			
			  
		      var storyIconBigImg = storyElem.find('img.storyIconBig');
		      // http://css-tricks.com/snippets/jquery/get-an-images-native-width/
		      // Create new offscreen image to test
		      var theImage = new Image();
		      var targetWidth = bucketElem.css("width");
		      var targetHeight = bucketElem.css("height");
		      // 16 pixels total to decrement before calculating image container window (.storyIconBigContainer div's) "width factor"
		      // comes from 2x7 pixels for left and rigth padding respectively and from additional 2x1 pixels for 1 pixel thick .story border
		      var windowWidth = bucketElem.width() - 16;
		      var windowHeight = (bucketElem.height() * 0.8) - 16;      
		      var imgContainerWindowWidthFactor = windowWidth / windowHeight;

		      theImage.onload = function() {
		        // Get accurate measurements from that.
		        var originalImgWidth = theImage.width;
		        var originalImgHeight = theImage.height;
		        var origImgWidthFactor = originalImgWidth/originalImgHeight;
		        // console.log('targetWidth: '+targetWidth, 'targetHeight: '+targetHeight);
		        // console.log('originalImgWidth: '+originalImgWidth, ' originalImgHeight: '+originalImgHeight, ' windowWidth: '+windowWidth, ' windowHeight: '+windowHeight, 'origImgWidthFactor: '+origImgWidthFactor, 'imgContainerWindowWidthFactor: '+imgContainerWindowWidthFactor);
		        if(origImgWidthFactor > imgContainerWindowWidthFactor){
		          storyIconBigImg.height(windowHeight);
		          //console.log('setting new width: ' + (storyIconBigImg.height() * origImgWidthFactor));
		          storyIconBigImg.width( storyIconBigImg.height() * origImgWidthFactor );
		          // console.log('new width: ' + storyIconBigImg.width() );
		          var trailingWidth = storyIconBigImg.height() * origImgWidthFactor - windowWidth;
		          if (trailingWidth > 1) storyIconBigImg.css('position', 'relative').css('right', trailingWidth/2);
		        }
		        else if( origImgWidthFactor < imgContainerWindowWidthFactor ){
		          storyIconBigImg.width(windowWidth);
		          // console.log('setting new height: ' + (storyIconBigImg.width() / origImgWidthFactor));
		          storyIconBigImg.height( storyIconBigImg.width() / origImgWidthFactor );
		          storyIconBigImg.css('position', 'relative').css('right', 0);
		        }
		        else {
		          storyIconBigImg.height(windowHeight);
		          storyIconBigImg.width(windowWidth);
		          storyIconBigImg.css('position', 'relative').css('right', 0);
		        }
		      };
		      
		      theImage.src = storyIconBigImg.attr("src");
		      bucketElem.append(storyElem);
		      
		      storyElem.css("width", targetWidth);
		      storyElem.css("height", targetHeight);
		      storyElem.css("top", '0px');
		      storyElem.css("left", '0px');
		      storyElem.css("margin", '0px');
		      storyElem.removeClass('masonry-brick');
		      
//		      $(storyElem).addClass("storyOnDrop");
		      storyElem.append('<div class="removeOverlay"><img src="images/close-icon-hover.png" class="removeStory"></div>');
//		      if($(storyElem).find(".tileNumber").length === 0){
//		        $(storyElem).append('<div class="tileNumber"></div>');
//		        $(storyElem).find(".tileNumber").html($(bucketElem).attr("id"));
//		      }
//		      else{
//		        $(storyElem).find(".tileNumber").html($(bucketElem).attr("id"));
//		      }
		      
		      bucketElem.find('.placeHolder').hide();
		      bucketElem.find(".storyIcon").hide();
		      bucketElem.find(".storyIconBigContainer").show();
		      //$(bucketElem).find(".storyIconBig").css("height", "80%");
		      bucketElem.find("h3").css("font-size", "20px");
		      

//		      if(shouldApplyTheme){
//		        if(!Ember.empty(this.get("controller").get("currentTheme"))){
//		          this.get("controller").applyTheme(null, this.get("controller").get("currentTheme"));
//		        }
//		      }

		      storyElem.hover(function(){
		        storyElem.find(".removeOverlay").show();
		      },
		      function(){
		        storyElem.find(".removeOverlay").hide();
		      });

		      storyElem.find(".removeStory").click(function(event){
		    	  storyElem.hide();
		    	  storyElem.parent().find('.placeHolder').show();
		    	  $('#storyContainer').find('#' + storyElem.attr('id')).show();
		    	  storyElem.empty().remove();
//		        var obj = $(bucketElem).parent().parent();
//		        $("#stories").append(obj);
//
//		        obj.removeClass("storyOnDrop");
//		        obj.removeAttr("style");
//		        obj.find(".removeOverlay").remove();
//		        obj.find(".storyIcon").show();
//		        obj.find(".storyIconBigContainer").hide();
//		        obj.find("h3").css("color", "#333");
//		        obj.find("h3").css("font-size", "12px");
//		        obj.find("h3").css("font-weight", "normal");
//		        obj.find("h3").css("font-family", "'News Cycle', Helvetica Light, sans-serif");
//		        obj.draggable("enable");

		      });
		}
		
		$np.addStory = function(storyElem){
			// Confirm story is not already added
			if( $('#npContainer').find('#' + storyElem.attr('id')).length > 0 ){
	            TN.utils.passiveNotification('Duplicate', "This story already exists in this Newspaper.");
			}
			else {
				var availableBucket = getNextAvailableBucket();
				if( !!availableBucket ){
					putStoryInBucket(storyElem.clone(), getNextAvailableBucket());
					storyElem.hide();
				}
				else {
		            TN.utils.passiveNotification('Error', "There are no more containers left to add another story to this Newspaper.");
				}				
			}
		};
				
		$('#previewBtn').click(function(){	
			var previewCont = '';
			previewCont = $('#npContainer').clone();
			previewCont.removeClass('seven');
			previewCont.find('#previewBtn').remove();
			previewCont.find('.storyIconBigContainer').removeClass('storyIconBigContainer');
			previewCont.find('.first-row').removeClass('first-row');
			previewCont.find('.second-row').removeClass('second-row');
			previewCont.find('.third-row').removeClass('third-row');
			previewCont.find('.ml').removeClass('ml');
			previewCont.find('.mr').removeClass('mr');
			previewCont.find('.placeHolder').remove();
			previewCont.find('.removeOverlay').remove();
			previewCont.find('.effect-on-hover').remove();
			previewCont.find('.storyIcon').remove();
			previewCont.find('.StoriesBox').css('position', 'relative').css('border', '0px').css('height', 'auto');
			previewCont.find('.storyBody').show();
			
			$('#previewStory').empty().append(previewCont).append('<a class="close-reveal-modal"></a>').reveal();
		})
	}
	
	$('document').ready(function(){
		if( dependenciesLoaded() ){
			extend(TN.newspaper);
		}
		else {
			$.getScript('javascripts/baseNewspaper.js').done(function(){
				$.getScript('javascripts/editable.js').done(function(){
					Editable.initEditiables();
				});
				TN.newspaper.initialize();
				extend(TN.newspaper);
			});
		}
	});
	
})(TN);

