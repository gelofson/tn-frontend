if (!TN) var TN= {};
if (!TN.newspaper) TN.newspaper= {};

(function($np){
	
	var catListElem = $('#storyCatsDropDown');
	var loadType;
	var catValue;
	var custId;
	var totalPages = 0;
	var pageNum = 1;
	var maxItems = 25;
	var storiesHtml = '';
	var storyCont = $('#storyContainer');
	var masonryInitialized = false;
	var allBtnElem = $('#allBtn');
	var mineBtnElem = $('#mineBtn');
	var searchFieldElem = $('#searchField');
	var currAjaxCallId = null;
		
	function resetStoriesBoard(){
		storiesHtml = '';
		pageNum = 1;
		storyCont.empty();
		if( masonryInitialized ){
			storyCont.masonry('destroy');
			masonryInitialized = false;
		}
	}
	
    function loadCategories(){
    	
        var csCatDropdown = $('#storyCatsDropDown > ul');
        function addCategoryItem(itemText){
        	var categoryItemElem = null;
        	
        	if( itemText === 'All Categories' ){
            	categoryItemElem = $('<li id="allCatsOption">' + itemText + '</li>');           		
        	} else {
            	categoryItemElem = $('<li>' + itemText + '</li>');
        	}            	
        	
        	categoryItemElem.click(function(){
        		var currElem = $(this);
                TN.utils.setDropdownValue(catListElem, currElem.text());            		
                
                if( currElem.text() === 'All Categories' ){
                	catValue = '';
                }
                else {
                	catValue = TN.categories.getValue(currElem.text());
                }
                
                if( !loadType ){
                	allBtnElem.addClass('active');
                	loadType = 'all';
                    searchFieldElem.val('');
                }
                
                loadStories();
                
        		$('body').click();
        		return false;        	
        	});
        	
        	csCatDropdown.append(categoryItemElem);
        }
                
        addCategoryItem('All Categories');
        TN.utils.setDropdownValue(catListElem, 'Trending News');
    	
        TN.services.getAllMessageTypeActiveCategories().done(function(json){
            if( !!json ){
                var maxItems = json.length;
                
                for( var i = 0; i < maxItems; i++ ){
					
                    if (json[i].id == "Q") continue;
                    if( json[i].global === 1 || json[i].global === 0 ){
    					var currCat = json[i].id;
    					var currCatLabel = TN.categories.getLabel(currCat);
    					
    					if( !!currCatLabel ){
                        	addCategoryItem(currCatLabel);
    					}
                    }
                }
                
                loadType = (allBtnElem.hasClass('active') ? 'all' : (mineBtnElem.hasClass('active') ? 'mine' : '' ) );
                if( !loadType ){
                	allBtnElem.addClass('active');
                	loadType = 'all';
                }
                catValue = "Breaking News";
                loadStories();
            }
        });
    }
    
	function getStoryHtml(story){
		var html = '<div class="StoriesBox" id="' + story.buzzId + '">\
    			<img class="storyIcon" src="' + story.storyThumbImg + '" alt="' + story.headline + '">\
    			<div class="storyIconBigContainer">\
    				<img class="storyIconBig" src="' + story.storyOriginalImg + '" alt"' + story.headline + '"/>\
    			</div>\
    			<h3>' + story.headline + '</h3>\
    			<a href="#" class="effect-on-hover"></a>\
    			<div class="storyBody">' +
    				( !!story.buzzWho ? '<p><b>Who</b>:&nbsp ' + story.buzzWho + ' </p>' : '' ) +
    				( !!story.buzzWhat ? '<p><b>What</b>:&nbsp ' + story.buzzWhat + ' </p>' : '' ) +
    				( !!story.buzzWhen ? '<p><b>When</b>:&nbsp ' + story.buzzWhen + ' </p>' : '' ) +
    				( !!story.buzzWhere ? '<p><b>Where</b>:&nbsp ' + story.buzzWhere + ' </p>' : '' ) +
    				( !!story.buzzWhy ? '<p><b>Why</b>:&nbsp ' + story.buzzWhy + ' </p>' : '' ) +
    				( !!story.buzzHow ? '<p><b>How</b>:&nbsp ' + story.buzzHow + ' </p>' : '' ) +
    				( !!story.buzzOped ? '<p>' + story.buzzOped + ' </p>' : '' ) +
    				'<div class="storyAuthor">by ' + story.originatorName + '</div>\
    			</div>' +
			'</div>'; 
		return html;
	}
	
    function searchStories(searchText){
    	resetStoriesBoard();
    	
    	var startIndex = 1;
    	var endIndex = 20;
    	
    	function performSearch(){
    		currAjaxCallId = new Date().getTime();
    		
    		(function(){
				var ourAjaxCallId = currAjaxCallId;
				
    			TN.services.searchStories(searchText, startIndex, endIndex).done(function(json){
    				
	    			if( ourAjaxCallId === currAjaxCallId ){
	    				if( !!json  ){
	    					var numStories = json.length;
	    					for( var i = 0; i < numStories; i++ ){
	    						var storyItem = json[i];
	    						
	                            var storyToAdd = {
	                                headline: storyItem.headline,
	                                buzzId: storyItem.buzzId,
	                                imageId:storyItem.imageId,
									elementId:0,
	                                storyThumbImg: storyItem.thumbImageUrl,
	                                storyOriginalImg: storyItem.imageUrl,
	                                buzzWhat:storyItem.what,
	                                buzzWhen:storyItem.when,
	                                buzzWhere:storyItem.where,
	                                buzzWhy:storyItem.why,
	                                buzzWho:storyItem.who,
	                                buzzHow:storyItem.how,
	                                buzzOped:storyItem.oped,
	                                originatorImageUrl: storyItem.customerImageUrl,
	                                originatorName: storyItem.firstName,
	                                elapsedTime: !!storyItem.date ? storyItem.date : ''
	                            };
								storiesHtml += getStoryHtml(storyToAdd);
	    					}
	    					
	    					if( !!storiesHtml ){
	    						var storiesHtmlElem = $(storiesHtml);
	    						storyCont.append(storiesHtmlElem);
	    						storiesHtmlElem.hide();
	    						storyCont.imagesLoaded(function(){
	    							
	    							if( ourAjaxCallId === currAjaxCallId ){
		                                if(!masonryInitialized){
		                                	storyCont.masonry({
		                                        // options
		                                        itemSelector : '.StoriesBox',
		                                        isFitWidth: true,
		                                        columnWidth : 178
		                                    });
		                                    masonryInitialized = true;
		                                } else {
		                                	storyCont.masonry('appended', storiesHtmlElem);
		                                }
		                                storiesHtmlElem.show();
		                                
		                                $('.StoriesBox').unbind('click').click(function(){
		                                	$np.addStory($(this));
		                                });
		                                		                                		                                
//		                                storiesHtml = "";
		                                
//		                                startIndex += 20;
//		                                endIndex += 20;
	    								
//		                                performSearch();
	    							}		                                
	    						});
	    					}
	    				}	    				
	    			}
    			});
    		})();
    	}
    	
    	performSearch();
    }
    
    function loadStories(){
    	function getStoriesForAll(){
			currAjaxCallId = new Date().getTime();

			(function(){
				var ourAjaxCallId = currAjaxCallId;
				
	    		TN.services.getAllRequestSummaries(custId, maxItems, pageNum, catValue).done(function(json){
	    			
	    			if( ourAjaxCallId === currAjaxCallId ){
		    			if( !!json && !!json[0] ){
							totalPages = json[0].noOfPages;
		    				var storyArray = json[0].fittingRoomSummaryList;
		    				if( !!storyArray ){
		    					var numStories = storyArray.length;
		    					
		    					for( var i = 0; i < numStories; i++ ){
		    						var storyItem = storyArray[i];
		    						if( !!storyItem ){
		                                var buzz = storyItem.wsBuzz;
		    							var storyToAdd = {
											headline: storyItem.headline,
											buzzId: (!!buzz ? buzz.buzzId : 0),
											imageId:(!!buzz ? buzz.imageId : 0),
											elementId:0,
											storyThumbImg: (!!buzz ? buzz.thumbImageUrl : ''),
											storyOriginalImg: (!!buzz ? buzz.imageUrl : ''),
											buzzWhat:(!!buzz ? buzz.what : ''),
											buzzWhen:(!!buzz ? buzz.when : ''),
											buzzWhere:(!!buzz ? buzz.where : ''),
											buzzWhy:(!!buzz ? buzz.why : ''),
											buzzWho:(!!buzz ? buzz.who : ''),
											buzzHow:(!!buzz ? buzz.how : ''),
											buzzOped:(!!buzz ? buzz.oped : ''),
		                                    originatorImageUrl: storyItem.originatorThumbImageUrl,
											originatorName: storyItem.originatorName,
											elapsedTime: !!storyItem.storyDate ? storyItem.storyDate : ''
										};
		    							storiesHtml += getStoryHtml(storyToAdd);
		    						}
		    					}
		    					
		    					if( !!storiesHtml ){
		    						var storiesHtmlElem = $(storiesHtml);
		    						storyCont.append(storiesHtmlElem);
		    						storiesHtmlElem.hide();
		    						storyCont.imagesLoaded(function(){
		    							
		    							if( ourAjaxCallId === currAjaxCallId ){
			                                if(!masonryInitialized){
			                                	storyCont.masonry({
			                                        // options
			                                        itemSelector : '.StoriesBox',
			                                        isFitWidth: true,
			                                        columnWidth : 178
			                                    });
			                                    masonryInitialized = true;
			                                } else {
			                                	storyCont.masonry('appended', storiesHtmlElem);
			                                }
			                                storiesHtmlElem.show();
			                                
			                                $('.StoriesBox').unbind('click').click(function(){
			                                	$np.addStory($(this));
			                                });
			                                			                                
			                                storiesHtml = "";
			                                pageNum++;
			                                
			                                if( pageNum <= totalPages ){
			                                	getStoriesForAll();
			                                }
		    								
		    							}		                                
		    						});
		    					}
		    				}
		    			}
		    			
		    			if( !storiesHtml ){
		    				storyCont.append('No stories available to display.')
		    			}	    				
	    			}
	    		});
			})();
    	}
    	
    	function getStoriesForMine(){
			currAjaxCallId = new Date().getTime();
			
			(function(){
				var ourAjaxCallId = currAjaxCallId;
				
	    		TN.services.getStoriesAndWishListFiltered(custId, maxItems, pageNum, 'mine', catValue).done(function(json){
	    			
	    			if( ourAjaxCallId === currAjaxCallId ){
		    			if( !!json && json[0] ){
		    				totalPages = json[0].numPages;
		    				
		    				var storyArray = json[0].stories;
		    				
		    				if(!!storyArray){
		    					var numStories = storyArray.length;
		    					
		    					for( var i = 0; i < numStories; i++ ){
		    						var storyItem = storyArray[i];
		    						if( !!storyItem ){
		    							var storyToAdd = {
											headline: storyItem.headline,
											buzzId: storyItem.id,
											imageId:storyItem.imageId,
											elementId:0,
											storyThumbImg: storyItem.imageThumbUrl,
											storyOriginalImg: storyItem.imageOriginalUrl,
											buzzWhat:storyItem.pwhat,
											buzzWhen:storyItem.pwhen,
											buzzWhere:storyItem.pwhere,
											buzzWhy:storyItem.pwhy,
											buzzWho:storyItem.pwho,
											buzzHow:storyItem.phow,
											buzzOped:storyItem.oped,
		                                    originatorImageUrl: storyItem.custImageOriginalUrl,
											originatorName: storyItem.author,
											elapsedTime: !!storyItem.date ? storyItem.date : ''
										};
		    							storiesHtml += getStoryHtml(storyToAdd);
		    						}
		    					}
		    					
		        				if( !!storiesHtml ){
		        					var storiesHtmlElem = $(storiesHtml);
		        					storyCont.append(storiesHtmlElem);
		        					storiesHtmlElem.hide();
		        					storyCont.imagesLoaded(function(){		        						
		        						if( ourAjaxCallId === currAjaxCallId ){
			                                if(!masonryInitialized){
			                                	storyCont.masonry({
			                                        // options
			                                        itemSelector : '.StoriesBox',
			                                        isFitWidth: true,
			                                        columnWidth : 178
			                                    });
			                                    masonryInitialized = true;
			                                } else {
			                                	storyCont.masonry('appended', storiesHtmlElem);
			                                }
			                                storiesHtmlElem.show();
			                                
			                                $('.StoriesBox').unbind('click').click(function(){
			                                	$np.addStory($(this));
			                                });
			                                
			                                storiesHtml = "";
			                                pageNum++;
			                                
			                                if( pageNum <= totalPages ){
			                                	getStoriesForMine();
			                                }                        		        							
		        						}		        						
		        					});
		        				}    				
		    				}
		    			}
		    			if( !storiesHtml ){
		    				storyCont.append('No stories available to display.')
		    			}	    				
	    			}	    			
	    		});    		
			})();
			
    	}
    	
    	if( loadType === 'all' ){
    		resetStoriesBoard();
    		getStoriesForAll();
    	}
    	
    	if( loadType === 'mine' ){
    		resetStoriesBoard();
    		getStoriesForMine();
    	}
    }
    
    function loadNewspaper(npId){
    	return;
    }
    
    function loadNewspapers(){
    	function populateNewspapersListDropDown(json){
    		if( !!json && json.length ){
    			var numNewspapers = json.length;
    			var newspaperListElem = $('#npList');
    			newspaperListElem.empty();
    			for( var i = 0; i < numNewspapers; i++ ){
    				var currNewspaper = json[i];
    				var currNewspaperElem = $('<li id="' + currNewspaper.id + '"><a href="javascript:void(0)">' + currNewspaper.headline + '</a></li>');
    				currNewspaperElem.click(function(){
    					var jqElem = $(this);
    					var currId = parseFloat(jqElem.attr('id'));
    					loadNewspaper(currId);
    				});
    				newspaperListElem.append(currNewspaperElem)
    			}
    		}    		
    	}
    	
    	TN.services.getNewspapers(custId).
		done(populateNewspapersListDropDown).
		fail(function(jqXHR){
			if( jqXHR.status === 404 ){
    			$('#npList').empty().append('<li><a href="javascript:void(0);">Your have not created any Newspapers yet.</a><li>');
			}
		});
    }
	
	$np.initialize = function(){
		custId = TN.utils.getCookie('TNUser');

		loadCategories();
		
		allBtnElem.click(function(){
			if (!allBtnElem.hasClass('active')){
				allBtnElem.addClass('active');
				mineBtnElem.removeClass('active');
				loadType = 'all';
				loadStories();
			}
		});
		
		mineBtnElem.click(function(){
			if (!mineBtnElem.hasClass('active')){
				mineBtnElem.addClass('active');
				allBtnElem.removeClass('active');
				loadType = 'mine';
				loadStories();
			}
		});
		
		searchFieldElem.keyup(function(event){
			if( event.which === 13 ){
				mineBtnElem.removeClass('active');
				allBtnElem.removeClass('active');
				loadType = '';
		        TN.utils.setDropdownValue(catListElem, 'All Categories');
		        catValue = '';
				searchStories(searchFieldElem.val());
			}
		});
		
		loadNewspapers();
	};
})(TN.newspaper);