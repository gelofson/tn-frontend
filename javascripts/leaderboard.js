if (!TN) var TN= {};
if (!TN.leaderboard) TN.leaderboard= {};

(function($leaderboard){
	
	function getFullDateStr(dateObj) {
		var month = dateObj.getMonth()+1;
		var day = dateObj.getDate();
		if (month < 10) {
			month = '0' + month;
		}
		if (day < 10) {
			day = '0' + day;
		}
		return dateObj.getFullYear() + '/' + month + '/' + day;
	}

	function getDateStr(dateObj) {
		return (dateObj.getMonth()+1) + '/' + dateObj.getDate();
	}

	$leaderboard.initUser = function(){
		
		TN.baseMypage.leftColInit.viewedUserInfo.done(function(){
			$('#leaderboard_username').text(TN.viewedUserInfo.firstName);
			
			// If viewing other people's leaderboard
			if (viewId != custId) {
				$('#leaderboard_username').attr('href', 'leaderboard-user.html?view=' + viewId);
				$('#my_likes').append('Likes');
				$('#my_dislikes').append('Dislikes');
			}
			// If viewing one's own leaderboard
			else {
				$('#my_likes').append('My Likes');
				$('#my_dislikes').append('My Dislikes');
			}

		});
		
		google.load("visualization", "1", {packages:["corechart", "imagechart"]});
		google.setOnLoadCallback(drawChart1);
		// Dislikes per day chart buggy, commenting out, BD-295
		// google.setOnLoadCallback(drawChart2);
		google.setOnLoadCallback(drawChart3);
		// Most talked about, buggy and therefore commented out
		// google.setOnLoadCallback(drawChart4);
		// Most Buzzworthy Newspapers, https://tinynews1.atlassian.net/browse/BD-298, buggy WS & therefore commented out
		// google.setOnLoadCallback(drawChart5);
		google.setOnLoadCallback(drawChart6);

		function drawChart1() {

			var today = new Date();
			var likesArray = [];
			var tempLikesArray = [];
			var fetched = 0;
			var tempArrayObject = [];
			var tempDateStr, tempFullDateStr;

			for (var i=0; i<=5; i++) {
			
				if (i > 0) today.setDate(today.getDate()-1);
				tempDateStr = getDateStr(today);
				tempFullDateStr = getFullDateStr(today);

				(function(saved_tempFullDateStr, saved_tempDateStr, saved_i, saved_tempDate){

					TN.services.getLikesPerDay(viewId, tempFullDateStr).done(function(msg){
						//console.log('saved_tempDateStr '+saved_tempDateStr);
						fetched++;
						//console.log(msg[0], typeof msg[0]);
						//likesArray.push([saved_tempDateStr, msg[0]]);
						tempArrayObject[saved_i] = { saved_tempDate: saved_tempDate, saved_tempDateStr: saved_tempDateStr, likenum: msg[0] };
						
						if (fetched == 6) {
							/*tempArrayObject.sort(function(a,b){
							var c = new Date(a.saved_tempDate);
							var d = new Date(b.saved_tempDate);
							return d-c;
							});*/
							//console.log(tempArrayObject);
							//test = tempArrayObject;
							for (j=tempArrayObject.length-1; j>=0; j--) {
								likesArray.push([tempArrayObject[j].saved_tempDateStr, tempArrayObject[j].likenum]);
							}
							
							likesArray.unshift(['Day', 'Likes']);
							//console.log(likesArray);
							var data = google.visualization.arrayToDataTable(likesArray);
							var options = {
								title: '',
								vAxis: { viewWindowMode: 'explicit',
										 viewWindow: { min: 0 }
										},
								chartArea: { width: 350 },
								legend: { position: 'none' },
								fontSize: 13
							};
							var chart = new google.visualization.LineChart(document.getElementById('chart_likes'));
							chart.draw(data, options);
							//console.log(fetched, likesArray);
						}
					});

				})(tempFullDateStr, tempDateStr, i, today);

			}

		};

		function drawChart2(){
			
			var today = new Date();
			var likesArray = [];
			var tempLikesArray = [];
			var fetched = 0;
			var tempArrayObject = [];
			var tempDateStr, tempFullDateStr;

			for (var i=0; i<=5; i++) {
			
				if (i > 0) today.setDate(today.getDate()-1);
				tempDateStr = getDateStr(today);
				tempFullDateStr = getFullDateStr(today);

				(function(saved_tempFullDateStr, saved_tempDateStr, saved_i, saved_tempDate){

					TN.services.getDislikesPerDay(viewId, tempFullDateStr).done(function(msg){
						//console.log('saved_tempDateStr '+saved_tempDateStr);
						fetched++;
						//console.log(msg[0], typeof msg[0]);
						//likesArray.push([saved_tempDateStr, msg[0]]);
						tempArrayObject[saved_i] = { saved_tempDate: saved_tempDate, saved_tempDateStr: saved_tempDateStr, likenum: msg[0] };
						
						if (fetched == 6) {
							/*tempArrayObject.sort(function(a,b){
							var c = new Date(a.saved_tempDate);
							var d = new Date(b.saved_tempDate);
							return d-c;
							});*/
							//console.log(tempArrayObject);
							//test = tempArrayObject;
							for (j=tempArrayObject.length-1; j>=0; j--) {
							likesArray.push([tempArrayObject[j].saved_tempDateStr, tempArrayObject[j].likenum]);
							}
							
							likesArray.unshift(['Day', 'Dislikes']);
							//console.log(likesArray);
							var data = google.visualization.arrayToDataTable(likesArray);
							var options = {
								title: '',
								vAxis: { viewWindowMode: 'explicit',
										 viewWindow: { min: 0 }
										},
								chartArea: { width: 350 },
								legend: { position: 'none' },
								fontSize: 13
							};
							var chart = new google.visualization.LineChart(document.getElementById('chart_dislikes'));
							chart.draw(data, options);
							//console.log(fetched, likesArray);
						}

					});

				})(tempFullDateStr, tempDateStr, i, today);

			}

		};

		function drawChart3() {

			var storiesArray = [['Story', 'Likes']];
			var maxXAxis = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			function mouseEventHandler(event)  {
				var storyId = 0;
				if (event.region.indexOf('bar0') != -1) {
					var barN = event.region.split('_')[1];
					storyId = regionToIdMap[barN];
					TN.utils.openStoryLB(storyId);
				}
				else if (event.region.indexOf('axis0') != -1) {
					var axisN = event.region.split('_')[1];
					storyId = regionToIdMapReversed[axisN];
					TN.utils.openStoryLB(storyId);
				}
			};

			TN.services.getMostLikedStory(viewId).done(function(msg){
				var mostLikedMaxNum = (msg.length > 5) ? 5 : msg.length;

				msg.sort(function(a,b){return b.like_counter-a.like_counter});

				var i = 0;
				var limiter = 20;
				var numPushed = 0;
				while ( i<limiter ) {
					if(!!msg[i]){
						if ( !( (msg[i].like_counter == 0 ) || (msg[i].headline == 'Message Board') || TN.utils.isBlank(msg[i].headline) ) ) {
							if (numPushed<mostLikedMaxNum) {
								if ( numPushed==0 ) maxXAxis = msg[i].like_counter;
								numPushed++;
								storiesArray.push([msg[i].headline, msg[i].like_counter]);
								regionToIdMap[numPushed-1] = msg[i].id;

							};
						};
					};
					i++;
				};
				// Hack with slice function to make a clone of regionToIdMap
				regionToIdMapReversed = regionToIdMap.slice(0);
				regionToIdMapReversed.reverse();

				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable(storiesArray);
				var options = {};
				// 'bhg' is a horizontal grouped bar chart in the Google Chart API.
				// The grouping is irrelevant here since there is only one numeric column.
				options.cht = 'bhg';
				// Add a data range.
				var min = 0;
				var max = maxXAxis;
				options.chds = min + ',' + max;
				// Now add data point labels at the end of each bar.
				// Add meters suffix to the labels.
				var meters = 'N** likes';
				// Draw labels in pink.
				var color = '000000';
				// Google Chart API needs to know which column to draw the labels on.
				// Here we have one labels column and one data column.
				// The Chart API doesn't see the label column.	From its point of view,
				// the data column is column 0.
				var index = 0;
				// -1 tells Google Chart API to draw a label on all bars.
				var allbars = -1;
				// 13 pixels font size for the labels.
				var fontSize = 13;
				// Priority is not so important here, but Google Chart API requires it.
				var priority = 0;
				options.chm = [meters, color, index, allbars, fontSize, priority].join(',');
				options.chxs = ['0,676767,13,0,l,676767'];
				options.enableEvents = true;
				// Create and draw the visualization.
				var chart = new google.visualization.ImageChart(document.getElementById('chart_most_buzzed_story'));
				
				chart.draw(data, options);
				
				google.visualization.events.addListener(chart, 'onclick', mouseEventHandler);
				//if (numMostLikedDisplayed == 0) $('#chart_most_buzzed_story').append('No liked stories yet');
			});

		};

		function drawChart4() {

			var storiesArray = [['Story', 'Comments']];
			var maxXAxis = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			function mouseEventHandler(event)  {
				if (event.region.indexOf('bar0') != -1) {
					var barN = event.region.split('_')[1];
					var storyId = regionToIdMap[barN];
					TN.utils.openStoryLB(storyId);
				}
				else if (event.region.indexOf('axis0') != -1) {
					var axisN = event.region.split('_')[1];
					var storyId = regionToIdMapReversed[axisN];
					TN.utils.openStoryLB(storyId);
				}
			};

			TN.services.getMostCommentedStories(viewId).done(function(msg){
				var maxNum = (msg.length > 5) ? 5 : msg.length;

				msg.sort(function(a,b){return b.comments-a.comments});

				var i = 0;
				var limiter = 200;
				var numPushed = 0;
				while ( i<limiter ) {
					if(!!msg[i]){
						if ( !( (msg[i].comments == 0 ) || (msg[i].headline == 'Message Board') || TN.utils.isBlank(msg[i].headline) ) ) {
							if (numPushed<maxNum) {
								if ( numPushed==0 ) maxXAxis = msg[i].comments;
								numPushed++;
								storiesArray.push([msg[i].headline, msg[i].comments]);
								regionToIdMap[numPushed-1] = msg[i].id;
							}
						};
					};
					i++;
				};
				// Hack with slice function to make a clone of regionToIdMap
				regionToIdMapReversed = regionToIdMap.slice(0);
				regionToIdMapReversed.reverse();

				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable(storiesArray);
				var options = {};
				// 'bhg' is a horizontal grouped bar chart in the Google Chart API.
				// The grouping is irrelevant here since there is only one numeric column.
				options.cht = 'bhg';
				// Add a data range.
				var min = 0;
				var max = maxXAxis;
				options.chds = min + ',' + max;
				// Now add data point labels at the end of each bar.
				// Add meters suffix to the labels.
				var meters = 'N** comments';
				// Draw labels in pink.
				var color = '000000';
				// Google Chart API needs to know which column to draw the labels on.
				// Here we have one labels column and one data column.
				// The Chart API doesn't see the label column.	From its point of view,
				// the data column is column 0.
				var index = 0;
				// -1 tells Google Chart API to draw a label on all bars.
				var allbars = -1;
				// 13 pixels font size for the labels.
				var fontSize = 13;
				// Priority is not so important here, but Google Chart API requires it.
				var priority = 0;
				options.chm = [meters, color, index, allbars, fontSize, priority].join(',');
				options.chxs = ['0,676767,13,0,l,676767'];
				options.enableEvents = true;
				// Create and draw the visualization.
				var chart = new google.visualization.ImageChart(document.getElementById('chart_most_talked_story'));

				chart.draw(data, options);

				google.visualization.events.addListener(chart, 'onclick', mouseEventHandler);
				//if (numMostLikedDisplayed == 0) $('#chart_most_buzzed_story').append('No liked stories yet');
			});

		};

		function drawChart5(){

			var npArray = [[''], ['']];
			var numPushed = 0;
			
			TN.services.mostRead(viewId).done(function(msg){
				for(i=0; i<5; i++) {
					if (!!msg[i]) {
						numPushed++;
						npArray[0].push(msg[i].headline);
						npArray[1].push(msg[i].count);
					};
				};
				if (numPushed == 0) $('#chart_most_buzzed_videos').text('No liked videos currently.');
				else {
					var wrapper = new google.visualization.ChartWrapper({
						chartType: 'ColumnChart',
						dataTable: npArray,
						options: {'title': 'Reads'},
						containerId: 'chart_most_buzzed_newspapers'
					});
					wrapper.draw();
				};
			});

		};
		
		function drawChart6(){

			var npArray = [[''], ['']];
			var numPushed = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			TN.services.getTopLikedVideosForUser(viewId, 6).done(function(msg){
					for(i=0; i<6; i++) {
						if (!!msg[i]) {
							numPushed++;
							npArray[0].push(msg[i].headline);
							npArray[1].push(msg[i].likeCount);
							regionToIdMap[numPushed] = msg[i].buzzId;
						};
					};
					//console.log(npArray);
					if (numPushed == 0) $('#chart_most_buzzed_videos').text('No liked videos currently.');
					else {
						var wrapper = new google.visualization.ChartWrapper({
							chartType: 'ColumnChart',
							dataTable: npArray,
							options: {
								'title': 'Likes',
								chartArea: { width: 200 },
								fontSize: 13
							},
							containerId: 'chart_most_buzzed_videos'
						});

						google.visualization.events.addListener(wrapper, 'ready', function() {
							google.visualization.events.addListener(wrapper.getChart(), 'select', function() {
								chartObject = wrapper.getChart();
								if (typeof chartObject.getSelection()[0] !== "undefined") {
									var columnN = chartObject.getSelection()[0].column;
									var storyId = regionToIdMap[columnN];
									TN.utils.openStoryLB(storyId);
								}
							});
						});

						wrapper.draw();
					};
			}).fail(function(){
				$('#chart_most_buzzed_videos').text('Problem while fetching video likes statistics.');
			});

		};
		
	};

	$leaderboard.initAll = function(){

		TN.baseMypage.leftColInit.viewedUserInfo.done(function(){
			$('#leaderboard_username').text(TN.viewedUserInfo.firstName);
			if (viewId != custId) $('#leaderboard_username').attr('href', 'leaderboard-user.html?view=' + viewId);
		});
		
		//$('#location_indicator').text('All of Tiny News');

		drawChart1();
		drawChart2();
		drawChart3();

		google.load("visualization", "1", {packages:["corechart", "imagechart"]});
		google.setOnLoadCallback(drawChart4);
		google.setOnLoadCallback(drawChart5);
		google.setOnLoadCallback(drawChart6);
		google.setOnLoadCallback(drawChart7);
		google.setOnLoadCallback(drawChart8);
		
		function drawChart1() {
			TN.services.getMostLikedReporter().done(function(msg){
				
				// For now WS returns only 1 item..
				for(var i=0; i<1; i++){
					$("#best_actor_0").mambo({
						image: msg[0].thumbnail,
						circleColor: '#FFFFFF',
						circleBorder: '#3399CC',
						ringColor: "#389BB8",
						drawShadow: true
					});
					$('#best_actor_' + i + '_likescounter').text(msg[i].likeCount + ' likes');
					$('#best_actor_' + i + '_username').html('<a href="mypage.html?view=' + msg[i].customerID + '">' + msg[i].name + '</a>');
				}
			});

		};

		function drawChart2() {
			TN.services.getTopStoryLikers(5).done(function(msg){
				
				for(var i=0; i<5; i++){
					$("#congeniality_award_"+i).mambo({
						image: msg[i].thumbImageUrl,
						circleColor: '#FFFFFF',
						circleBorder: '#3399CC',
						ringColor: "#389BB8",
						drawShadow: true
					});
					$('#congeniality_award_' + i + '_likescounter').text(msg[i].totalLikedStories + ' likes');
					$('#congeniality_award_' + i + '_username').html('<a href="mypage.html?view=' + msg[i].uniqueId + '">' + msg[i].firstname + '</a>');
				}

			});

		};

		function drawChart3() {
			TN.services.getTopStoryCommentContributors(5).done(function(msg){
				
				for(var i=0; i<5; i++){
					$("#chatty_cathy_"+i).mambo({
						image: msg[i].thumbImageUrl,
						circleColor: '#FFFFFF',
						circleBorder: '#3399CC',
						ringColor: "#389BB8",
						drawShadow: true
					});
					$('#chatty_cathy_' + i + '_likescounter').html(msg[i].totalCommentsPosted + ' <br>comments');
					$('#chatty_cathy_' + i + '_username').html('<a href="mypage.html?view=' + msg[i].uniqueId + '">' + msg[i].firstname + '</a>');
				}

			});

		};
		
		function drawChart4() {

			var storiesArray = [['Story', 'Reads']];
			var maxXAxis = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			function mouseEventHandler(event)  {
				if (event.region.indexOf('bar0') != -1) {
					var barN = event.region.split('_')[1];
					var storyId = regionToIdMap[barN];
					TN.utils.openStoryLB(storyId);
				}
				else if (event.region.indexOf('axis0') != -1) {
					var axisN = event.region.split('_')[1];
					var storyId = regionToIdMapReversed[axisN];
					TN.utils.openStoryLB(storyId);
				}
			};

			// Display user's most liked stories:
			TN.services.getMostReadStories(5).done(function(msg){
				var maxNum = (msg.length > 5) ? 5 : msg.length;

				msg.sort(function(a,b){return b.countOfReadStories-a.countOfReadStories});

				var i = 0;
				var limiter = 5;
				var numPushed = 0;
				while ( i<limiter ) {
					if ( !( (msg[i].countOfReadStories == 0 ) || (msg[i].headline == 'Message Board') || TN.utils.isBlank(msg[i].headline) ) ) {
						if (numPushed<maxNum) {
							if ( numPushed==0 ) maxXAxis = msg[i].countOfReadStories;
							numPushed++;
							storiesArray.push([msg[i].headline, msg[i].countOfReadStories]);
							regionToIdMap[numPushed-1] = msg[i].storyId;
						}
					};
					i++;
				};
				// Hack with slice function to make a clone of regionToIdMap
				regionToIdMapReversed = regionToIdMap.slice(0);
				regionToIdMapReversed.reverse();

				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable(storiesArray);
				var options = {};
				// 'bhg' is a horizontal grouped bar chart in the Google Chart API.
				// The grouping is irrelevant here since there is only one numeric column.
				options.cht = 'bhg';
				// Add a data range.
				var min = 0;
				var max = maxXAxis;
				options.chds = min + ',' + max;
				// Now add data point labels at the end of each bar.
				// Add meters suffix to the labels.
				var meters = 'N** reads';
				// Draw labels in pink.
				var color = '000000';
				// Google Chart API needs to know which column to draw the labels on.
				// Here we have one labels column and one data column.
				// The Chart API doesn't see the label column.	From its point of view,
				// the data column is column 0.
				var index = 0;
				// -1 tells Google Chart API to draw a label on all bars.
				var allbars = -1;
				// 13 pixels font size for the labels.
				var fontSize = 13;
				// Priority is not so important here, but Google Chart API requires it.
				var priority = 0;
				options.chm = [meters, color, index, allbars, fontSize, priority].join(',');
				options.chxs = ['0,676767,13,0,l,676767'];
				options.enableEvents = true;
				// Create and draw the visualization.
				var chart = new google.visualization.ImageChart(document.getElementById('best_original_screenplay_section'));
				chart.draw(data, options);
				google.visualization.events.addListener(chart, 'onclick', mouseEventHandler);
				//if (numMostLikedDisplayed == 0) $('#chart_most_buzzed_story').append('No liked stories yet');
			});

		};

		function drawChart5() {

			var storiesArray = [['Video story', 'Likes']];
			var maxLikes = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			function mouseEventHandler(event)  {
				if (event.region.indexOf('bar0') != -1) {
					var barN = event.region.split('_')[1];
					var storyId = regionToIdMap[barN];
					TN.utils.openStoryLB(storyId);
				}
				else if (event.region.indexOf('axis0') != -1) {
					var axisN = event.region.split('_')[1];
					var storyId = regionToIdMapReversed[axisN];
					TN.utils.openStoryLB(storyId);
				}
			};

			TN.services.getMostLikedVideos(5).done(function(msg){
				
				var numDisplayed = 0;
				var maxNum = (msg.length > 5) ? 5 : msg.length;

				msg.sort(function(a,b){return b.countOfVideos-a.countOfVideos});

				for(i=0; i<maxNum; i++) {
					if ( (msg[i].countOfVideos == 0 ) || (msg[i].headline == 'Message Board') || TN.utils.isBlank(msg[i].headline) ) continue;
					else {
						if ( i==0 ) maxLikes = msg[i].countOfVideos;
						storiesArray.push([msg[i].headline, msg[i].countOfVideos]);
						numDisplayed++;
						regionToIdMap[numDisplayed-1] = msg[i].storyId;
					}
				};
				// Hack with slice function to make a clone of regionToIdMap
				regionToIdMapReversed = regionToIdMap.slice(0);
				regionToIdMapReversed.reverse();

				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable(storiesArray);
				var options = {};
				// 'bhg' is a horizontal grouped bar chart in the Google Chart API.
				// The grouping is irrelevant here since there is only one numeric column.
				options.cht = 'bhg';
				// Add a data range.
				var min = 0;
				var max = maxLikes;
				options.chds = min + ',' + max;
				// Now add data point labels at the end of each bar.
				// Add meters suffix to the labels.
				var meters = 'N** likes';
				// Draw labels in pink.
				var color = '000000';
				// Google Chart API needs to know which column to draw the labels on.
				// Here we have one labels column and one data column.
				// The Chart API doesn't see the label column.	From its point of view,
				// the data column is column 0.
				var index = 0;
				// -1 tells Google Chart API to draw a label on all bars.
				var allbars = -1;
				// 13 pixels font size for the labels.
				var fontSize = 13;
				// Priority is not so important here, but Google Chart API requires it.
				var priority = 0;
				options.chm = [meters, color, index, allbars, fontSize, priority].join(',');
				options.chxs = ['0,676767,13,0,l,676767'];
				options.enableEvents = true;
				// Create and draw the visualization.
				var chart = new google.visualization.ImageChart(document.getElementById('best_motion_picture_section'));
				chart.draw(data, options);
				google.visualization.events.addListener(chart, 'onclick', mouseEventHandler);
				//if (numDisplayed == 0) ..

			});

		};

		function drawChart6() {

			var storiesArray = [['Video story', 'Reads']];
			var maxLikes = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			function mouseEventHandler(event)  {
				if (event.region.indexOf('bar0') != -1) {
					var barN = event.region.split('_')[1];
					var npId = regionToIdMap[barN];
					TN.newspaperViewLB.init(npId);
				}
				else if (event.region.indexOf('axis0') != -1) {
					var axisN = event.region.split('_')[1];
					var npId = regionToIdMapReversed[axisN];
					TN.newspaperViewLB.init(npId);
				}
			};

			TN.services.mostRead().done(function(msg){
				
				var numDisplayed = 0;
				var maxNum = (msg.length > 5) ? 5 : msg.length;

				msg.sort(function(a,b){return b.count-a.count});

				for(i=0; i<maxNum; i++) {
					if ( (msg[i].count == 0 ) || (msg[i].headline == 'Message Board') || TN.utils.isBlank(msg[i].headline) ) continue;
					else {
						if ( i==0 ) maxLikes = msg[i].count;
						storiesArray.push([msg[i].headline, msg[i].count]);
						numDisplayed++;
						regionToIdMap[numDisplayed-1] = msg[i].id;
					}
				};
				// Hack with slice function to make a clone of regionToIdMap
				regionToIdMapReversed = regionToIdMap.slice(0);
				regionToIdMapReversed.reverse();

				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable(storiesArray);
				var options = {};
				// 'bhg' is a horizontal grouped bar chart in the Google Chart API.
				// The grouping is irrelevant here since there is only one numeric column.
				options.cht = 'bhg';
				// Add a data range.
				var min = 0;
				var max = maxLikes;
				options.chds = min + ',' + max;
				// Now add data point labels at the end of each bar.
				// Add meters suffix to the labels.
				var meters = 'N** likes';
				// Draw labels in pink.
				var color = '000000';
				// Google Chart API needs to know which column to draw the labels on.
				// Here we have one labels column and one data column.
				// The Chart API doesn't see the label column.	From its point of view,
				// the data column is column 0.
				var index = 0;
				// -1 tells Google Chart API to draw a label on all bars.
				var allbars = -1;
				// 13 pixels font size for the labels.
				var fontSize = 13;
				// Priority is not so important here, but Google Chart API requires it.
				var priority = 0;
				options.chm = [meters, color, index, allbars, fontSize, priority].join(',');
				options.chxs = ['0,676767,13,0,l,676767'];
				options.enableEvents = true;
				// Create and draw the visualization.
				var chart = new google.visualization.ImageChart(document.getElementById('best_selling_newspaper_section'));
				chart.draw(data, options);
				google.visualization.events.addListener(chart, 'onclick', mouseEventHandler);
				//if (numDisplayed == 0) ..

			});

		};

		function drawChart7() {

			var storiesArray = [['Video story', 'Dislikes']];
			var maxLikes = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			function mouseEventHandler(event)  {
				if (event.region.indexOf('bar0') != -1) {
					var barN = event.region.split('_')[1];
					var storyId = regionToIdMap[barN];
					TN.utils.openStoryLB(storyId);
				}
				else if (event.region.indexOf('axis0') != -1) {
					var axisN = event.region.split('_')[1];
					var storyId = regionToIdMapReversed[axisN];
					TN.utils.openStoryLB(storyId);
				}
			};

			TN.services.getMostDislikedVideos().done(function(msg){
				
				var numDisplayed = 0;
				var maxNum = (msg.length > 5) ? 5 : msg.length;

				msg.sort(function(a,b){return b.countOfVideos-a.countOfVideos});

				for(i=0; i<maxNum; i++) {
					if ( (msg[i].countOfVideos == 0 ) || (msg[i].headline == 'Message Board') || TN.utils.isBlank(msg[i].headline) ) continue;
					else {
						if ( i==0 ) maxLikes = msg[i].countOfVideos;
						storiesArray.push([msg[i].headline, msg[i].countOfVideos]);
						numDisplayed++;
						regionToIdMap[numDisplayed-1] = msg[i].storyId;
					}
				};
				// Hack with slice function to make a clone of regionToIdMap
				regionToIdMapReversed = regionToIdMap.slice(0);
				regionToIdMapReversed.reverse();

				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable(storiesArray);
				var options = {};
				// 'bhg' is a horizontal grouped bar chart in the Google Chart API.
				// The grouping is irrelevant here since there is only one numeric column.
				options.cht = 'bhg';
				// Add a data range.
				var min = 0;
				var max = maxLikes;
				options.chds = min + ',' + max;
				// Now add data point labels at the end of each bar.
				// Add meters suffix to the labels.
				var meters = 'N** dislikes';
				// Draw labels in pink.
				var color = '000000';
				// Google Chart API needs to know which column to draw the labels on.
				// Here we have one labels column and one data column.
				// The Chart API doesn't see the label column.	From its point of view,
				// the data column is column 0.
				var index = 0;
				// -1 tells Google Chart API to draw a label on all bars.
				var allbars = -1;
				// 13 pixels font size for the labels.
				var fontSize = 13;
				// Priority is not so important here, but Google Chart API requires it.
				var priority = 0;
				options.chm = [meters, color, index, allbars, fontSize, priority].join(',');
				options.chxs = ['0,676767,13,0,l,676767'];
				options.enableEvents = true;
				// Create and draw the visualization.
				var chart = new google.visualization.ImageChart(document.getElementById('worst_motion_picture_section'));
				chart.draw(data, options);
				google.visualization.events.addListener(chart, 'onclick', mouseEventHandler);
				//if (numDisplayed == 0) ..

			});

		};

		function drawChart8() {

			var storiesArray = [['Story', 'Clips']];
			var maxLikes = 0;
			var regionToIdMap = [];
			var regionToIdMapReversed = [];

			function mouseEventHandler(event)  {
				if (event.region.indexOf('bar0') != -1) {
					var barN = event.region.split('_')[1];
					var storyId = regionToIdMap[barN];
					TN.utils.openStoryLB(storyId);
				}
				else if (event.region.indexOf('axis0') != -1) {
					var axisN = event.region.split('_')[1];
					var storyId = regionToIdMapReversed[axisN];
					TN.utils.openStoryLB(storyId);
				}
			};

			TN.services.getMostClippedStories().done(function(msg){
				
				var numDisplayed = 0;
				var maxNum = (msg.length > 5) ? 5 : msg.length;

				msg.sort(function(a,b){return b.countOfClippedStories-a.countOfClippedStories});

				for(i=0; i<maxNum; i++) {
					if ( (msg[i].countOfClippedStories == 0 ) || (msg[i].headline == 'Message Board') || TN.utils.isBlank(msg[i].headline) ) continue;
					else {
						if ( i==0 ) maxLikes = msg[i].countOfClippedStories;
						storiesArray.push([msg[i].headline, msg[i].countOfClippedStories]);
						numDisplayed++;
						regionToIdMap[numDisplayed-1] = msg[i].storyId;
					}
				};
				// Hack with slice function to make a clone of regionToIdMap
				regionToIdMapReversed = regionToIdMap.slice(0);
				regionToIdMapReversed.reverse();

				// Create and populate the data table.
				var data = google.visualization.arrayToDataTable(storiesArray);
				var options = {};
				// 'bhg' is a horizontal grouped bar chart in the Google Chart API.
				// The grouping is irrelevant here since there is only one numeric column.
				options.cht = 'bhg';
				// Add a data range.
				var min = 0;
				var max = maxLikes;
				options.chds = min + ',' + max;
				// Now add data point labels at the end of each bar.
				// Add meters suffix to the labels.
				var meters = 'N** clips';
				// Draw labels in pink.
				var color = '000000';
				// Google Chart API needs to know which column to draw the labels on.
				// Here we have one labels column and one data column.
				// The Chart API doesn't see the label column.	From its point of view,
				// the data column is column 0.
				var index = 0;
				// -1 tells Google Chart API to draw a label on all bars.
				var allbars = -1;
				// 13 pixels font size for the labels.
				var fontSize = 13;
				// Priority is not so important here, but Google Chart API requires it.
				var priority = 0;
				options.chm = [meters, color, index, allbars, fontSize, priority].join(',');
				options.chxs = ['0,676767,13,0,l,676767'];
				options.enableEvents = true;
				// Create and draw the visualization.
				var chart = new google.visualization.ImageChart(document.getElementById('best_forgery_section'));
				chart.draw(data, options);
				google.visualization.events.addListener(chart, 'onclick', mouseEventHandler);
				//if (numDisplayed == 0) ..

			});

		}
		
	};
	
})(TN.leaderboard);