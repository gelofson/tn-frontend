if(!TN) var TN = {};
if (!TN.baseNewsfeed) TN.baseNewsfeed = {};

(function($baseNewsfeed){

    var feedPage = 1;
    var okToLoadMore = true;

    var $el = $('.feed-container');
    var listView = new infinity.ListView($el);    

    $baseNewsfeed.MustacheTemplates = function(){
        
        var story_post_view = '\
            <div class="commentBox">\
                <div class="row">\
                    <div class="twelve column">\
                    <div class="row">\
                        <!--CommentBox User-->\
                        <div class="commentBox_User left">\
                            <a href="?view={{ item_author_id }}"><img style="display:none" onload="TN.utils.fwNormalizeImage(this, 68, 68);" src="{{ item_custThumb_url }}" alt="user"></a>\
                        </div><!--/commentBox_User-->\
                         <!--CommentBox Content-->\
                        <div class="commentBox_Content ">\
                             <h2>{{ item_author }} posted a new story</h2>\
                             <div class="row">\
                                <div class="twelve column">\
                                    <div class="commentBox_ContentStory">\
                                        <div class="row">\
                                        <div class="five column commentBox_ContentStoryImgCont">\
                                            <a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="{{ story_post_image }}" alt="user story image"></a>\
                                        </div><!--/five-->\
                                        <div class="commentBox_ContentStoryText five column">\
                                            <h3><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});">{{ item_title }}</a></h3>\
                                            {{ #story_post_who_section }}<p><b>Who:</b> {{ story_post_who }}</p>{{ /story_post_who_section }}\
                                            {{ #story_post_what_section }}<p><b>What:</b> {{ story_post_what }}</p>{{ /story_post_what_section }}\
                                            {{ #story_post_when_section }}<p><b>When:</b> {{ story_post_when }}</p>{{ /story_post_when_section }}\
                                            {{ #story_post_where_section }}<p><b>Where:</b> {{ story_post_where }}</p>{{ /story_post_where_section }}\
                                            {{ #story_post_why_section }}<p><b>Why:</b> {{ story_post_why }}</p>{{ /story_post_why_section }}\
                                            {{ #story_post_how_section }}<p><b>How:</b> {{ story_post_how }}</p>{{ /story_post_how_section }}\
                                            {{ #story_post_oped_section }}<p>{{ story_post_oped }}</p>{{ /story_post_oped_section }}\
                                            <br>\
                                        </div><!--/five-->\
                                        </div><!--/row-->\
                                    </div><!--/commentBox_ContentStory-->\
                                    <!--Comment Details-->\
                                    <div class="commentDetail storyPostCommentDetail">\
                                        <div class="row">\
                                        {{ #action_buttons }}<div class="twelve column">\
                                        <div class="left">\
                                            <ul class="no-bullet commentBoxList">\
                                                <li class="like"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/like.png" width="11" height="12" alt="like"> like</a></li>\
                                                <li class="dislike"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/dislike.png" width="10" height="13" alt="dislike"> dislike</a></li>\
                                                <li class="comment"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/comment.png" width="14" height="13" alt="comment"> comment</a></li>\
                                                <li class="share"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/share.png" width="13" height="13" alt="share"> share</a></li>\
                                                <li class="reclip"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/reclip.png" width="15" height="17" alt="reclip"> reclip</a></li>\
                                            </ul>\
                                        </div><!--/left-->\
                                        <div class="right">\
                                            <p class="timePosted">{{ time_ago }}</p>\
                                        </div><!--/right-->\
                                        </div><!--/twelve-->\
                                        </div><!--/row-->\
                                    </div><!--/commentDetail-->{{ /action_buttons }}\
                                </div><!--/twelve-->\
                             </div><!--/row-->\
                        </div><!--/commentBox_Content-->\
                    </div><!--/row-->\
                    </div><!--/twelve-->\
                </div><!--/row-->\
            </div>';

        var video_post_view = '\
            <div class="commentBox">\
                <div class="row">\
                    <div class="twelve column">\
                    <div class="row">\
                        <!--CommentBox User-->\
                        <div class="commentBox_User left">\
                            <a href="?view={{ item_author_id }}"><img style="display:none" onload="TN.utils.fwNormalizeImage(this, 68, 68);" src="{{ item_custThumb_url }}" alt="user"></a>\
                        </div><!--/commentBox_User-->\
                         <!--CommentBox Content-->\
                        <div class="commentBox_Content ">\
                             <h2 class="">{{ item_author }} posted a video</h2>\
                                    <div class="videoContainer">\
                                    <iframe width="565" height="320" src="{{ obj_url }}"></iframe>\
                                    </div><!--/videoContainer-->\
                                    <!--Comment Details-->\
                                    {{ #action_buttons }}<div class="commentDetail videoPostCommentDetail">\
                                        <div class="left">\
                                            <ul class="no-bullet commentBoxList">\
                                                <li class="like"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/like.png" width="11" height="12" alt="like"> like</a></li>\
                                                <li class="dislike"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/dislike.png" width="10" height="13" alt="dislike"> dislike</a></li>\
                                                <li class="comment"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/comment.png" width="14" height="13" alt="comment"> comment</a></li>\
                                                <li class="share"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/share.png" width="13" height="13" alt="share"> share</a></li>\
                                                <li class="reclip"><a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});"><img src="images/myPage/reclip.png" width="15" height="17" alt="reclip"> reclip</a></li>\
                                            </ul>\
                                        </div><!--/left-->\
                                        <!--div class="right"-->\
                                            <p class="timePosted"><span class="innerSpanTimePosted">{{ time_ago }}</span></p>\
                                        <!--/div--><!--/right-->\
                                    </div><!--/commentDetail-->{{ /action_buttons }}\
                        </div><!--/commentBox_Content-->\
                    </div><!--/row-->\
                    </div><!--/twelve-->\
                </div><!--/row-->\
            </div>';
                   
        var story_liked_disliked_shared_commented_read_view = '\
            <div class="commentBox">\
                <div class="row">\
                    <div class="twelve column">\
                    <div class="row">\
                        <!--CommentBox User-->\
                        <!--div class="commentBox_User left">\
                            <a href="?view={{ item_author_id }}"><img style="display:none" onload="TN.utils.fwNormalizeImage(this, 68, 68);" src="{{ item_custThumb_url }}" alt="user"></a>\
                        </div--><!--/commentBox_User-->\
                         <!--CommentBox Content-->\
                        <div class="commentBox_Content ">\
                            <h2><a href="?view={{ item_author_id }}">{{ item_author }}</a>\
                                {{ #item_action_like }} liked <span class="userLike"></span> the {{ /item_action_like }}\
                                {{ #item_action_dislike }} disliked <span class="userdisLike"></span> the {{ /item_action_dislike }}\
                                {{ #item_action_share }} shared the {{ /item_action_share }}\
                                {{ #item_action_comment }} commented on the {{ /item_action_comment }}\
                                {{ #item_action_read }} read the {{ /item_action_read }}\
                            <a href="javascript:TN.utils.openStoryLB({{ item_obj_id }});">{{ item_title }}</a></h2>\
                            {{ #item_action_comment }}<p>“{{ comment_content }}”</p>{{ /item_action_comment }}\
                            <div class="row">\
                                <div class="twelve column">\
                                    <!--Comment Details-->\
                                    <div class="commentDetail">\
                                        <div class="row">\
                                        {{ #action_buttons }}<div class="twelve column">\
                                        <div class="left">\
                                            <ul class="no-bullet commentBoxList">\
                                                <li class="like"><a href="#"><img src="images/myPage/like.png" width="11" height="12" alt="like"> like</a></li>\
                                                <li class="dislike"><a href="#"><img src="images/myPage/dislike.png" width="10" height="13" alt="dislike"> dislike</a></li>\
                                                <li class="comment"><a href="#"><img src="images/myPage/comment.png" width="14" height="13" alt="comment"> comment</a></li>\
                                                <li class="share"><a href="#"><img src="images/myPage/share.png" width="13" height="13" alt="share"> share</a></li>\
                                                <li class="reclip"><a href="#"><img src="images/myPage/reclip.png" width="15" height="17" alt="reclip"> reclip</a></li>\
                                            </ul>\
                                        </div><!--/left-->\
                                        <div class="right">\
                                            <p class="timePosted">{{ time_ago }}</p>\
                                        </div><!--/right-->\
                                        </div><!--/twelve-->{{ /action_buttons }}\
                                        </div><!--/row-->\
                                    </div><!--/commentDetail-->\
                                </div><!--/twelve-->\
                             </div><!--/row-->\
                        </div><!--/commentBox_Content-->\
                    </div><!--/row-->\
                    </div><!--/twelve-->\
                </div><!--/row-->\
            </div>';

        var video_liked_disliked_shared_commented_read_view = '\
            <div class="commentBox">\
                <div class="row">\
                    <div class="twelve column">\
                    <div class="row">\
                        <!--CommentBox User-->\
                        <!--div class="commentBox_User left">\
                            <a href="?view={{ item_author_id }}"><img style="display:none" onload="TN.utils.fwNormalizeImage(this, 68, 68);" src="{{ item_custThumb_url }}" alt="user"></a>\
                        </div--><!--/commentBox_User-->\
                         <!--CommentBox Content-->\
                        <div class="commentBox_Content ">\
                             <div class="row">\
                                <div class="ten column offset-by-two watch-video-position">\
                                    <div class="commentBox_ContentStory">\
                                        <div class="row">\
                                        <div class="five column">\
                                            <iframe width="400" height="227" src="{{ obj_url }}"></iframe>\
                                        </div><!--/five-->\
                                        <div class="commentBox_ContentStoryText seven column">\
                                            <h2><a href="?view={{ item_author_id }}">{{ item_author }}</a>\
                                                {{ #item_action_like }} liked <span class="userLike"></span> the {{ /item_action_like }}\
                                                {{ #item_action_dislike }} disliked <span class="userdisLike"></span> the {{ /item_action_dislike }}\
                                                {{ #item_action_share }} shared the {{ /item_action_share }}\
                                                {{ #item_action_comment }} commented on the {{ /item_action_comment }}\
                                                {{ #item_action_read }} read the {{ /item_action_read }}\
                                            <a href="javascript:;">{{ item_title }}</a></h2>\
                                            {{ #item_action_comment }}<p>“{{ comment_content }}”</p>{{ /item_action_comment }}\
                                        </div><!--/seven-->\
                                        </div><!--/row-->\
                                    </div><!--/commentBox_ContentStory-->\
                                    <!--Comment Details-->\
                                </div><!--/twelve-->\
                             </div><!--/row-->\
                             <div class="commentDetail">\
                                <div class="row">\
                                {{ #action_buttons }}<div class="twelve column">\
                                <div class="left">\
                                     <ul class="no-bullet commentBoxList">\
                                         <li class="like"><a href="#"><img src="images/myPage/like.png" width="11" height="12" alt="like"> like</a></li>\
                                         <li class="dislike"><a href="#"><img src="images/myPage/dislike.png" width="10" height="13" alt="dislike"> dislike</a></li>\
                                         <li class="comment"><a href="#"><img src="images/myPage/comment.png" width="14" height="13" alt="comment"> comment</a></li>\
                                         <li class="share"><a href="#"><img src="images/myPage/share.png" width="13" height="13" alt="share"> share</a></li>\
                                         <li class="reclip"><a href="#"><img src="images/myPage/reclip.png" width="15" height="17" alt="reclip"> reclip</a></li>\
                                     </ul>\
                                </div><!--/left-->\
                                <div class="right">\
                                    <p class="timePosted">{{ time_ago }}</p>\
                                </div><!--/right-->\
                                </div><!--/twelve-->{{ /action_buttons }}\
                                </div><!--/row-->\
                             </div><!--/commentDetail-->\
                        </div><!--/commentBox_Content-->\
                    </div><!--/row-->\
                    </div><!--/twelve-->\
                </div><!--/row-->\
            </div>';

        var newspaper_published_liked_disliked_shared_commented_read_view = '\
            <div class="commentBox">\
                <div class="row">\
                    <div class="twelve column">\
                    <div class="row">\
                        <!--CommentBox User-->\
                        <!--div class="commentBox_User left ">\
                            <a href="?view={{ item_author_id }}"><img style="display:none" onload="TN.utils.fwNormalizeImage(this, 68, 68);" src="{{ item_custThumb_url }}" alt="user"></a>\
                        </div--><!--/commentBox_User-->\
                         <!--CommentBox Content-->\
                        <div class="commentBox_Content ">\
                            <h2><a href="?view={{ item_author_id }}">{{ item_author }}</a>\
                                {{ #item_action_publish }} published the latest edition of {{ /item_action_publish }}\
                                {{ #item_action_like }} liked <span class="userLike"></span> the {{ /item_action_like }}\
                                {{ #item_action_dislike }} disliked <span class="userdisLike"></span> the {{ /item_action_dislike }}\
                                {{ #item_action_share }} shared the {{ /item_action_share }}\
                                {{ #item_action_comment }} commented on the {{ /item_action_comment }}\
                                {{ #item_action_read }} read the {{ /item_action_read }}\
                            <a href="javascript:TN.newspaperViewLB.init({{ item_obj_id }});">{{ item_title }}</a></h2>\
                            <div class="row">\
                                <div class="twelve column">\
                                    <!--Comment Details-->\
                                    <div class="commentDetail">\
                                        <div class="row">\
                                        {{ #action_buttons }}<div class="twelve column">\
                                        <div class="left">\
                                            <ul class="no-bullet commentBoxList">\
                                                <li class="like"><a href="#"><img src="images/myPage/like.png" width="11" height="12" alt="like"> like</a></li>\
                                                <li class="dislike"><a href="#"><img src="images/myPage/dislike.png" width="10" height="13" alt="dislike"> dislike</a></li>\
                                                <li class="comment"><a href="#"><img src="images/myPage/comment.png" width="14" height="13" alt="comment"> comment</a></li>\
                                                <li class="share"><a href="#"><img src="images/myPage/share.png" width="13" height="13" alt="share"> share</a></li>\
                                                <li class="reclip"><a href="#"><img src="images/myPage/reclip.png" width="15" height="17" alt="reclip"> reclip</a></li>\
                                            </ul>\
                                        </div><!--/left-->\
                                        <div class="right">\
                                             <p class="timePosted">{{ time_ago }}</p>\
                                        </div><!--/right-->\
                                        </div><!--/twelve-->{{ /action_buttons }}\
                                        </div><!--/row-->\
                                    </div><!--/commentDetail-->\
                                </div><!--/twelve-->\
                             </div><!--/row-->\
                        </div><!--/commentBox_Content-->\
                    </div><!--/row-->\
                    </div><!--/twelve-->\
                </div><!--/row-->\
            </div>';

        var compiled_templates = {};
        
        return {
            getCompiledTemplate : function(template_type){
                switch (template_type)  {
                    
                    case "POST_STORY":
                        var post_story_template;
                        if(typeof compiled_templates['POST_STORY'] !== 'undefined') {
                            return compiled_templates['POST_STORY'];
                        } else {
                            post_story_template = Mustache.compile(story_post_view);
                            compiled_templates['POST_STORY'] = post_story_template;
                            return post_story_template;
                        }
                    break;

                    case "POST_VIDEO":
                        var post_video_template;
                        if(typeof compiled_templates['POST_VIDEO'] !== 'undefined') {
                            return compiled_templates['POST_VIDEO'];
                        } else {
                            post_video_template = Mustache.compile(video_post_view);
                            compiled_templates['POST_VIDEO'] = post_video_template;
                            return post_video_template;
                        }
                    break;

                    case "LIKE_STORY": case "DISLIKE_STORY": case "SHARE_STORY": case "READ_STORY": case "COMMENT_STORY":
                        var story_action_template;
                        if(typeof compiled_templates['STORY_ACTION'] !== 'undefined') {
                            return compiled_templates['STORY_ACTION'];
                        } else {
                            story_action_template = Mustache.compile(story_liked_disliked_shared_commented_read_view);
                            compiled_templates['STORY_ACTION'] = story_action_template;
                            return story_action_template;
                        }
                    break;

                    case "LIKE_VIDEO": case "DISLIKE_VIDEO": case "SHARE_VIDEO": case "READ_VIDEO": case "COMMENT_VIDEO":
                        var video_action_template;
                        if(typeof compiled_templates['VIDEO_ACTION'] !== 'undefined') {
                            return compiled_templates['VIDEO_ACTION'];
                        } else {
                            video_action_template = Mustache.compile(video_liked_disliked_shared_commented_read_view);
                            compiled_templates['VIDEO_ACTION'] = video_action_template;
                            return video_action_template;
                        }
                    break;

                    // (Expected but not yet returned by WS template_type's included as well)
                    case "PUBLISH_NEWSPAPER": case "LIKE_NEWSPAPER": case "DISLIKE_NEWSPAPER": case "SHARE_NEWSPAPER": case "READ_NEWSPAPER": case "COMMENT_NEWSPAPER":
                        var newspaper_action_template;
                        if(typeof compiled_templates['NEWSPAPER_ACTION'] !== 'undefined') {
                            return compiled_templates['NEWSPAPER_ACTION'];
                        } else {
                            newspaper_action_template = Mustache.compile(newspaper_published_liked_disliked_shared_commented_read_view);
                            compiled_templates['NEWSPAPER_ACTION'] = newspaper_action_template;
                            return newspaper_action_template;
                        }
                    break;

                }
            }
        };

    }();

    var placeNewsfeed = function(nf_json){

        $.each(nf_json[0].newsfeedList, function(i,item) {
            
            //console.log(i, item.type, item.timeAgo, nf_json[0].newsfeedList.length);

            var compiledTemplate = TN.baseNewsfeed.MustacheTemplates.getCompiledTemplate(item.type);
            
            // Items with empty title seem to be deleted items. 
            // We will not append them to the newsfeed.
            if ( (!TN.utils.isBlank(item.title)) && (item.title != "Message Board") ) {

                var newContent = compiledTemplate({
                    "item_title" : item.title,
                    "item_author" : (item.custName.search("Lastname") == -1) ? item.custName : item.custName.substr(0, item.custName.search("Lastname")-1),
                    "item_author_id" : item.custId,
                    "item_action_publish" : (item.type == "PUBLISH_NEWSPAPER") ? true : false,
                    "item_action_like" : ((item.type == "LIKE_STORY") || (item.type == "LIKE_VIDEO")  || (item.type == "LIKE_NEWSPAPER")) ? true : false,
                    "item_action_dislike" : ((item.type == "DISLIKE_STORY") || (item.type == "DISLIKE_VIDEO")  || (item.type == "DISLIKE_NEWSPAPER")) ? true : false,
                    "item_action_share" : ((item.type == "SHARE_STORY") || (item.type == "SHARE_VIDEO")  || (item.type == "SHARE_NEWSPAPER")) ? true : false,
                    "item_action_comment" : ((item.type == "COMMENT_STORY") || (item.type == "COMMENT_VIDEO")  || (item.type == "COMMENT_NEWSPAPER")) ? true : false,
                    "item_action_read" : ((item.type == "READ_STORY") || (item.type == "READ_VIDEO")  || (item.type == "READ_NEWSPAPER")) ? true : false,
                    "item_custThumb_url" : item.custThumb_url + "&width=68",
                    // We are overriding the provided thumb link since the WS data is incorrect:
                    "story_post_image" : "/salebynow/form.htm?action=loadStoryImage&storyId=" + item.obj_id + "&width=400",
                    "story_post_who_section" : !!item.pwho ? true : false,
                    "story_post_what_section" : !!item.pwhat ? true : false,
                    "story_post_when_section" : !!item.pwhen ? true : false,
                    "story_post_where_section" : !!item.pwhere ? true : false,
                    "story_post_why_section" : !!item.pwhy ? true : false,
                    "story_post_how_section" : !!item.phow ? true : false,
                    "story_post_oped_section" : !!item.oped ? true : false,
                    "story_post_who" : item.pwho,
                    "story_post_what" : item.pwhat,
                    "story_post_when" : item.pwhen,
                    "story_post_where" : item.pwhere,
                    "story_post_why" : item.pwhy,
                    "story_post_how" : item.phow,
                    "story_post_oped" : item.oped,
                    "item_obj_id" : item.obj_id,
                    "obj_url" : item.obj_url,
                    "time_ago" : item.timeAgo,
                    "action_buttons" : ((item.type == "POST_STORY") || (item.type == "POST_VIDEO")) ? true : false
                });
                
                // With Infinity.js:
                // listView.append(newContent);
                // Without Infinity.js:
                $('.feed-container').append(newContent);

                // Clamp vertical story content size to 172px
                var itemsToClamp = document.getElementsByTagName('body')[0].getElementsByClassName('commentBox_ContentStoryText');
                $.each (itemsToClamp, function(i, v){$clamp(v, {clamp: '172px'});});

                // Adjust all video story posted margin-left attributes to align with right border of video box
                $.each($('.innerSpanTimePosted'), function(i,elem){ 
                    var parentEl = $(elem).parent();
                    parentEl.css("margin-left", (660-$(elem).width())+"px");
                });
                
                //skimlinks();
                
            }

            if ( i == (nf_json[0].newsfeedList.length-1) ) okToLoadMore = true;
        });


    }

    $baseNewsfeed.init = function(){

        // if (custId != viewId) TN.services.getMyNewsfeed(feedPage, viewId).done(placeNewsfeed); else
        TN.services.getNewsfeed(feedPage, viewId).done(placeNewsfeed);
        TN.baseMypage.leftColInit.viewedUserInfo.done(function(msg){
            $('#newsfeedTitle').text(TN.viewedUserInfo.firstName + '\'s Newsmap');
        });

        $(window).scroll( function() {

            //console.log ( $(window).scrollTop(), $(document).height(), $(window).height(), $(document).height() - $(window).height() );

            if (( $(window).scrollTop() >= $(document).height() - $(window).height() - 700 ) /*&& imageContainer.is(':visible')*/ && okToLoadMore) { 

                //if( pageNum < maxPages ){
                    okToLoadMore = false;
                    feedPage++;
                    // if (custId != viewId) TN.services.getMyNewsfeed(feedPage, viewId).done(placeNewsfeed); else
                    TN.services.getNewsfeed(feedPage, viewId).done(placeNewsfeed);
                //}
            }
        });

       TN.myPage.dissolveArtMap();
        
    };

}(TN.baseNewsfeed));