if(!TN) var TN = {};
if (!TN.allStories) TN.allStories = {};

(function($allStories){
	
	var imageContainer = null;
	var topStories = {};
	var custId = TN.utils.getCookie('TNUser');
	var likeBeingProcessed = false;
	var dislikeBeingProcessed = false;
	var reclipBeingProcessed = false;
	var catListElem = $('#storyCatsDropDown');

	(function($topStories){
		var pageNum = 0;
		var maxPages = 1;
		var catType;
		var okToLoadMore = false;
		var totalItems = 0;
		var itemCount = 0;
		var imagesHtml = "";
		var currAjaxCallId = null;
		var messageIdArray = [];
		
		function getMessageIdIndex(messageId){
			var arrayLen = messageIdArray.length;
			
			for( var i = 0; i < arrayLen; i++ ){
				if( parseInt(messageIdArray[i]) ===  parseInt(messageId )){
					break;
				}
			}
			
			return i;
		}
		
		function getImageItemHtml(itemSummary, itemDetails, countersObject){
			
			function getStoryCopy(itemDetails){
				var itemDetailsBuzz = itemDetails.wsBuzz;
				var opEdItemHtml = "";
				
				if( !!itemDetailsBuzz ){
					if( !!itemDetailsBuzz.who ){
						opEdItemHtml += 'Who: ' + itemDetailsBuzz.who+'<br/>';						
					}
					if( !!itemDetailsBuzz.what ){
						opEdItemHtml += 'What: ' + itemDetailsBuzz.what+'<br/>';						
					}
					if( !!itemDetailsBuzz.where ){
						opEdItemHtml += 'Where: ' + itemDetailsBuzz.where+'<br/>';						
					}
					if( !!itemDetailsBuzz.when ){
						opEdItemHtml += 'When: ' + itemDetailsBuzz.when+'<br/>';						
					}
					if( !!itemDetailsBuzz.how ){
						opEdItemHtml += 'How: ' + itemDetailsBuzz.how+'<br/>';						
					}
					if( !!itemDetailsBuzz.why ){
						opEdItemHtml += 'Why: ' + itemDetailsBuzz.why+'<br/>';						
					}
					if( !!itemDetailsBuzz.oped ){
						opEdItemHtml += itemDetailsBuzz.oped+'<br/>';						
					}
				}
				
				return ( !!opEdItemHtml ? '<p>' + opEdItemHtml + '</p>' : '');
			}
							
			
			function getOriginUrlHtml(){
				var itemDetailsBuzz = itemDetails.wsBuzz;
				var originUrlHtml = "";
				
				if( !!itemDetailsBuzz ){
					var storyUrl = itemDetailsBuzz.storyUrl;
					if( !!storyUrl ){
						originUrlHtml += ' via <a href="' + storyUrl + '"><div class="storyViaLink">' + TN.utils.getBaseUrl(storyUrl) + '</div></a>';
					}
				}
				
				return originUrlHtml;
			}

			//temp
			var reclipsCount = 0;
			
			var storyImageUrl = (!!itemSummary.senderThumbImageUrl ?  itemSummary.senderThumbImageUrl : itemSummary.thumbImageUrl );

            var storyHtml = '<div class="mypageStoryBox item">\
            	<div class="mypageStoryInfo">\
					<input class="messageId" type="hidden" value="' + itemSummary.messageId + '"/> \
            		<img class="storyImage" src="' + storyImageUrl + '"/>\
            		<div class="story-title">' + itemSummary.headline + '</div>\
            		<hr class="divider">' +
            		getStoryCopy(itemDetails) +
            	'</div><!--/mypageStoryInfo-->\
            	<div class="mypageStoryBoxOpt block">\
            		<!-- Button Bar -->\
            		<div class="button-bar centered">\
            			<ul class="button-group radius">\
            				<li><a href="javascript:void(0);" class="button like-button"><figure class="like-icon"></figure></a></li>\
            				<li><a href="javascript:void(0);" class="button dislike-button"><figure class="dislike-icon"></figure></a></li>\
            			</ul>\
            			<ul class="button-group radius">\
            				<li><a href="javascript:void(0);" class="button reclip-button"><figure class="reclip-icon"></figure></a></li>\
            				<li><a href="javascript:void(0);" class="button share-button"><figure class="share-icon"></figure></a></li>\
            			</ul>\
            		</div>\
            	</div><!--/mypageStoryBoxOpt-->\
            		<div class="mypageStoryBoxUser block">\
						<a onclick="if( TN.header.isGuestUser() ) return false;" href="mypage.html' + (( (!!itemDetails.wsBuzz) && (typeof itemDetails.wsBuzz.custid !== "undefined") && (itemDetails.wsBuzz.custid != TN.header.custIdNum)) ? '?view='+itemDetails.wsBuzz.custid : '') + '"><div style="width: 36px; height: 36px; overflow: hidden;">\
            				<img onload="TN.utils.fwNormalizeImage(this,36,36);" src="' + itemDetails.originatorThumbImageUrl + '" alt="user image"/>\
            			</div></a>\
            			<h3 class="auther-info">by<a onclick="if( !TN.header.isGuestUser() ) location.href=\'mypage.html' + (( (!!itemDetails.wsBuzz) && (typeof itemDetails.wsBuzz.custid !== 'undefined') && (itemDetails.wsBuzz.custid != TN.header.custIdNum)) ? '?view='+itemDetails.wsBuzz.custid : '') + '\';" href="javascript:void(0);">' + itemDetails.originatorName + getOriginUrlHtml() + '</a></h3>\
            			<p class="date-closed"><span>' + ( !!itemDetails.wsMessage ? itemDetails.wsMessage.elapsedTime : '' ) + '</span></p>\
            			<hr class="divider">\
            			<ul class="no-bullet results">' +
            				(!!countersObject ? 
	            				'<li class="likesCount"><span>' + countersObject.likesCounter + '</span> likes</li>\
	            				<li class="dislikesCount"><span>' + countersObject.unLikesCounter + '</span> dislikes</li>\
	            				<li class="commentsCount"><span>' + TN.commentHandler.trueNumOfComments(itemDetails.messageResponseList) + '</span> comments</li>\
	            				<li class="reclipsCount"><span>' + reclipsCount + '</span> clips</li>' : 
	            				'<li>Counters Info Not Available</li>' ) +
                		'</ul>\
            		</div><!--/mypageStoryBoxUser-->' + 
                    TN.commentHandler.getCommentsHtml(itemDetails.messageResponseList) +
                    TN.commentHandler.getCommentBoxHtml() +
            	'</div><!--/mypageStoryBox-->';

		    return storyHtml;
		}
		
		
		function getItemDetails(itemSummary, ourAjaxCallId, callGetFittingRoomRequest ){
			var messageId = itemSummary.messageId;
			
			function getDetails(itemDetails){
				function addItemToPage(countersObject){
					itemCount++;

					TN.utils.storeDetailItem(messageId, itemDetails, countersObject);
					messageIdArray.push(messageId);
					imagesHtml += getImageItemHtml(itemSummary, itemDetails, countersObject);
					
					// Once final item details are loaded, load the DOM
					if( itemCount === totalItems ){
						var imagesHtmlElem = $(imagesHtml);
                        // Test for unreachable or corrupt images and remove stories (article element)
                        // with such images before appending anything to masonryCont since they break
                        // the "lightbox" function:
                        imagesHtmlElem.find('.storyImage').each(function(){
                            var tempImgElem = $(this);
                            tempImgElem.attr("src", this.src).error(function(){
                                TN.utils.deleteDetailItem(tempImgElem.parent().find('.messageId').val());
                                tempImgElem.parent().parent().detach();
                            });
                        });
                        imagesHtmlElem.hide();
											
						imageContainer.append(imagesHtmlElem).imagesLoaded( function() {									
							
							if( ourAjaxCallId === currAjaxCallId ){
								imagesHtmlElem.find('.comments').unbind('click').bind('click', function(){
									var jqElem = $(this);
									TN.commentHandler.getAllComments(jqElem, jqElem.parents('.mypageStoryBox'));
								});
							
								//imagesHtmlElem.find('.mypageStoryBox').each(function(){
								imagesHtmlElem.each(function(){
									var currStoryElem = $(this);
									TN.commentHandler.addCommentBoxFunctions(currStoryElem);
								});
								
								imagesHtmlElem.find('.like-button').unbind('click').click(function(){
					            	if( TN.header.isGuestUser() ) return;
									
									if( !likeBeingProcessed ){
										likeBeingProcessed = true;
										var jqElem = $(this);
										var parentCont = jqElem.parents('.mypageStoryBox');
										
										if( !!parentCont.data('like') ){
											TN.likeHandler.reverseLikeCount( custId, parentCont, catType, jqElem).
												always(function(){
													likeBeingProcessed = false;
												});
										}
										else {
											TN.likeHandler.updateLikeCount( custId, parentCont, catType, jqElem).
												always(function(){
													likeBeingProcessed = false;
												});
										}
									}				
								});
								
								imagesHtmlElem.find('.dislike-button').unbind('click').click(function(){
					            	if( TN.header.isGuestUser() ) return;
					            	
									if( !dislikeBeingProcessed ){
										dislikeBeingProcessed = true;
										var jqElem = $(this);
										var parentCont = jqElem.parents('.mypageStoryBox');
										
										if( !!parentCont.data('dislike') ){
											TN.likeHandler.reverseDislikeCount( custId, parentCont, catType, jqElem).
												always(function(){
													dislikeBeingProcessed = false;
												});
										}
										else {
											TN.likeHandler.updateDislikeCount( custId, parentCont, catType, jqElem).
												always(function(){
													dislikeBeingProcessed = false;
												});
										}
									}									
								});
								
								imagesHtmlElem.find('.reclip-button').unbind('click').click(function(){
					            	if( TN.header.isGuestUser() ) return;
									
									if( !reclipBeingProcessed ){
										reclipBeingProcessed = true;
										
										var jqElem = $(this);
										var parentCont = jqElem.parents('.mypageStoryBox');
										var messageId = parentCont.find('.messageId').val();
										
										TN.services.addToWishListWithFittingRoomData(custId, messageId, 'public').
										done(function(){
											TN.utils.passiveNotification('Favorited!', 'This item has been successfully added to favorites');
										}).
										always(function(){
											reclipBeingProcessed = false;
										});
									}
								});
								
								imagesHtmlElem.find('.share-button').unbind('click').click(function(){
					            	if( TN.header.isGuestUser() ) return;
									
									var jqElem = $(this);
									var parentCont = jqElem.parents('.mypageStoryBox');
									var messageId = parentCont.find('.messageId').val();
									
									var infoStruct = TN.utils.getDetailItem(messageId);
					            	TN.sharingHandler.initStoryShare(infoStruct, function(shareType){
					            		_gaq.push(['_trackEvent', 'Story', 'Share: ' + shareType + ', Message ID: ' + messageId, 'User: ' + custId]);            			
					            	});
								});
								
								imagesHtmlElem.find('.storyImage').unbind('click').click(function(){
									var parentCont = $(this).parents('.mypageStoryBox');
									var messageId = parentCont.find('.messageId').val();
									okToLoadMore = false;
									TN.lightbox.show(custId, messageId, function(newLikesCount, newDislikesCount, newCommentsCount){
										okToLoadMore = true;
										
										if( !!newLikesCount || newLikesCount === 0 ){
											parentCont.find('.likesCount span').html(newLikesCount);
										}
										if( !!newDislikesCount || newDislikesCount === 0 ){
											parentCont.find('.dislikesCount span').html(newDislikesCount);
										}
										if( !!newCommentsCount || newCommentsCount === 0 ){
											parentCont.find('.commentsCount span').html(newCommentsCount);
										}
										
										TN.commentHandler.reloadComments(parentCont);
									}, null, {
										index : getMessageIdIndex(messageId),
										array : messageIdArray
									});
								});
								
								if( pageNum === 0 ){
									imageContainer.masonry({
										itemSelector : '.item',
										isFitWidth: true,
										columnWidth : 0
									});
								} else {
									imageContainer.masonry('appended', imagesHtmlElem);
								}
								
								pageNum++;
								imagesHtmlElem.show();
																
								skimlinks();
								
								okToLoadMore = true;
							}
						});			
					}
				}					
				
				TN.services.getMesageStats(messageId).done(function(countersJson){
					if( !!countersJson && countersJson[0] && ourAjaxCallId === currAjaxCallId ){
						addItemToPage(countersJson[0]);
					}
				}).fail(function(){
					addItemToPage();
				});				
			}
			
			if( callGetFittingRoomRequest ){
				TN.services.getFittingRoomRequest(custId, messageId).
				done(function(json){
					if( !!json && !!json[0] && ourAjaxCallId === currAjaxCallId ){
						getDetails(json[0]);
					}
				});
			} else {
				getDetails(itemSummary);
			}
		}
		
		function loadImageContainer( json, ourAjaxCallId ){
															
			if( json && json[0] ){
				
				maxPages = parseFloat(json[0].noOfPages);
				
				if( json[0].fittingRoomSummaryList  ){

					totalItems = json[0].fittingRoomSummaryList.length;
					for( var i = 0; i<totalItems; i++ ){
						var currItem = json[0].fittingRoomSummaryList[i];
						if(!!currItem){
							//get details of the currentitem
							getItemDetails(currItem, ourAjaxCallId, false );
						}
					}									
				}
			}
		}
		
		function loadImageContainerFromSearch( json, ourAjaxCallId ){
			if( !!json ){
				maxPages = 1;
				
				totalItems = json.length;
				for( var i = 0; i < totalItems; i++ ){
					var currItem = json[i];
					if( !!currItem ){
						//get details of the currentitem
						getItemDetails(currItem, ourAjaxCallId, true);
					}
				}
			}
		}
		
		function loadImageContainerFromTopStories( json, ourAjaxCallId ){
			if( json ){
				maxPages = 1;

				totalItems = json.length;
				for( var i = 0; i<totalItems; i++ ){
					var currItem = json[i];
					if(!!currItem){
						//get details of the currentitem
						getItemDetails(currItem, ourAjaxCallId, true);
					}
				}									
			}
		}
		
		function loadItems(){
			var numItems = 20;
			
			totalItems = 0;
			itemCount = 0;
			imagesHtml = "";
			filterType = TN.utils.getQueryStringParam("filter");
			
			if( !filterType ){
				filterType = 'wooped';
			}
			
			if( filterType === 'wooped' ){
				$('#storiesButton').addClass('active');				
				$('#editorialsButton').removeClass('active');				
			}
			
			if( filterType === 'woped' ){
				$('#storiesButton').removeClass('active');				
				$('#editorialsButton').addClass('active');				
			}
			
			if( pageNum > 0 ){
				if( pageNum === 1 ){
					pageNum = pageNum + 1;
				}
				numItems = 10;
			}
			
			currAjaxCallId = new Date().getTime();
			var searchPhrase = $('#storiesSearch').val();
			if( !!searchPhrase ){
				(function(){
					var ourAjaxCallId = currAjaxCallId;
					okToLoadMore = false;
		            TN.services.searchStories( searchPhrase, 1, 30, filterType ).
            		done(function(data){
            			if( ourAjaxCallId === currAjaxCallId ){
            				loadImageContainerFromSearch(data, ourAjaxCallId);            				
		            		_gaq.push(['_trackEvent', 'Story', 'Search: ' + searchPhrase, 'User: ' + custId]);
            			}
            		});
				}());				
			}
			else {
				if( !!catType ){
					(function(){
						var ourAjaxCallId = currAjaxCallId;
						okToLoadMore = false;
			            TN.services.getAllRequestSummaries( custId, numItems, pageNum + 1, catType, filterType ).
			            done(function(data){
			            	if( ourAjaxCallId === currAjaxCallId ){
				            	loadImageContainer(data, ourAjaxCallId);
			            	}
			            });
					}());
				}
				else {
					(function(){
						var ourAjaxCallId = currAjaxCallId;
						okToLoadMore = false;
						TN.services.getTopStories(custId).
						done(function(data){
							if( ourAjaxCallId === currAjaxCallId ){
								loadImageContainerFromTopStories(data, ourAjaxCallId);								
							}
						});						
					}());
				}			
			}
		};
		
		$topStories.reload = function(newType){
			catType = newType;
			if( imageContainer.html().trim() ){
				imageContainer.masonry('destroy');
			}
			$('#storiesSearch').val('');
			imageContainer.empty();
			pageNum = 0;
			messageIdArray = [];
			loadItems();
		};
		
		$topStories.search = function(searchPhrase){
			if( !!searchPhrase ){
				if( imageContainer.html().trim() ){
					imageContainer.masonry('destroy');
				}
				imageContainer.empty();
				pageNum = 0;
				totalItems = 0;
				itemCount = 0;
				imagesHtml = "";
				catType = "";
                TN.utils.setDropdownValue(catListElem, 'All Categories');
    			messageIdArray = [];
    			loadItems();                
			}
		};
		
		$(window).scroll( function() {
			if (( $(window).scrollTop() >= $(document).height() - $(window).height() - 2500  ) && imageContainer.is(':visible') && okToLoadMore) { 
				if( pageNum < maxPages ){
					loadItems();
				}
			}
		});
		
	}(topStories));
	
	function loadCategories(){
		
        var csCatDropdown = $('#storyCatsDropDown > ul');
        function addCategoryItem(itemText){
        	var categoryItemElem = null;
        	
        	if( itemText === 'All Categories' ){
            	categoryItemElem = $('<li id="allCatsOption">' + itemText + '</li>');           		
        	} else {
            	categoryItemElem = $('<li>' + itemText + '</li>');
        	}            	
        	
        	categoryItemElem.click(function(){
        		var currElem = $(this);
                TN.utils.setDropdownValue(catListElem, currElem.text()); 
                
//                if( currElem.text() === 'Trending News' ){
//   				topStories.reload('Breaking News');                	
//                }
//                else {
                    if( currElem.text() === 'All Categories' ){
        				topStories.reload('');
                    }
                    else {
        				topStories.reload(TN.categories.getValue(currElem.text()));
                    }
                    
	            	if( currElem.text() === 'The Turnip'){
	            		$('#turnipLogo').show();
	            		$('#storiesBackground').addClass('turnipBackground');
	            	}
	            	else {
	            		$('#turnipLogo').hide();				            		
	            		$('#storiesBackground').removeClass('turnipBackground');
	            	}

//                }
        		$('body').click();
        		return false;        	
        	});
        	
        	csCatDropdown.append(categoryItemElem);
        }
                
        addCategoryItem('All Categories');
        TN.utils.setDropdownValue(catListElem, 'All Categories');

		TN.services.getAllMessageTypeActiveCategories().done(function(json){
			if( !!json ){
				var maxItems = json.length;
				for( var i = 0; i < maxItems; i++ ){
					if( json[i].global === 1 || json[i].global === 0 ){
						var currCat = json[i].id;
						var currCatLabel = TN.categories.getLabel(currCat);
//						if( currCat === "Breaking News"){
//	                    	addCategoryItem('Trending News');
//						} 
//						else {
						if( !!currCatLabel ){
	                    	addCategoryItem(currCatLabel);							
						}
//						}
					}
				}
				
                TN.utils.setDropdownValue(catListElem, 'Trending News');            		
				topStories.reload('Breaking News');
			}
		});
	};
	
	$allStories.initialize = function(){
//		TN.Header.initialize();
		TN.services.keepThisSessionAlive();
		imageContainer= $('#container');
		
//		topStories.reload("Breaking News");
		loadCategories();
		
        $('#storiesSearch').keyup(function(event){
            if( event.which === 13 ){        
                var searchTxt = $('#storiesSearch').val();
            	topStories.search(searchTxt);                
            }
        });
        
        $('#storySearchIcon').click(function(){
                var searchTxt = $('#storiesSearch').val();
            	topStories.search(searchTxt);                
        });
	};
	
}(TN.allStories));
