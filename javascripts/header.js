if (!TN) var TN= {};
if (!TN.header) TN.header= {};

(function($header){
    
    custId = TN.utils.getCookie('TNUser');
    TN.header.custIdNum = TN.utils.getCookie('TNUserNum');
    if (TN.utils.getQueryStringParam("view")) viewId = TN.utils.getQueryStringParam("view");
    else viewId = TN.utils.getCookie('TNUser');
    var totalNPages = 0;
    var currNPage = 0;
    var notificationsNum = 0;
    var notificationsLoaded = false;
    var uncheckedNotifs = 0;
    if (typeof TN.homepage === "undefined") TN.homepage = false;
    var attempt = 0;
    var maxAttempts = 3;
    TN.guestUserName = "abc123987zyx@test.com";
    TN.guestPassword = "abc123987zyx";
    var emailRegistrationInProgress = false;

    function loadNotifications(pageNum, loadAll, prependOnly, prependNum) {

        function populateNotificationsHtml( response ){

            function getNotificationItemHtml(notificationItem){
                
                function contentTypeTemplate (messageType) {
                    if ((messageType == "FollowingRequest") || (messageType == "AcceptOrReject") || (messageType == "AcceptedFriendRequest")) {
                    	return 'javascript:TN.header.notifMypage(' + notificationItem.custId + ',' + notificationItem.notificationId + ')';
//                        return 'mypage.html?view=' + notificationItem.custId;
                    }
                    else {
                        if (notificationItem.messageId == "") return 'javascript:TN.utils.passiveNotification(\'Warning!\', \'Missing message ID (a notification for a duplicate like?)\')';
                        else return 'javascript:TN.header.notifLightbox(\'' + notificationItem.custId + '\', ' + notificationItem.messageId + ',' + notificationItem.notificationId + ')';
                    }
                }

//                function friendshipControls (requestOriginator) {
//                    TN.services.loadUserInfo(requestOriginator, custId).done(function(msg){
//                        if (msg[0].isFriend == "Y")
//                            $(".requestOriginator[value='"+requestOriginator+"']").parent().text('Accepted');
//                        else if (msg[0].friendRequestType == "") {
//                            // Commented out because currently both friend request withdrawn and friend request rejected
//                            // states fall into this same case:
//                            // $(".requestOriginator[value='"+requestOriginator+"']").parent().text('Rejected');
//                        }
//                        else
//                            $(".requestOriginator[value='"+requestOriginator+"']").parent().html('\
//                            <input class="requestOriginator" type="hidden" value="' + notificationItem.custId + '" />\
//                            <a href="javascript:void(0);" onclick="TN.header.answerFriendshipNotifs(\'Y\', this);" class="notifsReqAccept">accept</a>\
//                            <a href="javascript:void(0);" onclick="TN.header.answerFriendshipNotifs(\'N\', this);" class="notifsReqReject">reject</a>\
//                            ');
//                    });
//                    return "";
//                }
                
                if( loadAll ){
                    var dataHtml = '\
                        <li>\
                            <a onclick="if( TN.header.isGuestUser() ) return false;" href="' + 'javascript:TN.header.notifMypage(' + notificationItem.custId + ',' + notificationItem.notificationId + ')' + '"><figure class="notifications-profile-img"><img src="' + notificationItem.thumbImageUrl + '" onload="TN.utils.fwNormalizeImage(this, 36, 36);"/></figure></a>\
                            <a onclick="if( TN.header.isGuestUser() ) return false;" class="author left" href="'+ contentTypeTemplate(notificationItem.messageType) +'">'+ notificationItem.notificationMessage + '</a><br/><span class="post-time" style="font-size: 10px">'+ notificationItem.elapsedTime +'</span>\
                        </li>\
                        <li class="divider"></li>';
                    return dataHtml;                    
                }
                else {
                    var dataHtml = '\
                        <li class="notifItem bottom-border">\
                            <a class="author" style="padding-right:0px;padding-left:5px;" onclick="if( TN.header.isGuestUser() ) return false;" href="' + 'javascript:TN.header.notifMypage(' + notificationItem.custId + ',' + notificationItem.notificationId + ')' + '"><figure class="notifications-profile-img"><img src="' + notificationItem.thumbImageUrl + '" onload="TN.utils.fwNormalizeImage(this, 36, 36);"/></figure></a>\
                            <a style="padding-left:0px; padding-right:0px; float:left; width:158px; white-space:normal;font-size:14px; color:#006aa8; font-family:\'optima\', \'Candara\', Helvetica Light, sans-serif; font-weight:bold;" href="'+ contentTypeTemplate(notificationItem.messageType) +'">'+ notificationItem.notificationMessage + '</a><br/><span style="display:block; float:right; padding-right: 18px; font-size:10px; color:#808080; font-family:\'optima\', \'Candara\', Helvetica Light, sans-serif; font-weight:bold;">' + notificationItem.elapsedTime + '</span>\
                        </li>';
                    return dataHtml;
                }               
            }
            
            if( !!response && !!response[0] ){
                if( loadAll ){
                    currNPage = pageNum;
                    totalNPages = parseFloat(response[0].noOfPages);
                    notificationsNum = parseFloat(response[0].totalRecords); 
                    if( !!response[0].notificationTrayList && response[0].notificationTrayList.length > 0 ){
                        var notifsReadCount = response[0].notificationTrayList.length;
                        var notificationsHtml = "";
                        for( var i = 0; i < notifsReadCount; i++ ) {
                        	var currNotif = response[0].notificationTrayList[i];
                        	if( currNotif.messageType == "FollowingRequest" || currNotif.messageType == "AcceptOrReject" || 
                        			currNotif.messageType == "AcceptedFriendRequest" || !!currNotif.messageId ){
                                notificationsHtml += getNotificationItemHtml(currNotif);                        		
                        	}
                        }
                        if( !!notificationsHtml ){
                            $('#allNotificationsList').append(notificationsHtml);                        	
                        }
                    }
                }
                else {
                    if( !prependOnly ){
                        currNPage = pageNum;
                        totalNPages = parseFloat(response[0].noOfPages);
                        notificationsNum = parseFloat(response[0].totalRecords); 
                        if( !!response[0].notificationTrayList && response[0].notificationTrayList.length > 0 ){
                            var notifsReadCount = response[0].notificationTrayList.length;
                        	$('#notificationsList').find('.notifItem,#notifsShowAll').empty().remove();
                            var notificationsHtml = "";
                            for( var i = 0; i < notifsReadCount; i++ ) {
                            	var currNotif = response[0].notificationTrayList[i];
                            	if( currNotif.messageType == "FollowingRequest" || currNotif.messageType == "AcceptOrReject" || 
                            			currNotif.messageType == "AcceptedFriendRequest" || !!currNotif.messageId ){
                                    notificationsHtml += getNotificationItemHtml(currNotif);                        		
                            	}
                            }
                            
                            if( !!notificationsHtml ){
                                $('#notificationsList').append(notificationsHtml);                            	
                            }
                        }
                    }
                    
                    if (prependOnly) {
                        totalNPages = parseFloat(response[0].noOfPages);
                        notificationsNum = parseFloat(response[0].totalRecords); 
                        if( !!response[0].notificationTrayList && response[0].notificationTrayList.length > 0 ){
                            var notifsReadCount = response[0].notificationTrayList.length;
                            var notificationsHtml = "";
                            for( var i = 0; i < notifsReadCount; i++ ) {
                            	var currNotif = response[0].notificationTrayList[i];
                            	if( currNotif.messageType == "FollowingRequest" || currNotif.messageType == "AcceptOrReject" || 
                            			currNotif.messageType == "AcceptedFriendRequest" || !!currNotif.messageId ){
                                    notificationsHtml += getNotificationItemHtml(currNotif);                        		
                            	}
                            }
                            $('#notificationsList').prepend(notificationsHtml);
                            
                            if( !!notificationsHtml ){
                                $('#notificationsList').append(notificationsHtml);                            	
                            }
                        }
                    }
                }
            }
        }
        
        TN.services.getNotificationTrayDetails(custId, 5, pageNum).done(function(json){
            populateNotificationsHtml(json);
            if ( !prependOnly && !loadAll ){
                if (currNPage != totalNPages) {
                    $('#notificationsList').append('<li id="notifsShowAll"><a href="#" class="notifications full-width" onclick="TN.header.loadAllNotifications();"><span class="text-center">See All</span></a></li>');
                }
            }        
            $('#notifAlert').text(notificationsNum);
            
            if( TN.utils.getCookie('TNShowNotif') !== 'false' ){
               	$('#notifAlert').show();            	
            }
            notificationsLoaded = true;
        }).fail(function(){
            $('#noNotifications').show();
        });
    }
    
    function removeNotifAlert(){
        TN.utils.setCookie("TNShowNotif", false, null, 345600);
    	$('#notifAlert').hide();    	
    }
    
    $header.notifMypage = function(custId, notifId){
    	removeNotifAlert();
    	TN.services.updateNotificationTray(notifId).done(function(){
    		loadNotifications(1);    		
        	location.href = "mypage.html?view=" + custId;
    	});
    };
    
    $header.notifLightbox = function(custId, messageId, notifId){
    	removeNotifAlert();
    	TN.lightbox.show(custId, messageId, function(){
        	TN.services.updateNotificationTray(notifId).done(function(){
           		loadNotifications(1);    			
        	});
    	});
    };
    
//    $header.answerFriendshipNotifs = function(answer, sourceElem) {
//        var originator = $(sourceElem).parent().find('.requestOriginator').val();
//        $(".requestOriginator[value='"+originator+"']").parent().find('.notifsReqAccept, .notifsReqReject').unbind();
//        TN.services.answerFriendRequest(originator, answer).done(function(){
//            $(".requestOriginator[value='"+originator+"']").parent().find('.notifsReqAccept, .notifsReqReject').hide();
//            if (answer == "Y") {
//                $(".requestOriginator[value='"+originator+"']").parent().append("Accepted");
//            }
//            else {
//                $(".requestOriginator[value='"+originator+"']").parent().append("Rejected");
//            }
//        }).fail(function(){
//            TN.utils.passiveNotification('Error!', "Error occurred");
//        });
//    };
    
    $header.loadAllNotifications = function(){
        $('#allNotificationsList').empty();
        $('#NotificationsModal').reveal();
        loadNotifications(1, true);
    };
    
    $header.trackNavigation = function(linkElem){
        _gaq.push(['_trackEvent', 'Page Nagivation', $(linkElem).text(), 'User: ' + custId]);
    };
    
    $header.trackNavigationToMyPage = function(){
        _gaq.push(['_trackEvent', 'Page Nagivation', 'My Profile', 'User: ' + custId]);
    };
    
    $header.loadHeader = function(){
        var headerHtml = '<nav class="top-bar contain-to-grid">\
            <ul>\
                <li class="name"><a href="index.html" class="logo">Tiny News - beta</a></li>\
                <li class="toggle-topbar"><a href="#"></a></li>\
            </ul>\
            <section>\
                <ul class="left left-interactive-header-ul">\
                    <li class="has-dropdown">\
                        <a class="noArrow" href="javascript:void(0);"><img class="header-img-icons" src="images/mypage-icon.png" onclick="#"/></a>\
                        <ul class="dropdown" id="myPageHeaderDropdown" style="display:block">\
                            <li><a href="mypage.html">My Profile</a></li>\
                            <li><a onclick="if( TN.header.isGuestUser() ) return false;" href="settings.html">My Settings</a></li>\
                            <li><a href="javascript:TN.header.findPeople();">Find People</a></li>\
                            <li><a href="leaderboard-all.html">Leaderboard</a></li>\
                            <li><a href="javascript:void(0);" id="signOut">Sign out</a></li>\
                        </ul>\
                    </li>\
                    <li class="has-dropdown">\
                        <a id="notifIcons" class="noArrow" href="javascript:void(0);"><img class="header-img-icons" src="images/notifications-icon.png"/><label id="notifAlert" class="tiny round button"></label></a>\
                        <ul class="dropdown" id="notificationsList" style="width:230px;display:block;">\
                            <li id="noNotifications" style="display: none"><a href="#" class="notifications full-width"><span class="text-center">You have currently no notifications.</span></a></li>\
                        </ul>\
                    </li>\
                </ul>\
                <ul class="right">\
                    <li><a href="javascript:void(0);" onclick="if (!!custId) { TN.header.trackNavigation(this); location.href=\'map.html\'; } else $(\'#signInRegisterModal\').reveal();">Local News, Events and Promotions</a></li>\
                    <li><a href="javascript:void(0);" onclick="if (!!custId) { TN.header.trackNavigation(this); location.href=\'stories.html\'; } else $(\'#signInRegisterModal\').reveal();">The News Stand</a></li>\
                    <li class="has-dropdown">\
                        <a href="javascript:void(0);">The Printing Press</a>\
                        <ul class="dropdown" id="postStoryHeaderDropdown" style="display:none">\
                            <li><a href="javascript:void(0);" id="addStory">Create a story from my computer</a></li>\
                            <li><a id="addClipThis" href="javascript:void(0);">Create a story from a website</a></li>\
                            <li><a href="javascript:void(0);" onclick="if (!!custId) { TN.header.trackNavigation(this); location.href=\'newspaperBegin.html\'; } else $(\'#signInRegisterModal\').reveal();">Create a Newspaper</a></li>\
                        </ul>\
                    </li>\
                </ul>\
            </section>\
            </nav>';
        
        $('header').append(headerHtml);
        $('#notifIcons').mouseover(function(){
        	removeNotifAlert();
        });
    };
    
    TN.header.loadHeader();

    function loadFooter(){
        var footerHtml = '<div class="twelve columns">\
            <div class="row">\
                <div class="twelve columns">\
                    <ul class="link-list left">\
                        <!--li><a href="contact.html" ><img src="images/email.png"  alt="mail" title="Contact Tiny News" class="has-tip tip-top" data-width="80"></a></li-->\
                        <li><a href="http://www.facebook.com/pages/Tiny-News/326376470776048"><img src="images/facebook.png"  alt="facebook" title="Friend Us On Facebook" class="has-tip tip-top" data-width="90"></a></li>\
                        <li><a href="javascript:TN.utils.passiveNotification(\'Please wait!\', \'We are getting there!\');"><img src="images/google.png"  alt="gmail" title="Join Us On Google Plus" class="has-tip tip-top" data-width="90"></a></li>\
                        <li ><a href="https://twitter.com/TinyNewser"><img src="images/twitter.png" alt="twitter" title="Follow Us on Twitter" class="has-tip tip-top" data-width="70"></a></li>\
        				<li><p>&copy; 2013 Tiny News</p></li>\
                    </ul>\
        			<ul class="link-list right">\
        				<!--li><a href="javascript:void(0);" style="color: lightgray;">About Us</a></li-->\
        				<li><a href="contact.html" style="color: lightgray;">Contact Us</a></li>\
						<!--li><a href="javascript:void(0);" style="color: lightgray;">Terms of Service</a></li-->\
						<!--li><a href="javascript:void(0);" style="color: lightgray;">Privacy Policy</a></li-->\
        			</ul>\
                </div>\
            </div>\
        </div>';
        
        $('footer').empty().append(footerHtml);
    }
    
    function loadNotificationsModal(){
        var notifModalHtml = '<div id="NotificationsModal" class="reveal-modal notificationsSrollbar">\
                                <h2>Notifications</h2>\
                                <div class="row">\
                                    <div class="twelve columns">\
                                        <ul id="allNotificationsList" class="side-nav"></ul>\
                                    </div>\
                                </div>\
                                <a class="close-reveal-modal"></a>\
        						<div class="row centered">\
        							<a id="loadMoreNotifs" href="javascript:void(0);">more</a>\
        						</div>\
                            </div>';
        $('body').append(notifModalHtml); 
        
        $('#loadMoreNotifs').click(function(){
        	if( currNPage < totalNPages ){
        		loadNotifications(currNPage+1, true );
        	}
        });
    }

    function loadSignInReveal(){
        var revealHtml = '<div id="signInRegisterModal" class="reveal-modal large">\
            <h2 class="Signin">Sign in</h2>\
                    <div class="row">\
                        <div class="seven columns">\
                        	<div class="row">\
                        		<div class="eight columns top-margin">\
                            <form>\
                                <div class="row">\
                                    <div class="ten columns centered">\
                                        <label><b>Username</b></label>\
                                        <input type="text" placeholder="i.e. - jane@wrightstuff.com" name="username" id="username_input" />\
                                        <small style="display: none;" class="error" id="usernameTicker"></small>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="ten columns centered">\
                                        <label><b>Password</b></label>\
                                        <input type="password" name="password" id="password_input" />\
                                        <small style="display: none;" class="error" id="passwordTicker"></small>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="five columns offset-by-seven">\
                                        <a class="radius button" href="javascript:void(0);" id="signInButton">Sign In</a>\
                                    </div>\
                                </div>\
                            </form>\
                            	</div>\
                            <div class="four columns">\
                            	<div class="verticalLine" style="width: 100%; height: 72px;"></div>\
                            	<span class="OR-text" width="100%">OR</span>\
                            	<div class="verticalLine" style="width: 100%; height: 72px;"></div>\
                            </div>\
                            </div>\
                        </div>\
                        <div class="five columns">\
                            <div class="row">\
            					<div class="ten columns centered" style="padding-top: 30px">\
              						<a href="#" id="signin_fbRegister"><img src="images/signInRegister/facebook-signin.png" alt="fb"></a>\
              						<span id="socialRegisterTicker"></span>\
              						<!--\
              						<a href="#" id="signin_twRegister"><img src="images/signInRegister/twitter-signin.png" alt="tw"></a>\
              						<span id="socialRegisterTicker"></span>\
              						-->\
            					</div>\
                            </div>\
                        </div>\
                    </div>\
            <hr style="margin: 0 0 5px;">\
            <a class="" href="javascript:void(0);" onclick="$(\'#registerModal\').reveal();" style="padding-left: 194px;">Sign Up</a>\
                <a class="close-reveal-modal"></a>\
            </div>';
        $('body').append(revealHtml);
    }
    
    function loadRegistrationReveals(){
        var revealHtmlOld = '<div id="registerModal" class="reveal-modal expand">\
          <div class="row">\
            <div class="twelve columns">\
              <div class="row">\
                <div class="five columns">\
                  <div class="row">\
                    <div class="eight columns top-margin">\
                      <figure class="facepile"><img src="images/facepile.png" /></figure>\
                      <a href="#" id="fbRegister" class="primary button expand"><figure class="facebook-mark"></figure>Register via Facebook</a>\
                    </div>\
                    <div class="four columns"><span class="OR-text">OR</span></div>\
                  </div>\
                </div>\
                <div class="seven columns">\
                  <div class="row">\
                    <div class="eight columns">\
                      <div class="row">\
                        <div class="nine columns top-margin">\
                          <label><b>Name</b></label>\
                          <input type="text" placeholder="i.e. - Joey or JRock, you decide" name="username" id="username_input" />\
                          <label><b>Email</b></label>\
                          <input type="text" placeholder="i.e. - jane@wrightstuff.com" name="email" id="email_input" />\
                              <small style="display: none;" class="error" id="registerEmailTicker"></small>\
                          <label><b>Password</b></label>\
                          <input type="password" name="password" id="password_input"/>\
                              <small style="display: none;" class="error" id="registerTicker"></small>\
                          <div class="row">\
                            <div class="eight columns">\
                            </div>\
                            <div class="four columns">\
                              <input type="submit" name="Register" value="Submit" class="button right" id="submitRegisterFormButton" />\
                            </div>\
                          </div>\
                        </div>\
                        <div class="three columns">\
                          <span class="OR-text">&amp;</span>\
                        </div>\
                      </div>\
                    </div>\
                    <div class="four columns">\
                      <h4>Upload Photo</h4>\
                      <form action="/php/store_image.php" method="post" enctype="multipart/form-data" id="photoForm">\
                        <label for="file" id="uploadProfileImgArea">\
                          <figure class="add-photo"><img id="userPhoto" src="images/no-photo.png" /></figure>\
                          <a href="javascript:;" class="radius button">Add Profile Picture</a>\
                        </label>\
                        <input type="file" name="file" id="file" />\
                      </form>\
                    </div>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <a class="close-reveal-modal"></a>\
        </div>';
        
        var registerModalSocial = '<div id="registerModalSocial" class="reveal-modal" style="width:462px; border-left: 0 none;">\
          <div class="row">\
            <div class="ten columns centered" style="padding-top: 30px">\
              <a href="#" id="fbRegister"><img src="images/signInRegister/fb.png" alt="fb"></a>\
              <a href="#" style="display:none"><img src="images/signInRegister/tw.png" alt="tw"></a>\
              <span id="socialRegisterTicker"></span>\
            </div>\
          </div>\
          <div class="row" style="padding-top: 15px">\
            <p style="text-align:center; font-size:15px">or register with your \
            <a href="javascript:void(0);" onclick="$(\'#registerModalEmail\').reveal({\'opened\':function(){\
                    $(\'#username_input_reg, #email_input_reg, #password_input_reg\').placeholder();\
                 }});">email address</a></p>\
          </div>\
          <div class="row">\
            <div class="centered" style="padding-left: 3px">\
              <img src="images/signInRegister/pic-3.png" alt="user-pic">\
              <img src="images/signInRegister/pic-5.png" alt="user-pic">\
              <img src="images/signInRegister/pic-6.png" alt="user-pic">\
              <img src="images/signInRegister/pic-7.png" alt="user-pic">\
              <img src="images/signInRegister/pic-8.png" alt="user-pic">\
              <img src="images/signInRegister/pic-1.png" alt="user-pic">\
              <img src="images/signInRegister/pic-2.png" alt="user-pic">\
              <img src="images/signInRegister/pic-4.png" alt="user-pic">\
              <img src="images/signInRegister/pic-9.png" alt="user-pic">\
              <img src="images/signInRegister/pic-15.png" alt="user-pic" style="padding-left: 2px; padding-right: 1px">\
              <img src="images/signInRegister/pic-16.png" alt="user-pic">\
              <img src="images/signInRegister/pic-10.png" alt="user-pic">\
              <img src="images/signInRegister/pic-11.png" alt="user-pic">\
              <img src="images/signInRegister/pic-12.png" alt="user-pic">\
              <img src="images/signInRegister/pic-13.png" alt="user-pic" style="padding-right:1px">\
              <img src="images/signInRegister/pic-14.png" alt="user-pic">\
            </div>\
          </div>\
          <a class="close-reveal-modal"></a>\
        </div>';

        var registerModalEmail = '<div id="registerModalEmail" class="reveal-modal" style="width:462px; border-left: 0 none; padding: 15px">\
          <div class="row">\
            <div class="signup centered">\
              <div class="row" style="margin-bottom: 0px">\
                <div class="signup-values">\
                  <input type="text" placeholder="Name" name="email_username" id="email_username_input_reg">\
                    <small style="display: none;" class="error" id="emailRegisterNameTicker"></small>\
                  <input type="text" placeholder="Email Address" name="email_email" id="email_email_input_reg">\
                    <small style="display: none;" class="error" id="emailRegisterEmailTicker"></small>\
                  <input type="password" placeholder="Password" name="email_password" id="email_password_input_reg">\
                    <small style="display: none;" class="error" id="emailRegisterPasswordTicker"></small>\
                </div>\
              </div>\
              <div class="row">\
                <form action="/php/store_image.php" method="post" enctype="multipart/form-data" id="photoForm">\
                  <label for="file" id="uploadProfileImgArea">\
                    <div class="upload-pic">\
                      <figure class="add-photo-new"><img src="images/signInRegister/upload.png" alt="upload-picture" id="emailUserPhoto"></figure>\
                      <h1 id="registerModalUploadPhotoText">Upload Photo</h1>\
                    </div>\
                  </label>\
                  <input type="file" name="file" id="file" />\
                </form>\
              </div>\
              <div class="row" style="margin-bottom: 0px">\
                <div class="signup-btn twelve columns">\
                  <p>or <strong>add it later</strong></p>\
                  <div class="twelve columns primary button radius" id="submitEmailRegisterFormButton">\
                    <a href="javascript:void(0);">Sign Up</a>\
                  </div>\
                </div>\
              </div>\
            </div>\
          </div>\
          <a class="close-reveal-modal"></a>\
        </div>';
        
        var revealRegister = '<div id="registerModal" class="reveal-modal large">\
            <h2 class="Signin">Sign up</h2>\
                    <div class="row">\
                        <div class="seven columns">\
                        	<div class="row">\
                        		<div class="eight columns top-margin">\
                            <form>\
                                <div class="row">\
                                    <div class="ten columns centered">\
                                        <label><b>Name</b></label>\
                  							<input type="text" placeholder="Name" name="username" id="username_input_reg">\
                    						<small style="display: none;" class="error" id="registerNameTicker"></small>\
                    				</div>\
                                </div>\
                                <div class="row">\
                                    <div class="ten columns centered">\
                                        <label><b>Username</b></label>\
                  							<input type="text" placeholder="Email Address" name="email" id="email_input_reg">\
                    						<small style="display: none;" class="error" id="registerEmailTicker"></small>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="ten columns centered">\
                                        <label><b>Password</b></label>\
                  							<input type="password" placeholder="Password" name="password" id="password_input_reg">\
                    						<small style="display: none;" class="error" id="registerPasswordTicker"></small>\
                                    </div>\
                                </div>\
                                <div class="row">\
                                    <div class="five columns offset-by-seven">\
                                        <a class="radius button" href="javascript:void(0);" id="submitRegisterFormButton">Sign Up</a>\
                                    </div>\
                                </div>\
                            </form>\
                            	</div>\
                            <div class="four columns">\
                            	<div class="verticalLine" style="width: 100%; height: 93px;"></div>\
                            	<span class="OR-text" width="100%">OR</span>\
                            	<div class="verticalLine" style="width: 100%; height: 93px;"></div>\
                            </div>\
                            </div>\
                        </div>\
                        <div class="five columns">\
                            <div class="row">\
            					<div class="ten columns centered" style="padding-top: 30px">\
              						<a href="#" id="signup_fbRegister"><img src="images/signInRegister/fb.png" alt="fb"></a>\
              						<span id="socialRegisterTicker"></span>\
              						<!--\
              						<a href="#" id="signup_twRegister"><img src="images/signInRegister/tw.png" alt="tw"></a>\
              						<span id="socialRegisterTicker"></span>\
              						-->\
            					</div>\
          					</div>\
                        </div>\
                    </div>\
            <hr style="margin: 0 0 5px;">\
            <a class="" href="javascript:void(0);" onclick="$(\'#signInRegisterModal\').reveal();" style="padding-left: 194px;">Sign In</a>\
                <a class="close-reveal-modal"></a>\
            </div>';

        $('body').append(revealRegister);

    };

    $header.findPeople = function() {

        var searchString;

        function fillResultTemplate(obj, isFriend){

            return '<li>\
              <div class="row" style="padding-left: 30px; padding-right: 50px;">\
                <div class="five left">\
                    <div class="modulNews">\
                        <a href="mypage.html?view=' + obj.userId + '">\
                        <div class="modulNewsImage left"> <img style="width:50px; height:50px" alt="user image" src="' + obj.profileThumbPicUrl + '"> </div>\
                        <h2 class="text-left">' + obj.firstName + ' ' + obj.lastName + '</h2></a>\
                        <p class="text-left">  </p>\
                    </div>\
                </div>\
                <div class="seven right">\
                    <div class="modulNews right">\
                        <input name="userId" type="hidden" value="' + obj.userId + '">\
                        ' + (isFriend ? '' : '<a href="javascript:void(0);" class="searchResult_AddBtn button rounded" \
                            style="position:relative; top: 8px">Add Friend</a>') +'\
                    </div>\
                </div>\
              </div>\
            </li>';
        }

        function injectResult(searchResult) {
            jQuery.each(searchResult[0].nonFriends, function(index, val){
                $('.searchResult_list').append(fillResultTemplate(val, false));
            });
            jQuery.each(searchResult[0].friends, function(index, val){
                $('.searchResult_list').append(fillResultTemplate(val, true));
            });
            $('.searchResult_AddBtn').click(function(event){
                if( TN.header.isGuestUser() ) return;
                $(event.target).unbind();
                var emailid = $(event.target).parent().find('[name="emailid"]').val();
                TN.services.addShopBeeFriends(emailid).done(function(msg){
                    $(event.target).parent().text('Request Pending');
                });
            });
        }

        function initiateSearch() {
            searchString = $('#findPeopleSearchInput').val();
            if (TN.utils.isBlank(searchString)) return;
            $('.searchResult_list').html('');
            TN.services.shopbeeUsersSearch(searchString, 1, 5).done(function(json){
                injectResult(json);
                if ( (json[0].friends.length + json[0].nonFriends.length) == 5 ) {
                    $('.searchResult_list').append('<li style="text-align:center; height: 30px"><a href="javascript:void(0);" id="searchMoreFriends">More</a></li>');
                    $('#searchMoreFriends').click(function(){
                        initiateSearchMore();
                    });
                };
            }).fail(function(){
                $('.searchResult_list').html("<li>No match found</li>");
            });
        }

        function initiateSearchMore() {
            $('#searchMoreFriends').unbind().text('...');
            TN.services.shopbeeUsersSearch(searchString, 6, 1000).done(function(json){
                $('#searchMoreFriends').parent().remove();
                injectResult(json);
            });
        }

        var findPeopleReveal = $('<div class="row reveal-modal" id="findPeopleReveal">\
            <div class="add-friend centered">\
                <h4 class="af-heading">Add Friends</h4>\
                <ul>\
                    <li><a href="#"><img src="images/addFriendNew/tinynews.png" alt="tinynews-button"></a></li>\
                    <!--li><a href="#"><img src="images/addFriendNew/facebook.png" alt="facebook-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/twitter.png" alt="twitter-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/googleplus.png" alt="googleplus-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/pinterest.png" alt="pintrest-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/instagram.png" alt="instagram-button"></a></li>\
                    <li><a href="#"><img src="images/addFriendNew/linkedln.png" alt="linkedln-button"></a></li-->\
                </ul>\
                <div class="row">\
                    <div class="twelve columns ff-with-text">\
                        <h4 class="af-subheading">Begin by searching Tiny News for your friends.</h4>\
                        <input type="text" class="af-input" id="findPeopleSearchInput" placeholder="Enter your friends name here">\
                    </div>\
                </div>\
                    <div class="primary button radius" id="findPeopleSearchButton">\
                        <a href="#">Find Friends</a>\
                    </div>\
                </div>\
                <div class="">\
                    <ul class="searchResult_list no-bullet ModelList">\
                    </ul>\
                </div>\
            <a class="close-reveal-modal"></a>\
        </div>');

        findPeopleReveal.find('#findPeopleSearchInput').keyup(function(event){
            if( event.which === 13 ) {
                initiateSearch();
            }
        });

        findPeopleReveal.find('#findPeopleSearchButton').click(function(){
            initiateSearch();
        });

        $('body').append(findPeopleReveal);
        findPeopleReveal.reveal({"closed":function(){
            findPeopleReveal.remove();
            }
        });

    };

    /*function isJQueryLoaded(){
        if (typeof $ === "undefined") return false;
        else return true;
    };*/

    function isFoundationLoaded(){
        if (!!$ && typeof $.foundation === "undefined") return false;
        else return true;
    };

    function loginAsGuest(lightboxId, npId){
    	
        TN.services.loginCustomer('', TN.guestUserName, TN.guestPassword).done(function(msg){
            //console.log(typeof msg[0]);
            if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
                TN.utils.setCookie("TNUser", TN.guestUserName, null, 345600);
                if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                // window.location.href='./' + location.search; //
                var gaResp = _gaq.push(['_trackEvent', 'Join', 'Guest: Sign In', 'User: ' + TN.guestUserName]);
                
                if( !!lightboxId ){
                    location.href="index.html?lightboxId=" + lightboxId;                	
                }
                else {
                    if( !!npId ){
                        location.href="index.html?npId=" + npId;                	
                    }
                    else {
                        location.href="index.html";                    	
                    }
                }             
            }
            else TN.utils.passiveNotification('Error!', "Error occured during website initialization.");
        }).fail(function(msg){
        	TN.utils.passiveNotification('Error!', "Error occured during Guest login");
        });
    }
    
    $header.isGuestUser = function(){
    	custId = TN.utils.getCookie('TNUser');;
    	if( custId === TN.guestUserName ){
        	var message = "You are logged in as a Guest user.  To use this function please Sign in or Register.<br /><br />To continue as a Guest, click Continue.<br />";
        	//if( confirm(message) ){
        	//	location.href = "index.html?signIn=true";
        	//}
        	TN.utils.foundationConfirmRevealTwo(message, 'Sign in', 'Register', 'Continue', 'signInRegisterModal', 'registerModal');
       		return true;
    	}
    	else {
    		return false;
    	}
    };
        
    $header.initialize = function(){

        attempt++;
        //console.log(isFoundationLoaded(), isJQueryLoaded(), attempt);
        //TN.utils.passiveNotification('Info:', 'check1');

        if (TN.newspaperBegin || isFoundationLoaded()) {
            
            custId = TN.utils.getCookie('TNUser');
            
            //TN.utils.passiveNotification('Info:', 'check2');
            
            /* <header> is loaded before DOM ready, otherwise Foundation's "toggle-topbar" functionality breaks - Goran */
            
            loadSignInReveal();

            loadRegistrationReveals();

            loadFooter();
            
            var fbRootElem = $('<div id="fb-root"></div>');
            $('body').append(fbRootElem);
            
            loadNotificationsModal();
            
            var TNServerAddress = null;        
            
            $header.servAddressFetch = $.ajax({ type:'GET', dataType:'text', url:'/php/server_address.php' });
            $header.servAddressFetch.done(function( msg ) {
                TNServerAddress = msg;
            });

            var imageSrc = unescape(TN.utils.getQueryStringParam('imageSrc'));
            var storyUrl = unescape(TN.utils.getQueryStringParam('storyUrl'));
            var signIn = unescape(TN.utils.getQueryStringParam('signIn'));
            var lightboxId = unescape(TN.utils.getQueryStringParam('lightboxId'));
            var npId = unescape(TN.utils.getQueryStringParam('npId'));;
            
            var imageUploaded = false;

            $('#addStory').click(function(){ 
            	if( TN.header.isGuestUser() ) return;
            	
            	TN.createStory.show(); 
            });
            
            $('#signOut').click(function(){
                TN.services.logout().always(function(){
                    document.cookie = 'TNUser=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    document.cookie = 'TNUserNum=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    document.cookie = 'TNUserUrl=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    document.cookie = 'TNUserName=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    document.cookie = 'TNShowNotif=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                    TN.userInfo = {};
                    _gaq.push(['_trackEvent', 'Join', 'Sign Out', 'User: ' + custId]);
                    custId = null;
                    TN.FB.logout();
                    window.location.href = 'index.html';
                });
            });

            // This handles both sign-in and sign-out actions:
            $('#signInOutLink').click(function(){
               if (!!custId && custId !== TN.guestUserName ) {
                    // $('#signInOutLink').text('Signing out..');
                    // If signed in, call signout WS and irrespective of result empty session related data, register event to GA and reload page
                    TN.services.logout().always(function(){
                        document.cookie = 'TNUser=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        document.cookie = 'TNUserNum=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        document.cookie = 'TNUserUrl=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        document.cookie = 'TNUserName=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        document.cookie = 'TNShowNotif=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                        TN.userInfo = {};
                        custId = null;
                        _gaq.push(['_trackEvent', 'Join', 'Sign Out', 'User: ' + custId]);
                        TN.FB.logout();
                        location.href="index.html";
                    });
                }
                // Else, show Sign-in/registration Reveal
                else $("#signInRegisterModal").reveal();
            });

            $('#username_input, #password_input').keyup(function(event){
                if( event.which === 13 ) {
                    $('#signInButton').click();
                }
            });
            
            $.getScript("javascripts/fbFunc.js").done(function(){
			    if( TN.FB.isFacebookUser() ) {
			    	$('.settingsCredentials').hide();
			    }
			    else {
			    	$('.settingsCredentials').show();    	
			    }                	
            });
            
            $('#landing_fbRegister').click(function(){
                TN.FB.registerOrLogin();
            });
            
            $('#signin_fbRegister').click(function(){
                TN.FB.registerOrLogin();
            });
            
            $('#signup_fbRegister').click(function(){
                TN.FB.registerOrLogin();
            });
            
            $('#signInButton').click(function(){
                var $form = $(this).parent().parent().parent();
                var $email_input = $form.find('input[name="username"]');
                var $pass_input = $form.find('input[name="password"]');
                $('#usernameTicker, #passwordTicker').hide();
                if (!$email_input.val()) {
                    $pass_input.removeClass('error');
                    $email_input.addClass('error');
                    $('#usernameTicker').text('All Fields Are Required').show();
                }
                else if (!$pass_input.val()) {
                    $email_input.removeClass('error');
                    $pass_input.addClass('error');
                    $('#passwordTicker').text('All Fields Are Required').show();
                }
                else {
                    $email_input.removeClass('error'); 
                    $pass_input.removeClass('error');
                    $.ajaxSetup({ cache: false, dataType: 'json' });
                    TN.services.loginCustomer('', $email_input.val(), $pass_input.val()).done(function(msg){
                        //console.log(typeof msg[0]);
                        if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
                            TN.utils.setCookie("TNUser", $email_input.val(), null, 345600);
                            if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                            // window.location.href='./' + location.search; //
                            $('#signOut').show();
                            var gaResp = _gaq.push(['_trackEvent', 'Join', 'Sign In', 'User: ' + $email_input.val()]);
                            location.href="mypage.html";
                        }
                        else TN.utils.passiveNotification('Error!', "Error occured");
                    }).fail(function(msg){
                        jQuery.each( msg, function(key, value){ if (key == "status") loginfail = value; });
                        if (loginfail == 401) $('#usernameTicker').text('Wrong credentials').show();
                        else if (loginfail == 500) $('#passwordTicker').text('Wrong credentials').show();
                        else if (loginfail == 404) {
                            $('#usernameTicker').text('').hide();
                            $('#passwordTicker').text('Wrong Username/ Password').show();
                        }
                        else TN.utils.passiveNotification('Error!', "Error occured");
                    });
                }
            });
            
            $('#landingSignInButton').click(function(){
                var $form = $(this).parent().parent().parent();
                var $email_input = $form.find('input[name="landing_username"]');
                var $pass_input = $form.find('input[name="landing_password"]');
                $('#landingUsernameTicker, #landingPasswordTicker').hide();
                if (!$email_input.val()) {
                    $pass_input.removeClass('error');
                    $email_input.addClass('error');
                    $('#landingUsernameTicker').text('All Fields Are Required').show();
                }
                else if (!$pass_input.val()) {
                    $email_input.removeClass('error');
                    $pass_input.addClass('error');
                    $('#landingPasswordTicker').text('All Fields Are Required').show();
                }
                else {
                    $email_input.removeClass('error'); 
                    $pass_input.removeClass('error');
                    $.ajaxSetup({ cache: false, dataType: 'json' });
                    TN.services.loginCustomer('', $email_input.val(), $pass_input.val()).done(function(msg){
                        //console.log(typeof msg[0]);
                        if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
                            TN.utils.setCookie("TNUser", $email_input.val(), null, 345600);
                            if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                            // window.location.href='./' + location.search; //
                            $('#signOut').show();
                            var gaResp = _gaq.push(['_trackEvent', 'Join', 'Sign In', 'User: ' + $email_input.val()]);
                            location.href="mypage.html";
                        }
                        else TN.utils.passiveNotification('Error!', "Error occured");
                    }).fail(function(msg){
                        jQuery.each( msg, function(key, value){ if (key == "status") loginfail = value; });
                        if (loginfail == 401) $('#landingUsernameTicker').text('Wrong credentials').show();
                        else if (loginfail == 500) $('#landingPasswordTicker').text('Wrong credentials').show();
                        else if (loginfail == 404) {
                            $('#landingUsernameTicker').text('').hide();
                            $('#landingPasswordTicker').text('Wrong Username/ Password').show();
                        }
                        else TN.utils.passiveNotification('Error!', "Error occured");
                    });
                }
            });

            // Unused/old "registration with email" Reveal related
            if( $.browser.mozilla && (parseInt($.browser.version, 10) < 22) ){
              $('#uploadProfileImgArea').click(function(){
                  $('#file').click();
              });
            };
            
            // callback for ajaxSubmit:
            function showImageUrlResponse(responseText, statusText, xhr, $form) {
                var uploadedPhoto = new Image();
                
                uploadedPhoto.onload = function(){
                    imageUploaded = true;
                    
                    $('#userPhoto').on('load', function(){
                        $('#userPhoto').css('padding-top', '0px');
                        $('#registerModalUploadPhotoText').css('visibility', 'hidden');
                        var resultingHeight = $('#userPhoto').height();
                        if ( resultingHeight < 462 ) {
                            $('.upload-pic').css('height', resultingHeight + 'px');
                        }
                        else $('.upload-pic').css('height', '462px');
                    });
                    
                    $('#userPhoto').attr('src', responseText);
                };

                uploadedPhoto.src = responseText;
            };
            
            // Store image and preview by copying given url to registration's image src attribute:
            var regPhotoSubmitOptions = {
                success: showImageUrlResponse,
                dataType:'text'
            };

            $("#file").change(function() {
                $('#photoForm').ajaxSubmit(regPhotoSubmitOptions);
                //$('#registerModalUploadPhotoText').text('Uploading..');
            });

            // File input must be hidden in this way not to break any browser's file upload functionality
            $("#file").css('position', 'absolute').css('left', '-9999em');
            
            $('#email_input_reg, #username_input_reg, #password_input_reg').keyup(function(event){
                if (event.which == 13) {
                    $("#submitRegisterFormButton").click();
                }
            });
            
            $("#submitEmailRegisterFormButton").click(function() {
                if (emailRegistrationInProgress) return;
                emailRegistrationInProgress = true;
                var registerModal = $('#registerModalEmail'),
                    name_val = registerModal.find('input[name="email_username"]').val(),
                    email_val = registerModal.find('input[name="email_email"]').val(),
                    pass_val = registerModal.find('input[name="email_password"]').val(),
                    photo_url,
                    random_avatar = 'http://' + TNServerAddress + '/images/random_avatars/' + Math.floor((Math.random()*7)+1) + '.jpg',
                    regfail;
                $('#emailRegisterNameTicker, #emailRegisterEmailTicker, #emailRegisterPasswordTicker').hide();
                
                if ((typeof imageUploaded) !== "undefined") {
                    if (imageUploaded === true) photo_url = $('#emailUserPhoto').attr("src");
                    else photo_url = random_avatar;
                }
                else photo_url = random_avatar;
                if (!name_val) {
                    $('#emailRegisterNameTicker').text('All Fields Are Required').show();
                    $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                    emailRegistrationInProgress = false;
                } else if (!email_val) {
                    $('#emailRegisterEmailTicker').text('All Fields Are Required').show();
                    $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                    emailRegistrationInProgress = false;
                } else if (!pass_val) {
                    $('#emailRegisterPasswordTicker').text('All Fields Are Required').show();
                    $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                    emailRegistrationInProgress = false;
                }
                else {
                    TN.services.registerCustomer(email_val, pass_val, name_val, "", photo_url).done(function(msg){
                        if ((msg[0] == true) || (typeof msg[0] === "number")) {
                            TN.utils.setCookie("TNUser", email_val, null, 345600);
                            TN.services.loginCustomer('', email_val, pass_val).done(function(msg){
                                if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                                TN.services.sendBuzzRequest(email_val, "Message Board", "Q", photo_url).done(function(){
                                    _gaq.push(['_trackEvent', 'Join', 'Register', 'User: ' + email_val]);
                                    location.href="mypage.html";
                                });
                            });
                        }
                        else {
                            TN.utils.passiveNotification('Error!', "Error occured during the registration process.");
                            emailRegistrationInProgress = false;
                        }
                    }).fail(function(msg){
                        if ((msg == null) || (msg === null)) TN.utils.passiveNotification('Error!', "Error occured during the registration process.");
                        else {
                            jQuery.each( msg, function(key, value){ if (key == "status") regfail = value; });
                            if (regfail == 409) {
                                $('#emailRegisterEmailTicker').text('Provided e-mail is already registered. Please try a different e-mail address.').show();
                                $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                            }
                            else if (regfail == 500) {
                                $('#emailRegisterNameTicker').text('All Fields Are Required').show();
                                $('#emailRegisterEmailTicker').text('All Fields Are Required').show();
                                $('#emailRegisterPasswordTicker').text('All Fields Are Required').show();
                                $("html, body").animate({ scrollTop: $('#registerModalEmail').position().top }, 400);
                            }
                            else TN.utils.passiveNotification('Error!', "Error occured");
                        };
                        emailRegistrationInProgress = false;
                    });
                }
            });

            $("#submitRegisterFormButton").click(function() {
                if (emailRegistrationInProgress) return;
                emailRegistrationInProgress = true;
                var registerModal = $('#registerModal'),
                    name_val = registerModal.find('input[name="username"]').val(),
                    email_val = registerModal.find('input[name="email"]').val(),
                    pass_val = registerModal.find('input[name="password"]').val(),
                    photo_url,
                    random_avatar = 'http://' + TNServerAddress + '/images/random_avatars/' + Math.floor((Math.random()*7)+1) + '.jpg',
                    regfail;
                $('#registerNameTicker, #registerEmailTicker, #registerPasswordTicker').hide();
                
                if ((typeof imageUploaded) !== "undefined") {
                    if (imageUploaded === true) photo_url = $('#userPhoto').attr("src");
                    else photo_url = random_avatar;
                }
                else photo_url = random_avatar;
                if (!name_val) {
                    $('#registerNameTicker').text('All Fields Are Required').show();
                    emailRegistrationInProgress = false;
                } else if (!email_val) {
                    $('#registerEmailTicker').text('All Fields Are Required').show();
                    emailRegistrationInProgress = false;
                } else if (!pass_val) {
                    $('#registerPasswordTicker').text('All Fields Are Required').show();
                    emailRegistrationInProgress = false;
                }
                else {
                    TN.services.registerCustomer(email_val, pass_val, name_val, "", photo_url).done(function(msg){
                        if ((msg[0] == true) || (typeof msg[0] === "number")) {
                            TN.utils.setCookie("TNUser", email_val, null, 345600);
                            TN.services.loginCustomer('', email_val, pass_val).done(function(msg){
                                if (typeof msg[0] === "number") TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
                                TN.services.sendBuzzRequest(email_val, "Message Board", "Q", photo_url).done(function(){
                                    _gaq.push(['_trackEvent', 'Join', 'Register', 'User: ' + email_val]);
                                    location.href="mypage.html";
                                });
                            });
                        }
                        else {
                            TN.utils.passiveNotification('Error!', "Error occured during the registration process.");
                            emailRegistrationInProgress = false;
                        }
                    }).fail(function(msg){
                        if ((msg == null) || (msg === null)) TN.utils.passiveNotification('Error!', "Error occured during the registration process.");
                        else {
                            jQuery.each( msg, function(key, value){ if (key == "status") regfail = value; });
                            if (regfail == 409) {
                                $('#registerEmailTicker').text('Provided e-mail is already registered. Please try a different e-mail address.').show();
                            }
                            else if (regfail == 500) {
                                $('#registerNameTicker').text('All Fields Are Required').show();
                                $('#registerEmailTicker').text('All Fields Are Required').show();
                                $('#registerPasswordTicker').text('All Fields Are Required').show();
                            }
                            else TN.utils.passiveNotification('Error!', "Error occured");
                        };
                        emailRegistrationInProgress = false;
                    });
                }
            });
                    
            function getclipThisInstHtml(){
                return ('<!--div id="clipThisLightbox-bg" ></div-->\
                        <!--div id="clipThisLightbox-panel"-->\
                        <div id="clipThisHowTo" class="reveal-modal">\
                        <div id="clipThisHeader">\
                            <div id="clipThisTitle">\
                                <h3>Clip This How To</h3>\
                            </div>\
                            <!--div id="clipThisClose">\
                                <a id="clipThisClose-panel" href="#"><img src="images/icons/close-button.png"/></a>\
                            </div-->\
                            <div><a class="close-reveal-modal"></a></div>\
                        </div>\
                        <div id="clipThisAction">\
                            <div id="clipThisButton"></div>\
                            <div id="clipThisCallToAction">\
                                <a id="clipThisAnchor" href="#" onclick="return false;" title="Clip this"><img src="images/clipthislink.png" alt="Clip This"></a>\
                                <p>Add this link to your bookmark bar</p>\
                            </div>\
                        </div>\
                        <div id="clipThisInstructions">\
                            <span>\
                            <span class="clipThisInstructionsBrowsers">Chrome, Firefox, Safari</span><br>\
                            1. Drag the "Clip This" button to your browser&apos;s toolbar<br>\
                            2. When you are browsing, click the "Clip This" button to clip a story</span>\
                            <br><br>\
                            <span class="clipThisInstructionsBrowsers">IE</span><br>\
                            <span>1. Display your Bookmarks Bar by clicking on <br>View &gt; Toolbars &gt; Bookmarks Toolbar<br>\
                            2. Add the "Clip This" button to your Bookmarks Toolbar<br>\
                            3. When you are browsing the web, click the "Clip This" button to clip a story</span>\
                        </div>\
                    <!--/div--> \
                    <!--/div-->'
                );
            }
            
            $('body').append(getclipThisInstHtml());

            $('#addClipThis').click(function(){
                if( TN.header.isGuestUser() ) return;

                $('#clipThisAnchor').attr('href', "javascript:void((function(){var s=document.createElement('script');s.type='text/javascript';s.src='http://"+TNServerAddress+"/php/clipthis.php?r='+Math.random()*99999999;document.body.appendChild(s)})())");
                $('#clipThisHowTo').css('padding', '0');
                $('#clipThisHowTo').reveal();
            });
            
            // usage: log('inside coolFunc',this,arguments);
            // http://paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
            window.log = function(){
              log.history = log.history || [];   // store logs to an array for reference
              log.history.push(arguments);
              if(this.console){
                console.log( Array.prototype.slice.call(arguments) );
              }
            };
            
            function showRevealsFromFB(){
                if( !!lightboxId ){
                	TN.lightbox.show(custId, lightboxId);            
                }
            
                if( !!npId ){
                	TN.newspaperViewLB.init(npId);
                }          	
            }
            
            var pollNotifications = function() {
                if (notificationsLoaded) {
                    TN.services.getNotificationTrayDetails(custId, 1, 1).done(function(json){
                        if (json[0].totalRecords > notificationsNum) {
                            var incomingNotifsN = parseFloat(json[0].totalRecords) - parseFloat(notificationsNum);
                            loadNotifications(1, false, true, incomingNotifsN);
                            uncheckedNotifs += incomingNotifsN;
                            $('#id_newNotifiactionsNum').text('['+uncheckedNotifs+']');
                            $('#notifAlert').text(incomingNotifsN).show();
                            TN.utils.setCookie("TNShowNotif", true, null, 345600);
                            // log("incomingNotifsN: "+incomingNotifsN+" uncheckedNotifs: "+uncheckedNotifs);
                        }
                    });
                }
                setTimeout(pollNotifications, 60000);
            };

            setTimeout(pollNotifications, 60000);
            
            //log('0');
            // Check to see if we are logged-in on server side
            TN.services.pingServer().done(function(){
                //log('1');
                if (!!custId ) {
                    //log('2');
                    // Everything ok, considered logged in at this point  
                    //log('calling loadUserInfo with custId: ' + custId);
                    //log(typeof jQuery);
                    $header.userInfoFetch = TN.services.loadUserInfo(custId, custId);
                    $header.userInfoFetch.done(function(msg){
                        TN.userInfo = msg[0];
                        // $('#myNamePage').find('a').text(TN.userInfo.firstName + "'s Page");
                        TN.utils.setCookie('TNUserUrl', msg[0].profileThumbPicUrl, null, 345600);
                        TN.utils.setCookie('TNUserName', msg[0].firstName, null, 345600);
                        
                        showRevealsFromFB();
                    });
                    $('#postStoryHeaderDropdown, #myPageHeaderDropdown, #notificationsList').show();
                    if( custId !== TN.guestUserName ){
                        // $('#signInOutLink').text('Sign out');
                        //$('.signInOutParentLi').css('width', 'auto');
                    }
                    else {
                        //$('#signInOutLink').text('Sign in');
                        $('#signOut').hide();                        
                    }
                    $.getScript("javascripts/socketClient.js");
                    $.getScript("javascripts/lightbox-tool.js");
                    TN.services.keepThisSessionAlive();
                    loadNotifications(1);
                }
                else {
                	if( !imageSrc ){
                    	loginAsGuest(lightboxId, npId);
                	}
                	
//                    $('#postStoryHeaderDropdown, #myPageHeaderDropdown, #notificationsList').hide();
                    //log('3');
//                    if (!TN.homepage) parent.location.href = 'index.html';
                    //$('#signInOutLink').text('Sign in');
                    $('#signOut').hide();
//                    TN.userInfo = {};
                }
            }).fail(function(){
                //log('4');
                if (!TN.homepage) parent.location.href = 'index.html';
                document.cookie = 'TNUser=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                document.cookie = 'TNUserNum=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                document.cookie = 'TNUserUrl=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                document.cookie = 'TNUserName=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                document.cookie = 'TNShowNotif=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                $('#postStoryHeaderDropdown, #myPageHeaderDropdown, #notificationsList').hide();
                TN.userInfo = {};
                custId = null;
                //$('#signInOutLink').text('Sign in');
                $('#signOut').hide();
            	if( !imageSrc ){
                	loginAsGuest(lightboxId, npId);
            	}
            });
            //log('5');
            //TN.utils.passiveNotification('Info:', '5');
            //console.log(imageSrc);
            if( !!imageSrc ){
                //TN.utils.passiveNotification('Info:', '6');
                TN.createStory.show(imageSrc, storyUrl, true);
            };

            //TN.utils.passiveNotification('Info:', '7');
            if( !!signIn ){
            	$('#signInRegisterModal').reveal();
            }            
        }
        else {
            //TN.utils.passiveNotification('Info:', '8, attempt: ' + attempt);
            //console.log('reattempting '+attempt);
            if (attempt < maxAttempts) {
                setTimeout(function(){
                    TN.header.initialize();
                }, 2500);
            };
        };
    };
    
})(TN.header);