if (!TN) var TN= {};
if (!TN.FB) TN.FB= {};

(function($fb){
	var facebookLoggingIn = false;
	
	// Additional JS functions here
	window.fbAsyncInit = function() {
		FB.init({	
			appId      : '453204974697950', // App ID
			channelUrl : 'channel.html', // Channel File
			status     : true, // check login status
			cookie     : true, // enable cookies to allow the server to access the session
			xfbml      : true  // parse XFBML
		});

	// Additional init code here
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				// connected
			} else if (response.status === 'not_authorized') {
				//login();
				// not_authorized
			} else {
				//login();
				// not_logged_in
			}
		});
	};

	// Load the SDK Asynchronously
	(function(d){
	  var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	  if (d.getElementById(id)) {return;}
	  js = d.createElement('script'); js.id = id; js.async = true;
	  js.src = "//connect.facebook.net/en_US/all.js";
	  ref.parentNode.insertBefore(js, ref);
	}(document));	
	
	function loginFBUser(userId, userPicUrl, firstLogin){
        TN.services.loginCustomer('', userId, 'qwe').done(function(msg){
            if ( (typeof msg[0] === "number") || ((typeof msg[0] === "boolean") && (msg[0] === true)) ) {
            	TN.utils.setCookie("TNUser", userId, null, 345600);
	          	TN.utils.setCookie("TNUserNum", msg[0], null, 345600);
	          	TN.utils.setCookie("TNFBUser", true, null, 345600);
	            if (firstLogin) {
	            	TN.services.sendBuzzRequest(userId, "Message Board", "Q", userPicUrl).done(function(){
	                	// window.location.href='../' + location.search; // 
	                	_gaq.push(['_trackEvent', 'Join', 'FB Sign In', 'User: ' + userId]);
	                	location.href="index.html";
	            	}).always(function(){
	             		facebookLoggingIn = false;
	            	});
	            }
	            else {
	            	_gaq.push(['_trackEvent', 'Join', 'FB Sign In', 'User: ' + userId]);
	            	facebookLoggingIn = false;
	            	location.href="index.html";
	            };
            }
        }).fail(function(msg){
        	facebookLoggingIn = false;
        	TN.utils.passiveNotification('Error!', 'Unable to log the Facebook account into TinyNews due to an unknown error. Please try again later.');
        });
	}
	
	function registerOrLoginFBUser(facebook_auth_response){
		  $.ajax({
		    url: "/php/facebook_authenticate.php",
		    type: "POST",
		    data: {facebook_id : facebook_auth_response.id},
		    dataType: "html"
		  }).done(function(userId){
			  var fbId = facebook_auth_response.id;
			  var mockedId = (fbId.length <= 5 ? 'x' + fbId : 'x' + fbId.slice(-5));
			  console.log(mockedId);
			  var userPicUrl = 'http://graph.facebook.com/'+fbId+'/picture?type=large';
			  $('#userPhoto').attr('src', userPicUrl);
              TN.services.registerCustomer(userId, 'qwe', (!!facebook_auth_response.name ? facebook_auth_response.name : 'Facebook User' + mockedId), "",userPicUrl).done(function(msg){
                  if ((msg == null) || (msg === null)) {
                	  //This Facebook account is already registered with Tiny News.
                	  loginFBUser(userId, userPicUrl, false);
                  }
                  else if ((msg[0] == true) || (typeof msg[0] === "number")) {
                	  loginFBUser(userId, userPicUrl, true);
                  }
                  else {
                	  facebookLoggingIn = false;
                      TN.utils.passiveNotification('Error!', "Error occured during the registration process.");
                  }
              }).fail(function(msg){
            	  console.log(msg);
                  if ((msg == null) || (msg === null)) {
                	  facebookLoggingIn = false;
                	  TN.utils.passiveNotification('Error!', "Unknown error occured during the registration process. Please try again later.");
                  }
                  else {
                      jQuery.each( msg, function(key, value){ if (key == "status") regfail = value; });
                      if (regfail == 409) {
                      	loginFBUser(userId, userPicUrl, false);
                      }
                      else {
                    	  facebookLoggingIn = false;
                    	  TN.utils.passiveNotification('Error!', "Error occured during the registration process. Please try again later.");
                      }
                  };
              });
		  }).fail(function(userId){
			    TN.utils.passiveNotification('Error!', 'Error occured while authenticating your Facebook account.  Please try again later');
			    facebookLoggingIn = false;
		  });
		}
	
	$fb.registerOrLogin = function(){
		if( facebookLoggingIn === true ){
			return;
		}
		
		facebookLoggingIn = true;
		FB.login(function(response) {
			console.log(response);
			if (response.authResponse) {
				FB.api('/me', function(response) {
					console.log(response);
					registerOrLoginFBUser(response);
				});
			} else {
				facebookLoggingIn = false;
				// cancelled
			}
		},{scope : 'email'});		
	};
	
	$fb.isFacebookUser = function(){
		var FBUserCookie = TN.utils.getCookie('TNFBUser');
		return ( !!FBUserCookie && FBUserCookie === 'true' );
	};
	
	$fb.logout = function(){
		var FBUserCookie = TN.utils.getCookie('TNFBUser');
		if( !!FBUserCookie && FBUserCookie === 'true' ){
			FB.logout();
          	TN.utils.setCookie("TNFBUser", false, null, 345600);
		}
	};
	
	function postToFacebook(postInfo, fnCallback){
		var postObject;
		
		if( !!postInfo.messageId ){
			postObject = {
				method:'feed',
				link:'http://www.tinynews.me?lightboxId=' + postInfo.messageId,
				picture:postInfo.url,
				name:postInfo.headline
			};			
		} else {
			postObject = {
					method:'feed',
					link:'http://www.tinynews.me?npId=' + postInfo.npId,
					picture:postInfo.url,
					name:postInfo.headline
				};						
		}
		
		FB.ui(postObject, fnCallback);
	}
	
	$fb.post = function(postInfo, fnCallback){
		//Check if user already logged in facebook:
		FB.getLoginStatus(function(response) {
			  if (response.status === 'connected') {
			    // the user is logged in and has authenticated your
			    // app, and response.authResponse supplies
			    // the user's ID, a valid access token, a signed
			    // request, and the time the access token 
			    // and signed request each expire
			    var uid = response.authResponse.userID;
			    var accessToken = response.authResponse.accessToken;

			    postToFacebook(postInfo, fnCallback);
			  } else if (response.status === 'not_authorized') {
			    // the user is logged in to Facebook, 
			    // but has not authenticated your app
					FB.login(function(response) {
						if (response.authResponse) {
							postToFacebook(postInfo, function(fbResponse){
								FB.logout();
								fnCallback(fbResponse);
							});
						}
					},{scope : 'email'});		
			  } else {
			    // the user isn't logged in to Facebook.
					FB.login(function(response) {
						if (response.authResponse) {
							postToFacebook(postInfo, function(fbResponse){
								FB.logout();
								fnCallback(fbResponse);
							});
						}
					},{scope : 'email'});		
			  }
		});
	};
	
})(TN.FB);