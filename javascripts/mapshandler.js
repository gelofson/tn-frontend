
(function($TN){
	
	var catType = 'Breaking News';
	var mapDissolved = false;
	var mapsHandler = {};
	(function($mapsHandler){
		var apiKey = 'AIzaSyC3JpD-ZB_NgciJjjPDaz5u9G11vs37Czc';
		var mapCanvas = $('#map_canvas');
//		var mapInset = $('#mapInset');
//		var mapCanvasLeftOffset = mapCanvas.offset().left;
//		var mapCanvasTopOffset = mapCanvas.offset().top;
		var mapCanvasObject = null;
		var trafficLayer = null;
		var canvasZoomLevel = 2;
//		var insetZoomLevel = 12;
		var mapInsetObject = null;
		var latLongArray = {};
        var prevwindow = null;
        var showHeadlines = false;
        var all=document.getElementById("all");
//        var fr=document.getElementById("friends");
//        var foll=document.getElementById("following");
//        var me=document.getElementById("me");
//        var pop=document.getElementById("popular");
//        var vid=document.getElementById("videos");
                     
        var currentfilter=all;
        $(currentfilter).css('background','#0b68a8').css('color','white');     
		var custId=TN.utils.getCookie('TNUser');
//		var currentOpenDetailBox = null;
		
//		var loadingInProgress = false;
//		var interruptSignal = false;
//		var interruptCompleteCallback;
			
//		var insetBorderLeftWidth = parseFloat(mapInset.css('border-left-width'));
//		var insetBorderTopWidth = parseFloat(mapInset.css('border-top-width'));
						
		var latLng = null;
		var canvasOptions = null;
//		var insetOptions = null;
		
		var markersArray = [];
		var markerCluster = null;
//		var localCats = [];
//		var globalCats = [];
//		var catBarElem = $('#categoriesBar');
		var catDropDown = $('#Categories');
		var newsCatsFirstCol = $('#newsFirstColumn');
		var newsCatsSecondCol = $('#newsSecondColumn');
		var newsCatsThirdCol = $('#newsThirdColumn');
		var eventCatsFirstCol = $('#eventsFirstColumn');
		var eventCatsSecondCol = $('#eventsSecondColumn');
		var dealCatsFirstCol = $('#dealsFirstColumn');
		var dealCatsSecondCol = $('#dealsSecondColumn');
		
		var mapFilter = 'all';
		var boundchange = 0;
        var state=mapFilter;
        var prevId=-1;
        var count=0;
        var refreshslide = 0;
        var countcurr=-1;
        var current_slider=$("#slider");
        var sliderarray=[];
        
		var currAjaxCallId = null;
		var mapEnlarged = false;
		var currentSearchString = '';

		var currentDealTags = [];
		var selectedDealTag = '';
                
		function getInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content){
			var detailedBox = document.createElement('div');
			var onClickAction = '';
			if (( content.markerType == "eventMarker" ) || ( content.markerType == "eventfindaMarker" )) onClickAction = 'TN.showEventLightbox(\'' + messageId + '\');';
			else onClickAction = 'TN.lightbox.show(\'' + custId + '\', ' + messageId + ');';

			detailedBox.innerHTML = '<div class="inlineBox block" onclick="' + onClickAction + '">\
				<a href="#" class="inlineBox-btnClose"></a>\
				<div class="inlineBoxContainer block">\
					<div class="inlineBox-imageBlock">\
						<div class="block" >\
							<div class="inlineBox-imageWrapper">\
								<img ' + (content.markerType == 'eventfindaMarker' ? 'style="max-width:220px !important"' : '') + ' src="' + thumbImageUrl + '" title="Story Image" >\
							</div>\
						</div>\
						<div class="inlineBox-User block" style="display: ' + ((content.markerType == "eventMarker" || content.markerType == "eventfindaMarker") ? "none" : "") + '">\
							<img src="' + (!!content.originatorImageUrl ? content.originatorImageUrl : '') + '" title="' + content.originatorName + '">\
							<h2>by <a href="#">' + (!!content.originatorName ? content.originatorName : '') + '</a></h2>\
							<p>' + (!!content.elapsedTime ? content.elapsedTime : '') + '</p>\
						</div>\
					</div>\
					<div class="inlineBox-textBlock block">\
						<h1>' + markerTitle + '</h1>' +
						(!!content.who ? '<p>Who: ' + content.who + '</p>' : '') +
						(!!content.what ? '<p>What: ' + content.what + '</p>' : '') +
						(!!content.where ? '<p>Where: ' + content.where + '</p>' : '') +
						(!!content.when ? '<p>When: ' + content.when + '</p>' : '') +
						(!!content.how ? '<p>How: ' + content.how + '</p>' : '') +
						(!!content.why ? '<p>Why: ' + content.why + '</p>' : '') +
						(!!content.oped ? '<p>' + content.oped + '</p>' : '') +
					'</div>\
				</div>\
			</div>';
			detailedBox.style.cssText = 'width:506px; height:280px; cursor:pointer;';
			return detailedBox;
		}

		function getDealInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content){
			var detailedBox = document.createElement('div');
			detailedBox.innerHTML = '<div class="deals-container twelve columns" onclick="TN.showDealLightbox(\'' + content.optionId + '\', ' + messageId + ');">\
						<div class="arrow"></div>\
						<div class="groupon three columns">\
						<div class="image-container"><img style="width:300px" src="' + thumbImageUrl + '" alt="picture"></div>\
						<div class="deals-detail">\
							<p class="deals-detail-headline">' + markerTitle + '</p>\
							<div class="discounts">\
								<div class="left"><p>' + content.city + '</p></div>\
								<div class="right deals"><p id="strik-off">($' + content.itemValue + ')</p><p>$' + content.itemPrice + '</p></div>\
							</div>\
						</div>\
						</div>\
						</div>';
			detailedBox.style.cssText = 'width:300px; height:295px; cursor:pointer;';
			return detailedBox;
		}
		
        function addMarker( lat, long, messageId, markerTitle, thumbImageUrl, content, update){
			// Ref code for InfoBox: http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/docs/examples.html
			
			function getInfoBoxHtml(){
				return( '<div class="infoBoxContent">\
                                <img class="thumbImage" src="' + thumbImageUrl + '"></img> \
							<div class="headline">' + markerTitle + '</div>\
						</div>' );
			}			
			
			var latLong = new google.maps.LatLng(lat, long);
                        
			var markersObject = createMarker(latLong, markerTitle, content.markerType);
			
			var infoWindowOptions = {
				boxClass: (content.markerType == 'dealMarker' ? "dealInfoWindowDetailed" : "infoWindowDetailed"),
				maxWidth:0,
				alignBottom:false,
				pixelOffset:new google.maps.Size(5, -110),
				boxStyle:{ background: (content.markerType == 'dealMarker' ? "" : "url(images/body-bg.png) no-repeat")},
				content: (content.markerType == 'dealMarker' ? getDealInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content) : getInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content)),
				closeBoxURL: (content.markerType == 'dealMarker' ? "" : "images/icons/close-button.png"),
				closeBoxMargin: "15px 15px 2px 2px",
				disableAutoPan:true,
				position:latLong,
				enableEventPropagation:false
			};
			
//			var infoWindow = new InfoBox(infoBoxOptions);
			var infoWindowDetailed = new InfoBox(infoWindowOptions);
			
                        
            google.maps.event.addListener(mapCanvasObject,"bounds_changed",function() {
                if (countcurr===-1){
                	boundchange=1;
                	mapsHandler.interruptOrReload();
                }
                else{
                	refreshslide = 1;           
                }
            });
            
            google.maps.event.addListener(mapCanvasObject,'mouseup',function (){
            	if(refreshslide===1){
            		refreshslide=-1;
            		SliderReload();
            	}
            	//console.log('setMarker mapCanvasObject.getZoom(): ' + mapCanvasObject.getZoom());
            	//console.log('setMarker mapCanvasObject.getBounds(): ', mapCanvasObject.getBounds());
            });
           
            google.maps.event.addListener(mapCanvasObject,'click',function (){
                if (prevwindow !==null){
                	prevwindow.close();
                    prevwindow=null;
                }
            });
            
            google.maps.event.addListener(mapCanvasObject,'zoom_changed',function (){
            	if(refreshslide===1||refreshslide===-1){
            		refreshslide=0;
            		SliderReload();
            	}
            	//console.log('setMarker mapCanvasObject.getZoom(): ' + mapCanvasObject.getZoom());
            	//console.log('setMarker mapCanvasObject.getBounds(): ', mapCanvasObject.getBounds());
            });
               
            google.maps.event.addListener(infoWindowDetailed, 'domready', function(){
                // Clamp deal headline on medium sized card to 80px of height
                var itemsToClamp = document.getElementsByTagName('body')[0].getElementsByClassName('deals-detail-headline');
                $.each (itemsToClamp, function(i, v){$clamp(v, {clamp: '80px'});});
			});

			google.maps.event.addListener(infoWindowDetailed, 'closeclick', function(){
                prevwindow=null;
			});

			google.maps.event.addListener(markersObject.globalMarker, 'mouseover', function(){
//				if( !showHeadlines && currentOpenDetailBox === null){
				
                    if (prevwindow !==null){
                    	prevwindow.close();
                        prevwindow=null;
                    }
					infoWindowDetailed.open(mapCanvasObject, markersObject.globalMarker);
//                  skimlinks();
                    prevwindow = infoWindowDetailed;
//				}

//			google.maps.event.addListener(markersObject.globalMarker, 'mouseout', function(){
//				if(!showHeadlines ){
//					infoWindowDetailed.close();
//				}
//				markersObject.globalMarker.setAnimation(null);
			});
			
			
			google.maps.event.addListener(markersObject.globalMarker, 'click', function(){
				if (content.markerType == 'dealMarker') TN.showDealLightbox(content.optionId, messageId);
				else if ( (content.markerType == 'eventMarker') || (content.markerType == 'eventfindaMarker') ) TN.showEventLightbox(messageId);
				else TN.lightbox.show(custId, messageId);
            });		
			
			latLongArray[lat] = {};
			latLongArray[lat][long] = {};
			latLongArray[lat][long].globalMarker = markersObject.globalMarker;
			if( !!markersObject.insetMarker ){
				latLongArray[lat][long].insetMarker = markersObject.insetMarker;
            }
			
		    latLongArray[lat][long].headline = infoWindowDetailed;
          
            if (prevwindow !==null){
              prevwindow.close();
            }
           
            var currwindow = latLongArray[lat][long];
//            currwindow.headline.open(mapCanvasObject, currwindow.globalMarker);

            prevwindow=currwindow.headline;    
        }
		
        function TooltipOpen(lat,long, messageId, markerTitle, thumbImageUrl, content){
            		
			function getInfoBoxHtml(){
				return( '<div class="infoBoxContent">\
                                <img class="thumbImage" src="' + thumbImageUrl + '"></img> \
							<div class="headline">' + markerTitle + '</div>\
						</div>' );
			}
			
			var latLong = new google.maps.LatLng(lat, long);
                        
//			function getInfoBoxDetailed(){
//				
//				var detailedBox = document.createElement('div');
//				detailedBox.innerHTML = '<div class="inlineBox block">\
//					<a href="#" class="inlineBox-btnClose"></a>\
//					<div class="inlineBoxContainer block">\
//						<div class="inlineBox-imageBlock">\
//							<div class="block" >\
//								<div class="inlineBox-imageWrapper">\
//									<img src="' + thumbImageUrl + '" title="Story Image" >\
//								</div>\
//							</div>\
//							<div class="inlineBox-User block">\
//								<img src="' + content.originatorImageUrl + '" title="' + content.originatorName + '">\
//								<h2>by <a href="#">' + content.originatorName + '</a></h2>\
//								<p>' + content.elapsedTime + '</p>\
//							</div>\
//						</div>\
//						<div class="inlineBox-textBlock block">\
//							<h1>' + markerTitle + '</h1>\
//							<p>Who: ' + content.who + '</p>\
//							<p>When: ' + content.when + '</p>\
//							<p>What: ' + content.what + '</p>\
//						</div>\
//					</div>\
//				</div>';
//				detailedBox.style.cssText = 'width:506px; height:280px;';
//				return detailedBox;
//			}
				
//			var infoBoxOptions = {
//				maxWidth:0,
//				alignBottom:false,
//				pixelOffset:new google.maps.Size(5, -50),
//				boxStyle:{ background: "url('./images/icons/infobox_arrow.png') no-repeat left bottom"},
//				content: getInfoBoxHtml(),
//				disableAutoPan:true,
//                closeBoxURL:"none",
//                closeBoxMargin:"2px 10px 0px 2px",
//			    position:latLong,
//				enableEventPropagation:true
//			};
			var infoWindowOptions = {
				boxClass: (content.markerType == 'dealMarker' ? "dealInfoWindowDetailed" : "infoWindowDetailed"),
				maxWidth:0,
				alignBottom:false,
				pixelOffset:new google.maps.Size(5, -110),
				boxStyle:{ background: (content.markerType == 'dealMarker' ? "" : "url(images/body-bg.png) no-repeat")},
				content: (content.markerType == 'dealMarker' ? getDealInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content) : getInfoBoxDetailed(messageId, markerTitle, thumbImageUrl, content)),
				closeBoxURL: (content.markerType == 'dealMarker' ? "" : "images/icons/close-button.png"),
				closeBoxMargin: "15px 15px 2px 2px",
				disableAutoPan:true,
				position:latLong,
				enableEventPropagation:false
			};

//			var infoWindow = new InfoBox(infoBoxOptions);
			var infoWindow = new InfoBox(infoWindowOptions);
	             
			return infoWindow;
        }
        
        function sliderReset(){
        	current_slider.remove();
        	$('#slideme').append('\<div id="slider" class="slider-horizontal"></div>');
        	current_slider=$('#slider');
        	prevId=-1;
        	current_slider.FlowSlider();

/*			if( !!window.chrome ){
	        	current_slider.unbind('flowSliderMoveStop').bind('flowSliderMoveStop', function(slider){
	        		$('body').click();
	        	});				
			}
*/
        }
        
        function SliderReload(){
                      
        	sliderReset();
	        for(var i=0;i<countcurr;i++){
	        	var MapItem=sliderarray[i];
	        	var latLong = new google.maps.LatLng(MapItem.latitude,MapItem.longitude);			
	        	if( mapCanvasObject.getBounds().contains(latLong)){
				                        
	    			var storyImageUrl = (!!MapItem.senderThumbImageUrl ?  MapItem.senderThumbImageUrl : MapItem.thumbImageUrl );
                    var SliderItem= '\<div class="item"><div class="itemcontainer" id='+i+'><img src="'+storyImageUrl+'" />\
                                      <div class="text">'+(!!MapItem.headline ? MapItem.headline : 'Test Headline') +'</div>\
                                      </div></div>';
                                      current_slider.FlowSlider().content().append(SliderItem);
                                      current_slider.FlowSlider().setupDOM();
                  
                    $('#'+i+'').click( function(){
                                      var ID = $(this).attr("id");
                                      var selecteditem=sliderarray[ID];
                                      if (MapItem.markerType == 'dealMarker') TN.showDealLightbox(selecteditem.optionId, selecteditem.messageId);
                                      else if ( (MapItem.markerType == 'eventMarker') ||  (MapItem.markerType == 'eventfindaMarker') ) TN.showEventLightbox(selecteditem.messageId);
                                      else TN.lightbox.show(custId, selecteditem.messageId);
                     }); 
                    
                    $('#'+i+'').hover( function() {
	                      var ID = $(this).attr("id");
//	                      if (prevId!==ID){

	                         var selecteditem=sliderarray[ID];
	                         if (prevwindow !==null){
	                        	 prevwindow.close();
	                         }
							
							var markerContent = {};
							if ( selecteditem.markerType == 'dealMarker' ){
								markerContent = {
									markerType: selecteditem.markerType,
									itemTitle: selecteditem.headline,
									thumbImageUrl: selecteditem.thumbImageUrl,
									merchantName: selecteditem.merchantName,
									itemPrice: selecteditem.optionPrice,
									discountedVal: selecteditem.optionDiscountedValue,
									itemValue: selecteditem.optionValue, 
									city: selecteditem.redemptionLocationCity,
									optionId: selecteditem.optionId, 
									fullItemTitle: selecteditem.fullItemTitle,
									generalRedemptionLocation: selecteditem.generalRedemptionLocation,
									dealUrl: selecteditem.dealUrl,
									discountPercent: selecteditem.discountPercent
								};
							}
							else if ( selecteditem.markerType == 'eventMarker' ){
								markerContent = {
									what: selecteditem.what,
									when: selecteditem.when,
									where: selecteditem.Venue.Address,
									how: selecteditem.Venue.Access,
									oped: selecteditem.Description,
									markerType: selecteditem.markerType,
									itemTitle: selecteditem.itemTitle,
									thumbImageUrl: selecteditem.thumbImageUrl,
									itemUrl: selecteditem.itemUrl,
									itemId: selecteditem.itemId
								};
							}
							else if ( selecteditem.markerType == 'eventfindaMarker' ){
								markerContent = {
									what: selecteditem.what,
									when: selecteditem.when,
									where: selecteditem.where,
									who: selecteditem.who,
									oped: selecteditem.oped,
									markerType: selecteditem.markerType,
									itemTitle: selecteditem.name,
									thumbImageUrl: selecteditem.thumbImageUrl,
									itemUrl: selecteditem.itemUrl,
									itemId: selecteditem.id
								};
							}
							else if ( !!selecteditem.wsBuzz ){
								markerContent = {
									markerType: 'storyMarker',
									what: selecteditem.wsBuzz.what,
									when: selecteditem.wsBuzz.when,
									where: selecteditem.wsBuzz.where,
									who: selecteditem.wsBuzz.who,
									why: selecteditem.wsBuzz.why,
									how: selecteditem.wsBuzz.how,
									oped: selecteditem.wsBuzz.oped,
									originatorImageUrl: selecteditem.originatorImageUrl,
									originatorName: selecteditem.originatorName,
									elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 
								};
							}
							else {
		                         markerContent = {
		                         	markerType: 'storyMarker',
									what: selecteditem.what,
									when: selecteditem.when,
									where: selecteditem.where,
									who: selecteditem.who,
									why: selecteditem.why,
									how: selecteditem.how,
									oped: selecteditem.oped,
									originatorImageUrl: selecteditem.customerThumbImageUrl,
									originatorName: selecteditem.firstName,
									elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 											
	 	    					};
							}
	                                              
	                         var selectedImageUrl = (!!selecteditem.senderThumbImageUrl ?  selecteditem.senderThumbImageUrl : selecteditem.thumbImageUrl );
	
	                         var currtip=TooltipOpen(selecteditem.latitude,selecteditem.longitude, selecteditem.messageId, (!!selecteditem.headline ? selecteditem.headline : 'Test Headline'),
	                        		selectedImageUrl, markerContent);
	                          //TN.utils.passiveNotification('Info:', latLongArray[selecteditem.latitude][selecteditem.longitude].toSource);
	                          // if(latLongArray[selecteditem.latitude][selecteditem.longitude]!=={}){

	                        var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
	                        currtip.open(mapCanvasObject, currwindow.globalMarker);
//                          skimlinks();

	                        prevwindow=currtip; 
	                                                                                              
//	                      }

	                      prevId=ID;
                    });  
                    
                  }
	        }
        }
                
		function createMarker( latLng, markerTitle, markerType ){
			var markersObject = {};
  			var markerImage;
  			if ( markerType == 'dealMarker' ) {
  				markerImage = new google.maps.MarkerImage('images/icons/deals-icon.png',
                              new google.maps.Size(34,34),
                              new google.maps.Point(0,0),
                              new google.maps.Point(18,34));
  			}
  			else if (( markerType == 'eventMarker' ) || ( markerType == 'eventfindaMarker' )) {
  				markerImage = new google.maps.MarkerImage('images/icons/events-icon.png',
                              new google.maps.Size(34,34),
                              new google.maps.Point(0,0),
                              new google.maps.Point(18,34));
  			}
  			else markerImage = new google.maps.MarkerImage('images/mappin.png',
                              new google.maps.Size(29,29),
                              new google.maps.Point(0,0),
                              new google.maps.Point(15,29));
                                        
            var shadow = new google.maps.MarkerImage('images/mappinshadow.png',
                         new google.maps.Size(47,29),
                         new google.maps.Point(0,0),
                         new google.maps.Point(15,29));   
            var shape = {
                    coord: [28,0,28,1,28,2,28,3,28,4,28,5,28,6,28,7,28,8,28,9,28,10,28,11,28,12,28,13,28,14,28,15,28,16,28,17,28,18,28,19,28,20,28,21,28,22,28,23,28,24,28,25,28,26,28,27,28,28,0,28,0,27,0,26,0,25,0,24,0,23,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,0,5,0,4,0,3,0,2,0,1,0,0,28,0],
                    type: 'poly'
                    };             
			// Create inset map marker if inset map exists
			if( !!mapInsetObject ){
				markersObject.insetMarker = new google.maps.Marker({     
					  position: latLng,   
					  title: markerTitle,
					//  animation: google.maps.Animation.DROP,
					  icon: markerImage,
                                          shadow: shadow,
                                          shape: shape,
					  map: mapInsetObject  });
		                 //markerCluster.addMarkers(markersObject.insetMarker);		
				 //markersArray.push(markersObject.insetMarker);
			}
			
			markersObject.globalMarker = new google.maps.Marker({     
				position: latLng,   
				//animation: google.maps.Animation.DROP,
				icon: markerImage,
                                shadow: shadow,
                                shape: shape,
				map: mapCanvasObject  });
			
			markersArray.push(markersObject.globalMarker);

                             
			  // create and return global map marker so additional processing can be done for it.
			return( markersObject );
		}
		
		function getRequestSummaries(custId, numItems, pageNum, type){
			if(state!==mapFilter||boundchange === 1){
				current_slider.remove();
				$('#slideme').append('\<div id="slider" class="slider-horizontal"></div>');
				current_slider=$('#slider');
				boundchange=0;
			}
			countcurr=-1;
			state=mapFilter;
			current_slider.FlowSlider();
			
/*			if( !!window.chrome ){
	        	current_slider.unbind('flowSliderMoveStop').bind('flowSliderMoveStop', function(slider){
	        		$('body').click();
	        	});				
			}
*/ 
			if( mapFilter === 'all' ){
				return(TN.services.getAllRequestSummaries(custId, numItems, pageNum, type));
			}
			
			if( mapFilter ==='me' ){
				return(TN.services.getMyRequestSummaries(custId, numItems, pageNum, type));
			}
			
			if( mapFilter ==='friends' ){
				return(TN.services.getFriendsRequestSummaries(custId, numItems, pageNum, type));
			}
						
			if( mapFilter ==='following' ){
				return(TN.services.getFollowingRequestSummaries(custId, numItems, pageNum, type));
			}      
		}
		
		function loadCategories() {
			return(TN.services.getAllMessageTypeActiveCategories().done(function(json){
				if( !!json ){

					$('form.mapCategoriesSelectForm a.current').click(function(event){
						$('.newsmapCustomFlyout').toggle();
					});

					$('form.mapCategoriesSelectForm a.selector').click(function(event){
						$('.newsmapCustomFlyout').toggle();
					});

					// Ensure that Newsmap categories dropdown gets closed if clicked away
					$('body').click(function(event){
						var parentEls = $(event.target).parents().map(function () {
								return this.className;
							}).get().join(" ");
						if (( parentEls.indexOf('newsmapCustomFlyout') == -1 ) && ( parentEls.indexOf('mapCategoriesSelectForm') == -1)) {
							if ( ($('.newsmapCustomFlyout').css('display') == 'block') || ($('.newsmapCustomFlyout').css('display') == '') ){
								$('.newsmapCustomFlyout').hide();
							}
						}
					});

					function categoryLinkClick(){
						var selectedCat = this.rel;
						var labelCat = this.innerHTML;
						if( !!selectedCat ){
							$('#newsmap-search-input').val('');
							var currCat = selectedCat;
							TN.loadCategoryOnMap(currCat);
							$('.newsmapCustomFlyout').hide();
							$('#Categories option[value="' + currCat + '"]').attr("selected", "selected");
							$('form.mapCategoriesSelectForm a.current').text(labelCat);
							//$('form.mapCategoriesSelectForm div.custom ul')
						}
					};

					var itemsRendered = 0;
					var tempCounter	= 0;
					var maxItems = json.length;
					var testLabel;
					catDropDown.attr('disabled', 'disabled');

					for( var i = 0; i < maxItems; i++ ){
						testLabel = TN.categories.getLabel(json[i].id);
						if (!!testLabel) tempCounter++;
					};
					
					//var halfItems = parseInt(tempCounter / 2);
					var thirdOfItems = parseInt(tempCounter / 3);

					for( var i = 0; i < maxItems; i++ ){
						var catLabel = TN.categories.getLabel(json[i].id);
						
						//if label is valid
						if( !!catLabel ){
							if( ( json[i].global === 1 ) || ( json[i].global === 0 ) ){
								catDropDown.append('<option value="' + json[i].id + '">' + catLabel + '</option>').trigger('change');
								if (itemsRendered < thirdOfItems) {
									var link = $('<a href="javascript:void(0);" rel="' + json[i].id + '">' + catLabel + '</a><br>');
									newsCatsFirstCol.append(link);
									link.click(categoryLinkClick);
								}
								else if (itemsRendered < thirdOfItems*2 ) {
									var link = $('<a href="javascript:void(0);" rel="' + json[i].id + '">' + catLabel + '</a><br>');
									newsCatsSecondCol.append(link);
									link.click(categoryLinkClick);
								} else {
									var link = $('<a href="javascript:void(0);" rel="' + json[i].id + '">' + catLabel + '</a><br>');
									newsCatsThirdCol.append(link);
									link.click(categoryLinkClick);
								};
								itemsRendered++;
							}
						}
					}
					
					catDropDown.removeAttr('disabled');
					catDropDown.val('Breaking News').trigger('change');
					catDropDown.change(function(){
						var selectedCat = catDropDown.val();
						if( !!selectedCat ){
							$('#newsmap-search-input').val('');
							var currCat = selectedCat;
							TN.loadCategoryOnMap(currCat);
						}
					});
					
				}
				
			}));
		}
		
//		function moveLocalToMain(){
//			mapCanvasObject.setZoom(insetZoomLevel);
//			mapCanvasObject.setCenter(latLng);
//			mapInsetObject.setZoom(canvasZoomLevel);
//                        boundchange=1;
//                        mapsHandler.interruptOrReload();	
//                	google.maps.event.addListenerOnce(mapInsetObject, 'click', moveGlobalToMain);
//			
//			populateCatBar(localCats);
//		}
		
//		function moveGlobalToMain(){
//			mapCanvasObject.setZoom(canvasZoomLevel);
//			mapCanvasObject.setCenter(latLng);
//			
//			mapInsetObject.setZoom(insetZoomLevel);
//			boundchange=1;
//			mapsHandler.interruptOrReload();			
//			
//			google.maps.event.addListenerOnce(mapInsetObject, 'click', moveLocalToMain);
//			
//			populateCatBar(globalCats);
//		}
		
//		function populateCatBar(catArray){
//			var numCats = catArray.length;
//			
//			if( numCats >  0){
//				catBarElem.empty();
//				for (var i = 0; i < numCats; i++){
//					catBarElem.append('<li><a href="#catOnBar" onclick="TN.loadCategoryOnMap(\'' + catArray[i] + '\')" >' + catArray[i] + '</a></li>');
//				}
//			}
//		}
		function resetAndReload(searchOptions){
//			interruptSignal = false;
		
			if (markersArray) {
				for (i in markersArray) {
					markersArray[i].setMap(null);    
				}    
				markersArray.length = 0;  
			}
			latLongArray = {};
			markerCluster.clearMarkers();
            if (prevwindow !==null){
                prevwindow.close();
                prevwindow = null;
              }
			sliderReset();
        	
			setMarkers(searchOptions);
		}
            
		function crop(crop_id, x, y, width, height) {
                       
            var scale_x = (crop_id).attr('width') / width;
            var scale_y = (crop_id).attr('height') / height;

            (crop_id).css({
            	position: 'relative',
                overflow: 'hidden' 
            });

            (crop_id).css({
                 position: 'absolute',
                 display: 'block',
                 left: (-x * scale_x) + 'px',
                 top: (-y * scale_y) + 'px',
                 width: (crop_id.attr('width') * scale_x) + 'px',
                 height: (crop_id.attr('height') * scale_y) + 'px'
            });
		}
		
		function setMarkers(options){
			var pageNum = 1;
			var maxPages = 1;

//			loadingInProgress = true;
			//if (options) console.log('setMarkers called: options.searchStories: ' + options.searchStories + ' options.getDeals: ' + options.getDeals);

			function loadMarkers( json, ourAjaxCallId  ){
//				if( !interruptSignal ){
				//console.log('currAjaxCallId: ' + currAjaxCallId + ' ourAjaxCallId: ' + ourAjaxCallId);

				if( json && json[0] && currAjaxCallId === ourAjaxCallId ){
					maxPages = parseFloat( json[0].noOfPages );
					if( json[0].fittingRoomSummaryList ){
						var numItems = json[0].fittingRoomSummaryList.length;
						var currItem=null;
						
						for( var i = 0; i<numItems; i++ ){
							currItem = json[0].fittingRoomSummaryList[i];
							if( !!currItem.latitude && !!currItem.longitude && !!currItem.messageId){
								if( !latLongArray ){
									latLongArray = {};
								}
								latLongArray[currItem.latitude] = {};
								latLongArray[currItem.latitude][currItem.longitude] = {};
								latLongArray[currItem.latitude][currItem.longitude].messageId = currItem.messageId;
								
								var markerContent = {
									markerType: 'storyMarker',
									what: currItem.wsBuzz.what,
									when: currItem.wsBuzz.when,
									where: currItem.wsBuzz.where,
									who: currItem.wsBuzz.who,
									why: currItem.wsBuzz.why,
									how: currItem.wsBuzz.how,
									oped: currItem.wsBuzz.oped,
									originatorImageUrl: currItem.originatorImageUrl,
									originatorName: currItem.originatorName,
									elapsedTime: !!currItem.wsMessage ? currItem.wsMessage.elapsedTime : '' 
								};
								
								var latLong = new google.maps.LatLng(currItem.latitude,currItem.longitude);			
//								if( mapCanvasObject.getBounds().contains(latLong)){

									addMarker(currItem.latitude,currItem.longitude, currItem.messageId, 
										(!!currItem.headline ? currItem.headline : 'Test Headline'),
										currItem.senderThumbImageUrl, markerContent, 0);								
				                              //add here
									var SliderItem= '\<div class="item"><div class="itemcontainer" id='+count+'><img src="'+currItem.senderThumbImageUrl+'" />\
										<div class="text">'+(!!currItem.headline ? currItem.headline : 'Test Headline') +'</div>\
										</div></div>';
									current_slider.FlowSlider().content().append(SliderItem);
									current_slider.FlowSlider().setupDOM();
									sliderarray[count]=currItem;      
                                                                  
									$('#'+count+'').click( function(){
                                       var ID = $(this).attr("id");
                                       var selecteditem=sliderarray[ID];
                                       TN.lightbox.show(custId, selecteditem.messageId);                                                                       
									}); 
                                                                  
									$('#'+count+'').hover( function() {
										var ID = $(this).attr("id");
//										if (prevId!==ID){

										
										var selecteditem=sliderarray[ID];
										markerContent = {
											markerType: 'storyMarker',
											what: selecteditem.wsBuzz.what,
											when: selecteditem.wsBuzz.when,
											where: selecteditem.wsBuzz.where,
											who: selecteditem.wsBuzz.who,
											why: selecteditem.wsBuzz.why,
											how: selecteditem.wsBuzz.how,
											oped: selecteditem.wsBuzz.oped,
											originatorImageUrl: selecteditem.originatorImageUrl,
											originatorName: selecteditem.originatorName,
											elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 
										};
											
										if (prevwindow !==null){
											prevwindow.close();
										}
										var currtip=TooltipOpen(selecteditem.latitude,selecteditem.longitude,selecteditem.messageId, (!!selecteditem.headline ? selecteditem.headline : 'Test Headline' ),
											selecteditem.senderThumbImageUrl, markerContent);
										//TN.utils.passiveNotification('Info:', latLongArray[selecteditem.latitude][selecteditem.longitude].toSource);
										// if(latLongArray[selecteditem.latitude][selecteditem.longitude]!=={}){

										var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
										currtip.open(mapCanvasObject, currwindow.globalMarker);
//                              		skimlinks();

										prevwindow=currtip;                                           
											//}   

										prevId=ID;
									});

//crop($('#'+count+''), 0, 0, 223, 150);
                                      
                                   /*   $('#'+count+'').hover( function() {
                                          Todo   
                                         });
                                     */
									count=count+1;

//								}
							}
						}
                                                             
                                    
						if( pageNum < maxPages ){
							pageNum++;
							getRequestSummaries(custId, 25, pageNum, catType).
								done( function( data ){
									loadMarkers(data, ourAjaxCallId);
								});
//								.fail(function(){
//									loadingInProgress = false;
//									if( interruptSignal ){
//										resetAndReload();
//									}
//								});
						}
						else {  
							countcurr=count;
							//console.log('loadMarkers end countcurr: ' + countcurr);
							count=0;
							markerCluster.addMarkers(markersArray);
							mapCanvasObject.setZoom(canvasZoomLevel);
							if( !!prevwindow ){
								prevwindow.open(mapCanvasObject, prevwindow.globalMarker);
//								skimlinks();

							}
							// markerCluster = new MarkerClusterer(mapCanvasObject,[], markersArray);				       
//							loadingInProgress = false;

						}
					}
				}				
//				}
//				else {  
//					count=0;
//					loadingInProgress = false;
//					resetAndReload();
//				}
            }

            function loadSearchMarkers( json, ourAjaxCallId ){
//				if( !interruptSignal ){
            	//console.log('currAjaxCallId: ' + currAjaxCallId + ' ourAjaxCallId: ' + ourAjaxCallId);

				if( json && json[0] && currAjaxCallId === ourAjaxCallId ){
					var numItems = json.length;
					var currItem = null;
					
                    for( var i = 0; i<numItems; i++ ){
						currItem = json[i];
						if( !!currItem.latitude && !!currItem.longitude && !!currItem.messageId){
							if( !latLongArray ){
								latLongArray = {};
							}
							latLongArray[currItem.latitude] = {};
							latLongArray[currItem.latitude][currItem.longitude] = {};
							latLongArray[currItem.latitude][currItem.longitude].messageId = currItem.messageId;
							
							var markerContent = {
								markerType: 'storyMarker',
								what: currItem.what,
								when: currItem.when,
								where: currItem.where,
								who: currItem.who,
								why: currItem.why,
								how: currItem.how,
								oped: currItem.oped,
								originatorImageUrl: currItem.customerThumbImageUrl,
								originatorName: currItem.firstName,
								elapsedTime: !!currItem.wsMessage ? currItem.wsMessage.elapsedTime : '' 
							};
				            var latLong = new google.maps.LatLng(currItem.latitude,currItem.longitude);			
					        
//					        if( mapCanvasObject.getBounds().contains(latLong)){

		                                      
								addMarker(currItem.latitude,currItem.longitude, currItem.messageId, 
								(!!currItem.headline ? currItem.headline : 'Test Headline'),
								currItem.thumbImageUrl, markerContent, 0);								
								//add here
								var SliderItem= '\<div class="item"><div class="itemcontainer" id='+count+'><img src="'+currItem.thumbImageUrl+'" />\
									<div class="text">'+(!!currItem.headline ? currItem.headline : 'Test Headline') +'</div>\
									</div></div>';
								current_slider.FlowSlider().content().append(SliderItem);
								current_slider.FlowSlider().setupDOM();
								sliderarray[count]=currItem;      

								$('#'+count+'').click( function(){
									var ID = $(this).attr("id");
									var selecteditem=sliderarray[ID];
									TN.lightbox.show(custId, selecteditem.messageId);
								}); 

								$('#'+count+'').hover( function() {
									var ID = $(this).attr("id");
//									if (prevId!==ID){

									
									var selecteditem=sliderarray[ID];
									markerContent = {
										markerType: 'storyMarker',
										what: selecteditem.what,
										when: selecteditem.when,
										where: selecteditem.where,
										who: selecteditem.who,
										why: selecteditem.why,
										how: selecteditem.how,
										oped: selecteditem.oped,
										originatorImageUrl: selecteditem.customerThumbImageUrl,
										originatorName: selecteditem.firstName,
										elapsedTime: !!selecteditem.wsMessage ? selecteditem.wsMessage.elapsedTime : '' 											
									};
									
									if (prevwindow !==null){
										prevwindow.close();
									}
									var currtip=TooltipOpen(selecteditem.latitude,selecteditem.longitude, selecteditem.messageId, (!!selecteditem.headline ? selecteditem.headline : 'Test Headline'),
									selecteditem.thumbImageUrl, markerContent);

									var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
									currtip.open(mapCanvasObject, currwindow.globalMarker);
//										skimlinks();

									prevwindow=currtip; 
//									}

									prevId=ID;
								});

								count=count+1;
//                        	}

                        }
            		}
                                                         
					countcurr=count;
					count=0;
                    markerCluster.addMarkers(markersArray);			
                    mapCanvasObject.setZoom(canvasZoomLevel);
					if( !!prevwindow ){
	                    prevwindow.open(mapCanvasObject, prevwindow.globalMarker);
					}

				}
        	}

        	function loadDealMarkers ( json, ourAjaxCallId, dealTag ){
        		//console.log('currAjaxCallId: ' + currAjaxCallId + ' ourAjaxCallId: ' + ourAjaxCallId);

				if( json && json.deals && currAjaxCallId === ourAjaxCallId ){
					var numItems = json.deals.length;
					var currItem = null;
					var currItemMerchantName = null;
					var currItemThumb = null;
					var currItemDealUrl = null;
					var currItemTitle = null;
					var currItemShortAnnouncementTitle = null;
					var currItemAnnouncementTitle = null;

					var currItemRedemptionLocation = null;

					var currItemOptionId = null;
					var currItemOptionPrice = null;
					var currItemOptionDiscount = null;
					var currItemOptionValue = null;
					var currItemOptionTitle = null;
					var currItemOptionDiscountPercent = null;

					var currItemRedemptionLocationLat = null;
					var currItemRedemptionLocationLng = null;
					var currItemRedemptionLocationCity = null;
					var currItemRedemptionLocationId = null;

                    for( var i = 0; i<numItems; i++ ){

						currItem = json.deals[i];
						var currItemTagMatch = false;
						
						if (typeof currItem.options[0] === "undefined" ) continue;
						
						$.each(currItem.tags, function(i, tag){
							if (typeof this.name !== "undefined") {
								if (this.name == dealTag) currItemTagMatch = true;
							}
						});

						if (!currItemTagMatch) continue;

						currItemMerchantName = currItem.merchant.name;
						currItemThumb = currItem.largeImageUrl;
						currItemShortAnnouncementTitle = currItem.shortAnnouncementTitle;
						currItemAnnouncementTitle = currItem.announcementTitle;;
						currItemDealUrl = currItem.dealUrl;
						currItemTitle = currItem.title;
						currItemRedemptionLocation = currItem.redemptionLocation;

						$.each(currItem.options, function(i, option){
							if (typeof option.redemptionLocations[0] === "undefined") return;

							currItemOptionId = option.id;
							currItemOptionPrice = option.price.amount / 100;
							currItemOptionDiscount = option.discount.amount / 100;
							currItemOptionValue = option.value.amount / 100;
							currItemOptionTitle = option.title;
							currItemOptionDiscountPercent = option.discountPercent;

							$.each(option.redemptionLocations, function(j, redemptionLocation){

								currItemRedemptionLocationLat = redemptionLocation.lat;
								currItemRedemptionLocationLng = redemptionLocation.lng;
								currItemRedemptionLocationCity = redemptionLocation.city;
								currItemRedemptionLocationId = redemptionLocation.id

								if( !!currItemRedemptionLocationLat && !!currItemRedemptionLocationLng && !!currItemRedemptionLocationId){
									if( !latLongArray ){
										latLongArray = {};
									}
									latLongArray[currItemRedemptionLocationLat] = {};
									latLongArray[currItemRedemptionLocationLat][currItemRedemptionLocationLng] = {};
									//latLongArray[currItem.latitude][currItemRedemptionLocationLng].dealId = currItem.options[0].id;
									latLongArray[currItemRedemptionLocationLat][currItemRedemptionLocationLng].messageId = currItemRedemptionLocationId;
									
									var markerContent = {
										markerType: 'dealMarker',
										itemTitle: currItemShortAnnouncementTitle,
										itemTitleMedium: currItem.currItemAnnouncementTitle,
										thumbImageUrl: currItemThumb,
										merchantName: currItemMerchantName,
										itemPrice: currItemOptionPrice,
										discountedVal: currItemOptionDiscount,
										itemValue: currItemOptionValue, 
										city: currItemRedemptionLocationCity,
										optionId: currItemOptionId, 
										fullItemTitle: currItemTitle,
										generalRedemptionLocation: currItemRedemptionLocation, 
										dealUrl: currItemDealUrl,
										discountPercent: currItemOptionDiscountPercent
									};

						            var latLong = new google.maps.LatLng(currItemRedemptionLocationLat,currItemRedemptionLocationLng);			
				                                      
									addMarker(currItemRedemptionLocationLat, currItemRedemptionLocationLng, currItemRedemptionLocationId, 
									(!!currItemShortAnnouncementTitle ? currItemShortAnnouncementTitle : 'No Title'),
									currItemThumb, markerContent, 0);
									//add here
									var SliderItem= '\<div class="item"><div class="itemcontainer" id='+count+'><img src="'+currItemThumb+'" />\
										<div class="text">'+(!!currItemShortAnnouncementTitle ? currItemShortAnnouncementTitle : 'No Title') +'</div>\
										</div></div>';
									current_slider.FlowSlider().content().append(SliderItem);
									current_slider.FlowSlider().setupDOM();

									redemptionLocation.markerType = 'dealMarker';
									/*redemptionLocation.headline = currItemShortAnnouncementTitle;*/
									redemptionLocation.headline = currItemAnnouncementTitle;
									redemptionLocation.optionValue = currItemOptionValue;
									redemptionLocation.optionDiscountedValue = currItemOptionDiscount;
									redemptionLocation.redemptionLocationCity = currItemRedemptionLocationCity;
									redemptionLocation.latitude = currItemRedemptionLocationLat;
									redemptionLocation.longitude = currItemRedemptionLocationLng;
									redemptionLocation.thumbImageUrl = currItemThumb;
									redemptionLocation.messageId = currItemRedemptionLocationId;
									redemptionLocation.merchantName = currItemMerchantName;
									redemptionLocation.optionId = currItemOptionId;
									redemptionLocation.itemDealUrl = currItemDealUrl;
									redemptionLocation.fullItemTitle = currItemTitle;
									redemptionLocation.generalRedemptionLocation = currItemRedemptionLocation;
									redemptionLocation.dealUrl = currItemDealUrl;
									redemptionLocation.discountPercent = currItemOptionDiscountPercent;
									redemptionLocation.optionPrice = currItemOptionPrice;

									// In case of deal Markers, stored item is particular redemption location
									sliderarray[count]=redemptionLocation;

									$('#'+count+'').click( function(){
										var ID = $(this).attr("id");
										var selecteditem=sliderarray[ID];
										//console.log(custId, ' selecteditem.currItemOptionId: '+selecteditem.optionId, ' selecteditem.messageId: ' + selecteditem.messageId);
										TN.showDealLightbox(selecteditem.optionId, selecteditem.messageId);
									});


									$('#'+count+'').hover( function() {
										var ID = $(this).attr("id");
										var selecteditem = sliderarray[ID];

										markerContent = {
											markerType: selecteditem.markerType,
											itemTitle: selecteditem.headline,
											thumbImageUrl: selecteditem.thumbImageUrl,
											merchantName: selecteditem.merchantName,
											itemPrice: selecteditem.optionPrice,
											discountedVal: selecteditem.optionDiscountedValue,
											itemValue: selecteditem.optionValue, 
											city: selecteditem.redemptionLocationCity,
											optionId: selecteditem.optionId, 
											fullItemTitle: selecteditem.fullItemTitle,
											generalRedemptionLocation: selecteditem.generalRedemptionLocation,
											dealUrl: selecteditem.dealUrl,
											discountPercent: selecteditem.discountPercent
										};
										
										if (prevwindow !==null){
											prevwindow.close();
										}
										var currtip=TooltipOpen(selecteditem.latitude, selecteditem.longitude, selecteditem.messageId, 
											selecteditem.headline, selecteditem.thumbImageUrl, markerContent);

										var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
										currtip.open(mapCanvasObject, currwindow.globalMarker);

										prevwindow=currtip; 

										prevId=ID;
									});

									count=count+1;

		                        }
							});

						})
						
            		}
                                                         
					countcurr=count;
					count=0;
                    markerCluster.addMarkers(markersArray);			
                    //mapCanvasObject.setZoom(canvasZoomLevel);
					if( !!prevwindow ){
	                    prevwindow.open(mapCanvasObject, prevwindow.globalMarker);
//	                    skimlinks();
					}

//                    searchInProgress = false;
//					loadingInProgress = false;
				}
//				}
//				else {  
//					count=0;
//					searchInProgress = false;
//					loadingInProgress = false;
//					resetAndReload();
//				}
        	}

        	function loadArtbeatEventMarkers ( json, ourAjaxCallId ){

				if( json && json.Event && currAjaxCallId === ourAjaxCallId ){
					var numItems = json.Event.length;
					var currItem = null;
					var currItemLocationLat = null;
					var currItemLocationLng = null;
					var currItemTitle = null;
					var currItemThumb = null;
					var currItemUrl = null;
					var currItemId = null;
					var currItemWhat = null;
					var currItemWhen = null;
					var currItemWhere = null;
					var currItemHow = null;
					var currItemOped = null;
					var currItemPoweredBy = '<span style="font-size:6px">Powered by <a href="http://www.' + options.city + 'artbeat.com">' + options.city + 'artbeat.com</a></span>';

					for( var i = 0; i<numItems; i++ ){

						currItem = json.Event[i];
						currItemLocationLat = currItem.Latitude;
						currItemLocationLng = currItem.Longitude;
						currItemTitle = currItem.Name;
						currItemThumb = currItem.Image[2]["@attributes"].src;
						currItemUrl = currItem["@attributes"].href;
						currItemId = currItem["@attributes"].id;
						if (typeof currItem.Media !== "string") currItemWhat = currItem.Media.join(", ");
						else currItemWhat = currItem.Media;
						currItemWhen = currItem.DateStart + ' - ' + currItem.DateEnd + '; Venue hours: from ' + currItem.Venue.OpeningHour + ' to ' + currItem.Venue.ClosingHour;
						currItemWhere = currItem.Venue.Name + '; ' + currItem.Venue.Address;
						currItemHow = currItem.Venue.Access;
						currItemOped = currItem.Description + '<br><br>' + currItemPoweredBy;

						if( !!currItemLocationLat && !!currItemLocationLng && !!currItemId){
							if( !latLongArray ){
								latLongArray = {};
							};
							latLongArray[currItemLocationLat] = {};
							latLongArray[currItemLocationLat][currItemLocationLng] = {};
							latLongArray[currItemLocationLat][currItemLocationLng].messageId = currItemId;
							
							var markerContent = {
								markerType: 'eventMarker',
								what: currItemWhat,
								when: currItemWhen,
								where: currItemWhere,
								how: currItemHow,
								oped: currItemOped,
								itemTitle: currItemTitle,
								thumbImageUrl: currItemThumb,
								itemUrl: currItemUrl,
								itemId: currItemId
							};

							var latLong = new google.maps.LatLng(currItemLocationLat,currItemLocationLng);			

							addMarker(currItemLocationLat, currItemLocationLng, currItemId, (!!currItemTitle ? currItemTitle : 'No Title'),
								currItemThumb, markerContent, 0);
							//add here
							var SliderItem= '\<div class="item"><div class="itemcontainer" id='+count+'><img src="'+currItemThumb+'" />\
								<div class="text">'+(!!currItemTitle ? currItemTitle : 'No Title') +'</div>\
								</div></div>';
							current_slider.FlowSlider().content().append(SliderItem);
							current_slider.FlowSlider().setupDOM();

							currItem.markerType = 'eventMarker';
							// Make a lovercase latitude and logitude field copies for purpose of compatibility
							currItem.latitude = currItemLocationLat;
							currItem.longitude = currItemLocationLng;
							// Map items "messageId", "headline", "thumbImageUrl" for same purpose
							currItem.messageId = currItemId;
							currItem.headline = currItemTitle;
							currItem.thumbImageUrl = currItemThumb;
							currItem.what = currItemWhat;
							currItem.when = currItemWhen;
							currItem.where = currItemWhere;
							currItem.how = currItemHow;
							currItem.oped = currItemOped;
							currItem.itemUrl = currItemUrl;

							sliderarray[count]=currItem;

							$('#'+count+'').click( function(){
								var ID = $(this).attr("id");
								var selecteditem=sliderarray[ID];
								//TN.lightbox.show(custId, selecteditem.itemId);
								TN.showEventLightbox(selecteditem.messageId);
							});

							$('#'+count+'').hover( function() {
								var ID = $(this).attr("id");
								var selecteditem = sliderarray[ID];

								markerContent = {
									what: selecteditem.what,
									when: selecteditem.when,
									where: selecteditem.Venue.Address,
									how: selecteditem.Venue.Access,
									oped: selecteditem.Description,
									markerType: selecteditem.markerType,
									itemTitle: selecteditem.itemTitle,
									thumbImageUrl: selecteditem.thumbImageUrl,
									itemUrl: selecteditem.itemUrl,
									itemId: selecteditem.itemId
								};
								
								if (prevwindow !==null){
									prevwindow.close();
								}
								var currtip=TooltipOpen(selecteditem.Latitude, selecteditem.Longitude, selecteditem["@attributes"].id, 
									selecteditem.Name, selecteditem.Image[2]["@attributes"].src, markerContent);

								var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
								currtip.open(mapCanvasObject, currwindow.globalMarker);

								prevwindow=currtip;

								prevId=ID;
							});

							count=count+1;

						};

						
					};

					countcurr=count;
					count=0;
					markerCluster.addMarkers(markersArray);
					// mapCanvasObject.setZoom(canvasZoomLevel);
					if( !!prevwindow ){
						prevwindow.open(mapCanvasObject, prevwindow.globalMarker);
						// skimlinks();
					};

				};

			};

			function loadEventfindaMarkers ( json, ourAjaxCallId ){
				if( json && json.events && currAjaxCallId === ourAjaxCallId ){
					var numItems = json.events.length;
					var currItem = null;
					var currItemLocationLat = null;
					var currItemLocationLng = null;
					var currItemTitle = null;
					var currItemThumb = null;
					var currItemUrl = null;
					var currItemId = null;
					var currItemWhen  = null;
					var currItemWhere = null;
					var currItemWho   = null;
					var currItemWhy   = null;
					var currItemHow   = null;
					var currItemOped  = null;

					var currItemPoweredBy = '<span style="font-size:6px">Powered by <a href="http://www.eventfinda.com">eventfinda.com</a></span>';

					for( var i = 0; i<numItems; i++ ){
						currItem = json.events[i];
						currItemLocationLat = currItem.location.point.lat;
						currItemLocationLng = currItem.location.point.lng;
						currItemTitle = currItem.name;
						var thumbs = [];
						$.each(currItem.images.images['0'].transforms.transforms, function(i, val){
							thumbs.push(val.url);
						});
						// The last item in array corresponds to highest resolution image
						currItemThumb = thumbs[thumbs.length-1];
						currItemUrl = currItem.url;
						currItemId = currItem.id;
						currItemWhat = currItem.category.name;
						currItemWhen = currItem.datetime_summary + '; ' + currItem.datetime_start + ' - ' + currItem.datetime_end;
						currItemWhere = currItem.location_summary + '; ' + currItem.address;
						currItemWho = [];
						$.each(currItem.artists.artists, function(i, val){
							currItemWho.push(val.name);
						});
						currItemWho = currItemWho.join(', ');
						if ( TN.utils.isBlank(currItemWho) ) currItemWho = currItem.name;
						currItemOped = currItem.description + '<br><br>' + currItemPoweredBy;

						if( !!currItemLocationLat && !!currItemLocationLng && !!currItemId){
							if( !latLongArray ){
								latLongArray = {};
							};
							latLongArray[currItemLocationLat] = {};
							latLongArray[currItemLocationLat][currItemLocationLng] = {};
							latLongArray[currItemLocationLat][currItemLocationLng].messageId = currItemId;
							
							var markerContent = {
								markerType: 'eventfindaMarker',
								what: currItemWhat,
								when: currItemWhen,
								where: currItemWhere,
								who: currItemWho,
								oped: currItemOped,
								itemTitle: currItemTitle,
								thumbImageUrl: currItemThumb,
								itemUrl: currItemUrl,
								itemId: currItemId
							};

							var latLong = new google.maps.LatLng(currItemLocationLat,currItemLocationLng);			

							addMarker(currItemLocationLat, currItemLocationLng, currItemId, (!!currItemTitle ? currItemTitle : 'No Title'),
								currItemThumb, markerContent, 0);
							//add here
							var SliderItem= '\<div class="item"><div class="itemcontainer" id='+count+'><img src="'+currItemThumb+'" />\
								<div class="text">'+(!!currItemTitle ? currItemTitle : 'No Title') +'</div>\
								</div></div>';
							current_slider.FlowSlider().content().append(SliderItem);
							current_slider.FlowSlider().setupDOM();

							currItem.markerType = 'eventfindaMarker';
							// Make a lovercase latitude and logitude field copies for purpose of compatibility
							currItem.latitude = currItemLocationLat;
							currItem.longitude = currItemLocationLng;
							// Map items "messageId", "headline", "thumbImageUrl" for same purpose
							currItem.messageId = currItemId;
							currItem.headline = currItemTitle;
							currItem.thumbImageUrl = currItemThumb;
							currItem.what = currItemWhat;
							currItem.when = currItemWhen;
							currItem.where = currItemWhere;
							currItem.who = currItemWho;
							currItem.oped = currItemOped;
							currItem.itemUrl = currItemUrl;

							sliderarray[count]=currItem;

							$('#'+count+'').click( function(){
								var ID = $(this).attr("id");
								var selecteditem=sliderarray[ID];
								TN.showEventLightbox(selecteditem.messageId);
							});

							$('#'+count+'').hover( function() {
								var ID = $(this).attr("id");
								var selecteditem = sliderarray[ID];

								markerContent = {
									what: selecteditem.what,
									when: selecteditem.when,
									where: selecteditem.where,
									who: selecteditem.who,
									oped: selecteditem.oped,
									markerType: selecteditem.markerType,
									itemTitle: selecteditem.name,
									thumbImageUrl: selecteditem.thumbImageUrl,
									itemUrl: selecteditem.itemUrl,
									itemId: selecteditem.id
								};
								
								if (prevwindow !==null){
									prevwindow.close();
								}
								var currtip=TooltipOpen(selecteditem.latitude, selecteditem.longitude, selecteditem.id, 
									selecteditem.name, selecteditem.thumbImageUrl, markerContent);

								var currwindow = latLongArray[selecteditem.latitude][selecteditem.longitude];
								currtip.open(mapCanvasObject, currwindow.globalMarker);

								prevwindow=currtip;

								prevId=ID;

							});

							count=count+1;

						};

						
					};

					countcurr=count;
					count=0;
					markerCluster.addMarkers(markersArray);
					// mapCanvasObject.setZoom(canvasZoomLevel);
					if( !!prevwindow ){
						prevwindow.open(mapCanvasObject, prevwindow.globalMarker);
						// skimlinks();
					};

				};

			};

        	//console.log(options, options.searchStories, (options && options.searchStories));
        	
			currAjaxCallId = new Date().getTime();
			
			if (options && options.searchStories) {
				count=0;
				(function(){
					var ourAjaxCallId = currAjaxCallId;
					TN.services.searchStories(options.searchTxt, 1, 50).
					done( function(data){
						loadSearchMarkers(data, ourAjaxCallId);
					});
				})();

			}

			else if (options && options.getDeals) {
				count=0;
				(function(){
//						searchInProgress = false;
//						loadingInProgress = false;
//						if( interruptSignal ){
//							resetAndReload();
					var ourAjaxCallId = currAjaxCallId;
					//if( mapCanvasObject.getBounds().contains(latLong)) {
						TN.services.grouponDeals(options.dealLat, options.dealLng).
						done( function(data){
							//console.log(latLng.toUrlValue());
							loadDealMarkers(data, ourAjaxCallId, options.dealTag);
						}).
						fail( function(data){
							log('Groupon api call fail: ');
							log(data);
						});
					//}
				})();
			}

			else if (options && options.getArtbeatEvents) {
				count=0;
				(function(){
					var ourAjaxCallId = currAjaxCallId;
						TN.services.getArtBeatList(options.city, options.xmlName).
						done( function(data){
							loadArtbeatEventMarkers(data, ourAjaxCallId);
						}).
						fail( function(data){
							log('Artbeat api call fail: ');
							log(data);
						});
				})();
			}

			else if (options && options.getEventfindaEvents) {
				count=0;
				(function(){
					var ourAjaxCallId = currAjaxCallId;
						TN.services.getEventfindaEvents(options.lat, options.lng, options.catSlug).
						done( function(data){
							loadEventfindaMarkers(data, ourAjaxCallId);
						}).
						fail( function(data){
							log('Eventfinda api call fail: ');
							log(data);
						});
				})();
			}

			else {
				count=0;
				(function(){
					var ourAjaxCallId = currAjaxCallId;
					getRequestSummaries(custId, 25, pageNum, catType).
					done( function(data){
						loadMarkers(data, ourAjaxCallId);
					});
				})();
//				.fail(function(){
//					loadingInProgress = false;
//					if( interruptSignal ){
//						resetAndReload();
//					}
//				});	
			}
			
			//console.log(latLng);
        	//console.log('setMarker mapCanvasObject.getBounds(): ', mapCanvasObject.getBounds());
        	//console.log('setMarker mapCanvasObject.getZoom(): ' + mapCanvasObject.getZoom());
		}

		function searchDispatch(){

            var nextSearchTxt = $('#newsmap-search-input').val();

            if ( !TN.utils.isBlank(nextSearchTxt) ) {
                
                currentSearchString = nextSearchTxt;
	        	
	        	catDropDown.val('').trigger('change');
	        	resetAndReload({searchStories:true, searchTxt: currentSearchString});

	        }
        };

        function populateDealCategories(lat, lng){
			TN.services.grouponDeals(lat, lng).done( function(data){
				var link;
				var halfNumberOfTags;
				var itemsRendered = 0;
				//console.log(data);
				
				$(data.deals).each(function(){
					if (typeof this.tags !== 'undefined' && typeof this.tags[0] !== 'undefined') {
						
						$(this.tags).each(function(){
							if (typeof this.name !== 'undefined'){
								if ( jQuery.inArray(this.name, currentDealTags) == -1) {
									currentDealTags.push(this.name);
								}
							}
						});
					}
				});

				halfNumberOfTags = parseInt(currentDealTags.length / 2);

				$.each(currentDealTags, function(i,tag){

//	                }).
//	                always(function(){
	        			//loadingInProgress = false;
//                        searchInProgress = false; //('#progressIndicator').remove();
//	                }).fail(function(){
	                    //("No stories available.");
//	                    searchInProgress = false; //('#progressIndicator').remove();
//	                });
					//console.log('currentDealTag: ' + tag);
					if (itemsRendered <= halfNumberOfTags){
						link = $('<a href="javascript:void(0);" rel="' + tag + '">' + tag + '</a><br>');
						dealCatsFirstCol.append(link);
					}
					else {
						link = $('<a href="javascript:void(0);" rel="' + tag + '">' + tag + '</a><br>');
						dealCatsSecondCol.append(link);				
					}

					itemsRendered++;
					link.click(function(){
						//console.log({getDeals:true, tag:tag});
						$('form.mapCategoriesSelectForm a.current').text(tag);
						$('.newsmapCustomFlyout').hide();
						resetAndReload({getDeals:true, dealTag:tag, dealLat: lat, dealLng: lng});
					});
				});
				//console.log(this.channels[0].name);
			}).fail(function(){
				log('Failed to populate deal categories.');
			});
        }

        // Populates newsmap dropdown with Events' Smart Lists from http://www.nyartbeat.com/resources/doc/api
        // and http://www.tokyoartbeat.com/resources/doc/api
        function populateEventLists(){
			
			var link;
			var eventsSmartLists = {
			    "ny": {
			        "Most Popular": "event_mostpopular.en",
			        "Starting Soon": "event_comingsoon.en",
			        "Opening Receptions": "event_opening.en",
			        "Just Started": "event_juststarted.en",
			        "Closing Soon": "event_lastdays.en",
			        "Free Entrance": "event_free.en",
			        "For Kids Too": "event_kids.en",
			        "Open Late": "event_openlate.en",
			        "Permanent": "event_permanent.en"
			    },
			    "tokyo": {
			        "Most Popular": "event_mostpopular.en",
			        "Starting Soon": "event_comingsoon.en",
			        "Opening Receptions": "event_opening.en",
			        "Just Started": "event_juststarted.en",
			        "Closing Soon": "event_lastdays.en",
			        "Free Entrance": "event_free.en",
			        "For Kids Too": "event_kids.en",
			        "Open Late": "event_openlate.en",
			        "Permanent": "event_permanent.en"
			    }
			};

			eventCatsFirstCol.prepend('New York art events<br><br>');
			eventCatsSecondCol.prepend('Tokyo art events<br><br>');

			$.each(eventsSmartLists, function(index,city){

				$.each(city, function (listTitle, listXmlFile){
					//console.log(title, xmlName);
					link = $('<a href="javascript:void(0);" rel="' + listXmlFile + '">' + listTitle + '</a><br>');
					link.click(function(){
						$('form.mapCategoriesSelectForm a.current').text(listTitle);
						$('.newsmapCustomFlyout').hide();
						resetAndReload({getArtbeatEvents:true, city: index, xmlName: listXmlFile});
						//console.log({getArtbeatEvents:true, city: index, xmlName: listXmlFile});
					});
					(index == "ny") ? eventCatsFirstCol.append(link) : eventCatsSecondCol.append(link);
				});
			});
        };

        function populateEventfindaElements(lat, lng){
        	var link;
        	var eventfindaCats = {
        		"Business & Education" : "business-education",
				"Exhibitions" : "exhibitions",
				"Performing Arts" : "arts",
				"Concerts & Gig Guide" : "concerts-gig-guide",
				"Festivals & Lifestyle" : "festivals-lifestyle",
				"Sports" : "sports"
        	};

        	eventCatsFirstCol.append('<br>Local events<br><br>');

        	$.each(eventfindaCats, function (catTitle, catSlug){
	        	link = $('<a href="javascript:void(0);" rel="' + catSlug + '">' + catTitle + '</a><br>');
				link.click(function(){
					$('form.mapCategoriesSelectForm a.current').text(catTitle);
					$('.newsmapCustomFlyout').hide();
					resetAndReload({getEventfindaEvents:true, lat: lat, lng: lng, catSlug: catSlug});
					console.log({getEventfindaEvents:true, lat: lat, lng: lng, catSlug: catSlug});
				});
				eventCatsFirstCol.append(link);
			});
        };
		
		$mapsHandler.getApiKey = function(){
			return apiKey;
		};
				
		$mapsHandler.hide = function(){
			mapCanvas.hide();
			delete mapCanvasObject;
		};
		
		$mapsHandler.interruptOrReload = function(){
//			if( loadingInProgress ){
//				interruptSignal = true;
//			}
//			else {
			resetAndReload();
//			}
		};
		
		$mapsHandler.show = function(){
			function loadScript() {  
				var script = document.createElement('script');  
				script.type = 'text/javascript';  
				script.src = 'http://maps.googleapis.com/maps/api/js?key=' + apiKey + '&sensor=true&callback=TN.showMap';  
				document.body.appendChild(script);
			}
			
			function noGeoLocationSupport(){
				TN.utils.passiveNotification('Error!', "Unable to determine your current location. Your browser does not support geolocation.");
			}
			
			function noPositionError(){
				TN.utils.passiveNotification('Error!', "Your browser is unable to determine your current location.");
			}
			
			function showGlobalMap(){
				latLng =  new google.maps.LatLng(37.441, 0);
				canvasOptions = {
					zoom: canvasZoomLevel,
					center: latLng,
					mapTypeId: google.maps.MapTypeId.HYBRID};

				mapCanvas.empty();
				mapCanvasObject = new google.maps.Map(document.getElementById("map_canvas"), canvasOptions);
/*                            var styles=[{
      url: 'images/mappin.png',
        height: 35,
        width: 35,
        opt_anchor: [16, 0],
        opt_textColor: '#FF00FF'
      },
      {
        url: 'images/mappin.png',
        height: 45,
        width: 45,
        opt_anchor: [24, 0],
        opt_textColor: '#FF0000'
      },
      {
        url: 'images/mappin.png',
        height: 55,
        width: 55,
        opt_anchor: [32, 0]
      }];*/

				var mcOptions = {gridSize: 5, maxZoom: 60};
				markerCluster = new MarkerClusterer(mapCanvasObject,mcOptions);
				$("#map_canvas").show();
				// Add a listener to close an open current detail box if we click outside it on the map
				// Currently disabled because it's not playing well with our drag map through infoWindowDetailed functionality
				/*google.maps.event.addListener(mapCanvasObject, 'click', function(){
					if(currentOpenDetailBox !== null){
						currentOpenDetailBox.close();
						currentOpenDetailBox = null;
					}
				});*/
				
				setMarkers();
			}
			
			function loadWeatherGadget(position){
				var openWeatherAPIKey = '1976e9a941e48ec94d00e79c050a4617';
								
				var sliceType = [];
				
				sliceType['09'] = 'Morning';
				sliceType['12'] = 'Morning';
				sliceType['15'] = 'Day';
				sliceType['18'] = 'Day';
				sliceType['21'] = 'Evening';
				sliceType['00'] = 'Evening';
				sliceType['03'] = 'Night';
				sliceType['06'] = 'Night';
				
				function getWindHtml(wind, prefix){
					
					var icon = '';
					var direction = '';
					var degree = wind.deg;

					if( degree === 0 ){
						direction = 'N;'
					}
					
					if( degree > 0 && degree < 90 ){
						direction = 'NE';
					}
					
					if( degree === 90 ){
						direction = 'E';
					}
					
					if( degree > 90 && degree < 180 ){
						direction = 'SE';
					}
					
					if( degree === 180 ){
						direction = 'S';
					}
					
					if( degree > 180 && degree < 270 ){
						direction = 'SW';
					}
					
					if( degree === 270 ){
						direction = 'W';
					}
					
					if( degree > 270 ){
						direction = 'NW';
					}
					
					if (!!prefix ){
						icon =  prefix + '-' + direction + '-' + 'directional.png';
					}
					else {
						icon =  direction + '-' + 'directional.png';
					}
					
					var windHtml = '';
					
					if( prefix === 'lrg' ){
						windHtml = '<div class="wind">\
							<p>Wind</p>\
							<div class="left">\
								<img src="images/weather/wind/' + icon + '" alt="' + direction + '">\
								<p><strong>' + direction + '</strong></p>\
							</div>\
							<div class="right">\
								<p><strong>' + Math.round(wind.speed) + 'mph</strong></p>\
							</div>\
						</div>';
					} else {
						windHtml = '<div class="wind">\
							<p><img src="images/weather/wind/' + icon + '" alt="' + direction + '">&nbsp' + Math.round(wind.speed) + '</p>\
							<p class="sw">' + direction + '</p>\
						</div>';
					}
					
					return windHtml;
					
				}
				
				function getRestOfDayHtml(daySlice, headerClassAdd){
					var forecastHtml = '';
					
					forecastHtml = '<div class="header ' + headerClassAdd + '"><p>' + sliceType[daySlice.dt_txt.split(' ')[1].split(':')[0]] + '</p></div>\
						<div class="temperature">\
							<img src="http://openweathermap.org/img/w/' + daySlice.weather[0].icon + '.png" alt="' + daySlice.weather[0].main + '">\
							<p><span class="tempNum">' + Math.round(daySlice.main.temp) + '</span> &deg;<span class="tempType">F</span></p>\
						</div>' + 
						getWindHtml(daySlice.wind, '') +
						'<div class="humidity">\
							<p>' + daySlice.main.humidity + '%</p>\
						</div>';
					
					return forecastHtml;
				}
				
				function getWeatherGadgetHtml(data){
					var gadgetHtml = '';
										
					var rightNow = data.list[0];
					var daySlice1, daySlice2, daySlice3;
					
					//determine the index for the next day slice
					var currentForecastTime = rightNow.dt_txt.split(' ')[1].split(':')[0];
					
					if( currentForecastTime === '09' || currentForecastTime === '15' || currentForecastTime === '21' || currentForecastTime === '03' ){
						daySlice1 = 2;
					}
					else {
						daySlice1 = 1;
					}
					
					daySlice2 = daySlice1 + 2;
					daySlice3 = daySlice2 + 2;
					
//					gadgetHtml = '<iframe id="weather_gadget_iframe" src="weather-gadget.html?lat=' + position.coords.latitude + '&long=' + position.coords.longitude + '"></iframe>';
					gadgetHtml = '<div class="weather-gadget twelve columns">\
						<div id="weather-app-header">\
							<p class="left curr-city">' + data.city.name + '</p>\
							<dl class="tabs pill">\
								<dd id="celcius"><a href="#pillTab1">C</a></dd>\
								<dd id="farenheit" class="active"><a href="#pillTab2">F</a></dd>\
							</dl>\
                        </div>\
						<div class="days">\
							<div class="four columns">\
								<div class="right-now">\
									<div class="sun">\
										<p>Right now</p>\
										<div class="left">\
											<img src="http://openweathermap.org/img/w/' + rightNow.weather[0].icon + '.png" alt="' + rightNow.weather[0].main + '">\
										</div>\
										<div class="right">\
											<p><strong><span class="tempNum">' + Math.round(rightNow.main.temp) + '</span> &deg;<span class="tempType">F</span></strong></p>\
										</div>\
									</div>' +
									getWindHtml(rightNow.wind, 'lrg') +
									'<div class="humidity">\
										<div class="left">\
											<p>Humidity</p>\
										</div>\
										<div class="right">\
											<p><strong>' + rightNow.main.humidity + '%</strong></p>\
										</div>\
									</div>\
								</div>\
							</div>\
							<div class="two columns">' + 
								getRestOfDayHtml(rightNow) + 
							'</div>' + 
							'<div class="two columns lb">' +
								getRestOfDayHtml(data.list[daySlice1], 'dark') + 
							'</div>\
							<div class="two columns">' + 
								getRestOfDayHtml(data.list[daySlice2]) + 
							'</div>\
							<div class="two columns lb">' + 
								getRestOfDayHtml(data.list[daySlice3], 'dark') + 
							'</div>\
						</div>\
						<div class="right settings">\
						</div>\
						<div class="right row">\
							<p>Source: <a href="http://openweathermap.org">OpenWeatherMap</a></p>\
						</div>\
					</div>';
					
					return gadgetHtml;
				}
				
				$.ajax({
					type: 'GET',
					data : {
						lat: position.coords.latitude,
						lon: position.coords.longitude,
						units:'imperial',
						APPID : openWeatherAPIKey
					},
					url : 'http://api.openweathermap.org/data/2.5/forecast',
					dataType: 'jsonp'
				}).done(function(json){
					// Insert weather display gadget for detected position
					$('#weather-app').html(getWeatherGadgetHtml(json));
					$('#farenheit').click(function(){
						$('.tempNum').each(function(){
							var jqElem = $(this);
							jqElem.text(TN.utils.toFarenheit(jqElem.text()));
						});
						$('.tempType').each(function(){
							var jqElem = $(this);
							jqElem.text('F');
						});
					});
					$('#celcius').click(function(){
						$('.tempNum').each(function(){
							var jqElem = $(this);
							jqElem.text(TN.utils.toCelcius(jqElem.text()));
						});
						$('.tempType').each(function(){
							var jqElem = $(this);
							jqElem.text('C');
						});
					});
				}).fail(function(jqXHR, textStatus, errorThrown){
					TN.utils.passiveNotification('Error!', 'There was an error in retrieving weather information at this time for your location.');
				});
			}
			
			function centerToUserLocation(position){
				
				if( !!position && !!position.coords && !!position.coords.latitude && !!position.coords.longitude ){
					delete latLng;

					loadWeatherGadget(position);
					
					latLng =  new google.maps.LatLng(position.coords.latitude, 
							position.coords.longitude);
					
					mapCanvasObject.setCenter(latLng);

					populateDealCategories(position.coords.latitude, position.coords.longitude);
					populateEventfindaElements(position.coords.latitude, position.coords.longitude);
				}
			}
			
//			function showInsetMap(position) {  
//				if( !!position && !!position.coords && !!position.coords.latitude && !!position.coords.longitude ){
//					delete latLng;
//					
//					latLng =  new google.maps.LatLng(position.coords.latitude, 
//							position.coords.longitude);
//					
//					insetOptions = {
//						zoom:insetZoomLevel,
//						center:latLng,
//						mapTypeControl:false,
//						panControl:false,
//						rotateControl:false,
//						scrollwheel:false,
//						streetViewControl:false,
//						zoomControl:false,
//						draggable:false,
//						tilt:45,
//						mapTypeId:google.maps.MapTypeId.ROADMAP
//					};
//
//					var insetLeftOffset = mapCanvas.offset().left + mapCanvas.width()-mapInset.width()-insetBorderLeftWidth;
//					//var insetTopOffset = mapCanvas.offset().top + mapCanvas.height()-mapInset.height()-insetBorderTopWidth;
//					var insetTopOffset = -mapInset.height()-insetBorderTopWidth;
//					
//					$('#mapFilterBar').css('position', 'relative').css('top',-183).css('float','right').show();
//                                        
//					mapInset.css('top', insetTopOffset-25).css('position', 'relative').css('float','right').show();
//					$('#slideme').css('position','relative').css('top',-200).show();
//					$('#foot').css('position','relative').css('top',-60).css('padding',0).show();
//  
//				       // $('#slideme').css('position', 'relative').css('top', -171).show();
//                                //	$('#features').css('position', 'relative').css('top', insetTopOffset).css('margin-bottom', insetTopOffset).show();
//					
//					mapInset.empty();
//					mapInsetObject = new google.maps.Map(document.getElementById("mapInset"), insetOptions);
//								
////					google.maps.event.addListenerOnce(mapInsetObject, 'click', moveLocalToMain);
//					
//					// reset center of global map the new latlang
//					mapCanvasObject.setCenter(latLng);
//					
//					moveLocalToMain();
//					
//					// update inset map with markers added so far in the global map
//					for( var curLat in latLongArray ){
//						for( var curLong in latLongArray[curLat] ){
//							latLongArray[curLat][curLong].insetMarker = new google.maps.Marker({     
//								  position: latLongArray[curLat][curLong].marker.getPosition(),   
//								  title: latLongArray[curLat][curLong].headline.getContent(),
//								//  animation: google.maps.Animation.DROP,
//								  map: mapInsetObject  });
//							
//							markersArray.push(latLongArray[curLat][curLong].insetMarker);
//						}
//					}
//				}
//			}
			$TN.showMap = function(){
				$.getScript('javascripts/libs/infobox_packed.js').done(function(){
					showGlobalMap();
					trafficLayer = new google.maps.TrafficLayer();
					
					
					if( !!navigator.geolocation ){

						navigator.geolocation.getCurrentPosition(centerToUserLocation, noPositionError);
					}
					else {
						noGeoLocationSupport();
					}
//					var insetTopOffset = -mapInset.height()-insetBorderTopWidth;

					$('#mapFilterBar').css('position', 'relative').css('top',-20).show();
										//$('#slideme').css('position','relative').css('top',-60).show();
  
					$('#mapShowHeadlines').click(function(){
						showHeadlines = $(this).is(':checked');
						
						var curLatLong=null;
						for( var curLat in latLongArray ){
							for( var curLong in latLongArray[curLat] ){
								curLatLong = latLongArray[curLat][curLong];
								if( showHeadlines ){
									curLatLong.headline.open(mapCanvasObject, curLatLong.globalMarker);
								}
								else {
									curLatLong.headline.close();
								}
							}
						}
					});
				
					$('#traffic').click(function(){
						var jqElem = $(this);
						if( !!trafficLayer ){
							if( !!trafficLayer.getMap() ){
								trafficLayer.setMap(null);
								jqElem.removeClass('selected');
							}
							else {
								trafficLayer.setMap(mapCanvasObject);
								jqElem.addClass('selected');
							}
						}
					});

					$('#trafficDropdownLink').click(function(){
						var jqElem = $(this);
						if( !!trafficLayer ){
							if( !!trafficLayer.getMap() ){
								trafficLayer.setMap(null);
								$('.newsmapCustomFlyout').hide();
							}
							else {
								trafficLayer.setMap(mapCanvasObject);
								$('.newsmapCustomFlyout').hide();
							}
						}
					});
					
					$('#enlargeShrink').click(function(){
						var jqElem = $(this);
						if( mapEnlarged ){
							mapEnlarged = false;
							$('#mapContainer').removeClass('twelve').addClass('nine');
							$('#friendsStatus').show();
							google.maps.event.trigger(mapCanvasObject, 'resize');
							jqElem.css('background-image', 'url(\'images/expand-map-icon.png\')');
						}
						else {
							mapEnlarged = true;
							$('#mapContainer').removeClass('nine').addClass('twelve');
							$('#friendsStatus').hide();
							google.maps.event.trigger(mapCanvasObject, 'resize');
							jqElem.css('background-image', 'url(\'images/collapse-map-icon.png\')');
						}
					});
					
					$('body').click(function(){
		                if (prevwindow !==null){
		                	prevwindow.close();
		                    prevwindow=null;
		                }						
					});
                           
//					$(all).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=all;
//						$(currentfilter).css('background','#0b68a8').css('color','white');
//						mapFilter = 'all';
//						mapsHandler.interruptOrReload();
//					});
//                                    
//					$(fr).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=fr;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						mapFilter = 'friends';
//						mapsHandler.interruptOrReload();
//					});
//				
//					$(foll).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=foll;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						mapFilter = 'following';
//						mapsHandler.interruptOrReload();
//					});
//					$(me).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//                                                        
//						$(currentfilter).css('background','transparent').css('color','black');                                                                                       
//						currentfilter=me;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						mapFilter = 'me';
//						mapsHandler.interruptOrReload();
//					});
//				
//					$(pop).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=pop;
//						$(currentfilter).css('background','#0b68a8').css('color','white');                                                        
//						//    mapFilter = 'popular';
//						//	mapsHandler.interruptOrReload();
//					});                               
//
//					$(vid).click(function(){
//						if(prevwindow!==null){
//							prevwindow.close();
//						}	
//						$(currentfilter).css('background','transparent').css('color','black');
//						currentfilter=vid;
//						$(currentfilter).css('background','#0b68a8').css('color','white');
//						//      mapFilter = 'video';
//						//	mapsHandler.interruptOrReload();
//					}); 
					$('#newsmap-search-input').keyup(function(event){

						if( event.which === 13 ){

			                    searchDispatch();

			            }
					});
					
					mapDissolved = true;
				});
			};	
			
			loadCategories().done(loadScript);
			populateEventLists();
		};

		$TN.showDealLightbox = function(optionId, redemptionLocationId){
			var sliderToShow = null;
			
			$.each(sliderarray, function(i, slider){
				if ( (slider.optionId == optionId) && (slider.messageId == redemptionLocationId) ){
					sliderToShow = slider;
				};
			});

			//console.log(sliderToShow);

			var dealLBHtml = '<div class="reveal-modal dealLightboxReveal">\
					<div class="twelve columns  groupon-r">\
						<div class="row d-buy">\
							<div class="d-image"><img src="' + sliderToShow.thumbImageUrl + '" alt="deal-picture"></div>\
							<div class="buy">\
								<div class="price">\
								<p>FROM</p>\
								<p id="b">$' + sliderToShow.optionPrice + '</p>\
								</div>\
								<div class="btn"><a href="' + sliderToShow.itemDealUrl + '" target="_blank">BUY!</a></div>\
							</div>\
						</div>\
						<div class="row d-details">\
							<div class="sb-h">\
							<p>' + sliderToShow.merchantName + ' | ' + sliderToShow.generalRedemptionLocation + '</p>\
							<p>' + sliderToShow.redemptionLocationCity + '</p>\
							</div>\
							<p>' + sliderToShow.fullItemTitle + '</p>\
						</div>\
						<div class="row d-values">\
							<div class="d-values-c">\
								<div class="values">\
								<p>Value</p>\
								<p id="b">$' + sliderToShow.optionValue + '</p>\
							</div>\
							<div class="disc">\
								<p>Discount</p>\
								<p id="b">' + sliderToShow.discountPercent + '%</p>\
							</div>\
							<div class="saving">\
								<p>You Save</p>\
								<p id="b">$' + sliderToShow.optionDiscountedValue + '</p>\
							</div>\
							</div>\
						</div>\
					</div>\
					<a class="close-reveal-modal"></a>\
				</div>';

			$('body').append(dealLBHtml);
			$('.dealLightboxReveal').reveal({"closed":function(){
	            $('.dealLightboxReveal').remove();
	            }
	        });

		};

		$TN.showEventLightbox = function(itemId){

			var sliderToShow = null;
			
			$.each(sliderarray, function(i, slider){
				if ( slider.messageId == itemId ){
					sliderToShow = slider;
				};
			});

			//console.log(sliderToShow);
			var lBoxHtml = $('<div class="reveal-modal stories-modal eventLightboxReveal" id="lightboxModal">\
			    <input type="hidden" value="' + sliderToShow.messageId + '" class="messageId">\
			    <div class="row">\
			        <div class="row">\
			            <a class="close-reveal-modal"></a>\
			            <div class="storyimage left">\
			                <img alt="Event Image" src="' + sliderToShow.thumbImageUrl + '">\
			            </div>\
			            <div style="display:none;" class="button-margin"><a class="button radius" href="javascript:void(0);" sl-processed="1"> Tweet</a>\
			            </div>\
			        </div>\
			        <div class="row news-header">\
			            <div class="">\
			                <a href="' + sliderToShow.itemUrl + '" target="_blank"><div class="StoryTitle">' + sliderToShow.headline + '</div></a>\
			            </div>\
			            <div class="row news-details">\
			                <div class="twelve columns">\
			                    <div class="storyContent">\
			                        <div class="StoryText">' + (!!sliderToShow.who ? '<b>Who:</b> ' + sliderToShow.who : '') + '</div>\
			                        <div class="StoryText"><b>What:</b> ' + sliderToShow.what + '</div>\
			                        <div class="StoryText"><b>Where:</b> ' + sliderToShow.where + '</div>\
			                        <div class="StoryText"><b>When:</b> ' + sliderToShow.when + '</div>\
			                        <div class="StoryText">' + (!!sliderToShow.how ? '<b>How:</b> ' + sliderToShow.how : '') + '</div>\
			                        <div class="StoryText">' + sliderToShow.oped + '</div>\
			                    </div>\
			                </div>\
			            </div>\
			        </div>\
			    </div>\
			</div>');

			$('body').append(lBoxHtml);
			lBoxHtml.reveal({"closed":function(){
	            	$('.eventLightboxReveal').remove();
	            },
	            // There's something wrong with height calculation in Chrome so we adjust it after Reveal opens:
	            "opened":function(){
	            	$('.eventLightboxReveal').height( $('.eventLightboxReveal').height() + 50 );
	            }
	        });

		};
				
	}(mapsHandler));

	$TN.dissolveArtMap = function(){
		mapsHandler.show();
	};
	
	$TN.loadCategoryOnMap = function(categoryType){
		catType = escape(categoryType);
		if( !mapDissolved ){
			$TN.dissolveArtMap();
		}
		else {
			mapsHandler.interruptOrReload();
		}
	};
	
}(TN));
