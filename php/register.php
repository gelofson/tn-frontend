<?php

	// header('Content-type: application/json');
	// Enable Error Reporting and Display:
	// error_reporting(~0);
	// ini_set('display_errors', 1);
	$email = $_POST['emaill'];
	$pass = $_POST['passs'];
	$firstname = $_POST['firstnamee'];
	$lastname = $_POST['lastnamee'];
	$photo_url = $_POST['photo_urll'];
	
	if (!empty($email) && !empty($pass) && !empty($firstname) && !empty($photo_url)) {

		// First ensure that proper image is fetchable from $photo_url given and if not, 
		// in case of redirect, attempt to fetch the redirect location from returned header
		$temp_context = stream_context_create(
			array(
				'http' => array(
					'follow_location' => false
				)
			)
		);
		
		$temp_html = file_get_contents($photo_url, false, $temp_context);
		
		foreach ($http_response_header as $key => $value) {
			if (strpos($value, 'Location') !== false) {
				$position = strpos($http_response_header[$key], ':');
				// Overwrite $photo_url with the url redirected to
				$photo_url = trim(substr($http_response_header[$key], $position+1));
			}
		}

		// echo $photo_url;
		// Pack image into "data array"
		$contents = file_get_contents($photo_url);	
		$unpacked = unpack("c*", $contents);
		$pparam = "[";
		foreach ($unpacked as $val) {
			$pparam .= $val.",";
		}
		$pparam = rtrim($pparam, ",");
		$pparam .= "]";
	
		$jsonArray = array("email"=>$email, "password"=>$pass, "firstname"=>$firstname, "lastname"=>$lastname, "photo"=>$pparam);

		//var_dump($jsonArray);
		$customer = json_encode($jsonArray);
		//var_dump($buzz);
		$params = array("action"=>"registerCustomer", "customer"=>$customer);
		//print_r("\n\n\$params: ".$params);
		//var_dump($params);
		$query = http_build_query ($params);
		//var_dump($query);
		
		$contextData = array ('method' => 'POST', 'header' => "Connection: close\r\n"."Content-Length: ".strlen($query)."\r\n", 'content'=> $query, 'follow_location' => false);
		//var_dump($contextData);
		$context = stream_context_create (array ( 'http' => $contextData ));
		
		$result = file_get_contents("http://".$_SERVER['HTTP_HOST']."/salebynow/json.htm", false, $context);

		foreach ($http_response_header as $key => $value) {
			header ($value."\r\n");
		}
		echo($result);

	}

?>